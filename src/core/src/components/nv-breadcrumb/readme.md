# nv-breadcrumb



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute | Description                                                                                | Type     | Default     |
| ------------------- | --------- | ------------------------------------------------------------------------------------------ | -------- | ----------- |
| `href` _(required)_ | `href`    | The href for the `a` element, this is used for the add a link for the Breadcrumb elements. | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
