/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h } from '@stencil/core';

@Component({
  tag: 'nv-card-footer',
  shadow: false,
})
export class NvCardFooter {
  render() {
    return (
      <div class='nv-card-footer'>
        <slot />
      </div>
    );
  }
}
