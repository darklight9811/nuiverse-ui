/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element } from '@stencil/core';
import Ripple                    from '../../utils/ripple';

@Component({
  tag: 'nv-card-body',
  shadow: false,
})
export class NvCardBody {
  /**
   *  Reference to the host element
   */
  @Element() el: HTMLElement;

  componentDidLoad() {
    // Get button element
    const cardBody: HTMLElement = this.el.querySelector('.nv-card-body');
    // Add ripple effect
    Ripple(cardBody);
  }

  render() {
    return (
      <div class='nv-card-body'>
        <slot />
      </div>
    );
  }
}
