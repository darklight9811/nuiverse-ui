/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element, Prop, Method, Host } from '@stencil/core';
import Fragment                                      from 'stencil-fragment';
import { Color }                                     from '../../interface';

@Component({
  tag: 'nv-dialog-confirm',
  shadow: false,
})
export class NvDialogConfirm {

  show!: boolean;

  constructor() {
    this.show = false;
  }

  /**
   * Reference to the host element
   */
  @Element() el: HTMLNvDialogConfirmElement;

  //
  // Props
  //

  /**
   * The header title for the dialog.
   */
  @Prop() headerTitle?: boolean = false;

  /**
   * If `true`, the dialog is displayed.
   */
  @Prop() opened?: boolean = false;

  /**
   * The confirmation button text for this dialog
   */
  @Prop() textConfirm: string;

  /**
   * The cancel button text for this dialog
   */
  @Prop() textCancel: string;

  /**
   * The color for the confirm button
   */
  @Prop() color!: Color;

  //
  // Lifecycles
  //

  componentDidLoad() {
    const dialog = this.el;
    if (dialog) {
      // Add Watch events
      dialog.addEventListener('transitionend', (e: Event) => this.watchEvents(e));
    }
  }

  //
  // Methods
  //

  @Method()
  async close() {
    const dialog = this.el;
    dialog.classList.add('closing');
    this.show = false;
  }

  @Method()
  async toggle() {
    if (this.show) {
      this.close();
      return;
    }

    this.open();
  }

  @Method()
  async confirm() {
    this.close();
  }

  @Method()
  async open() {
    const dialog = this.el;
    dialog.style.display = 'flex';

    // Wait a frame once display is no longer "none", to establish basis for animation
    this.runNextAnimationFrame(() => {
      // no operation during flying and hiding
      if (dialog.classList.contains('flying') ||
          dialog.classList.contains('hiding')) {
        return;
      }

      // If we get focus when we are closing the group, we need to cancel the closing
      // state.
      dialog.classList.remove('closed');
      dialog.classList.remove('closing');
      dialog.classList.add('opening');
      this.show = true;
    });
  }

  private watchEvents(evt): void {
    const dialog = this.el;
    if (evt.type === 'transitionend') {

      // We only process 'opacity' because all states have this
      // change.
      if (evt.propertyName !== 'opacity') {
        return;
      }

      if (dialog.classList.contains('opening')) {
        dialog.classList.add('opened');
        dialog.classList.remove('opening');
      }  else if (dialog.classList.contains('flying')) {
        dialog.classList.remove('flying');
        this.open();
      }

      // go back to initial state after hiding
      if (dialog.classList.contains('hiding') || dialog.classList.contains('closing')) {
        dialog.classList.remove('opened');
        dialog.classList.remove('opening');
        dialog.classList.remove('closing');
        dialog.classList.remove('flying');
        dialog.classList.remove('hiding');
        dialog.classList.add('closed');
        dialog.style.display = 'none';
      }
    }
  }

  //
  // Utils
  //

  private runNextAnimationFrame(callback) {
    requestAnimationFrame(() => {
      setTimeout(callback, 0);
    });
  }

  private makeFooter(): HTMLElement | null {
    return (
      <Fragment>
        {/* Cancel */}
        <nv-button
          text
          disabled-ripple
          onClick={ () => this.close() }>
            <span>{ this.textCancel }</span>
        </nv-button>

        {/* Confirm */}
        <nv-button
          text
          disabled-ripple
          color={this.color}
          onClick={ () => this.confirm() }>
            <span>{ this.textConfirm }</span>
        </nv-button>
        <div class='nv-dialog-footer-divider'/>

      </Fragment>
    );
  }

  render() {
    const footer = this.makeFooter();

    return (
      <Host
        class={{
          'nv-dialog': true,
          closed: !this.opened,
        }}
        style={{ display: !this.opened ? 'none' : null }}
        >
          <div class='nv-dialog-background' onClick={ () => this.close() }></div>
          <div class='nv-dialog-container'>

            <section class='nv-dialog-body'>
              <slot></slot>
            </section>

            <footer class='nv-dialog-footer'>
              { footer }
            </footer>
        </div>
      </Host>
    );
  }
}
