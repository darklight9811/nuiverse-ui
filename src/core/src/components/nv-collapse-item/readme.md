# nv-collapse-item



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute   | Description                                                     | Type      | Default |
| ----------- | ----------- | --------------------------------------------------------------- | --------- | ------- |
| `accordion` | `accordion` | This props is used by `nv-collapse` to apply a accordion login. | `boolean` | `false` |
| `disabled`  | `disabled`  | If `true`, the user cannot interact with the callapse item.     | `boolean` | `false` |


## Methods

### `close() => Promise<void>`

Call this method for te close this collapse.

#### Returns

Type: `Promise<void>`



### `toggleContent() => Promise<void>`

Call this method for the open or close this collapse.

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
