/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Element,
  Prop,
  State,
  Listen,
  Method }          from '@stencil/core';
import Ripple       from '../../utils/ripple';

@Component({
  tag: 'nv-collapse-item',
  shadow: false,
})
export class NvCollapseItem {

  /**
   * Reference to the host element
   */
  @Element() el: HTMLNvCollapseItemElement;

  //
  // State
  //

  @State() open: boolean                        = false;
  @State() contentScrollHeight: string | number = 0;
  @State() maxHeight: string                    = '0px';
  @State() rootEl: HTMLNvCollapseElement | null;

  //
  // Lifecycles
  //

  componentWillLoad () {
    this.rootEl = this.el.parentElement.parentElement as HTMLNvCollapseElement;
  }

  componentDidLoad() {
    this.contentScrollHeight = this.el.querySelector('.nv-collapse-item-container').scrollHeight;
    const header: HTMLElement = this.el.querySelector('header');
    // Apply Ripple effect to header
    Ripple(header);

    if (this.open) {
      this.maxHeight = `${this.contentScrollHeight}px`;
    }
  }

  //
  // Props
  //

  /**
   * This props is used by `nv-collapse` to apply an accordion login.
   */
  @Prop() accordion: boolean  = false;

  /**
   * If `true`, the user cannot interact with the collapse item.
   */
  @Prop() disabled: boolean   = false;

  //
  // Events
  //

  @Listen('resize', { target: 'window' })
  handleResize() {
    this.changeHeight();
  }

  //
  // Methods
  //

  private changeHeight() {
    const height = this.contentScrollHeight;

    if (this.maxHeight !== '0px') {
      this.maxHeight = `${height}px`;
    }
  }

  /**
   * Call this method to close this collapse.
   */
  @Method()
  async close() {
    this.maxHeight = '0px';
    this.open = false;
  }

  /**
   * Call this method to open or close this collapse.
   */
  @Method()
  async toggleContent() {
    if (this.disabled) {
      return;
    }

    if (this.accordion) {
      this.rootEl.closeAllItems(this.el);
    }

    const height = this.contentScrollHeight;

    if (this.maxHeight === '0px') {
      this.maxHeight = `${height}px`;
      this.open = true;
    } else {
      this.maxHeight = `0px`;
      this.open = false;
    }
  }

  render() {
    return (
      <div class={{
        'nv-collapse-item': true,
        disabled: this.disabled,
      }}>
        {/* Header */}
        <header onClick={ () => this.toggleContent() }>
          <div class='header-container'>
            <slot name='header'/>
          </div>
        </header>
        {/* Content */}
        <div
          class={{
            'nv-collapse-item-container': true,
            show: this.open,
          }}
          style={{ maxHeight: this.maxHeight }}>
          <slot name='content' />
        </div>
      </div>
    );
  }
}
