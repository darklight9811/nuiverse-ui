# nv-alert



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                    | Type                                                             | Default     |
| -------- | --------- | ------------------------------ | ---------------------------------------------------------------- | ----------- |
| `active` | `active`  | If `true` the alert is active. | `boolean`                                                        | `true`      |
| `color`  | `color`   | The color of the alert.        | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
