/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host } from '@stencil/core';
import { Color }              from '../../interface';

@Component({
  tag: 'nv-alert',
  shadow: false,
})
export class NvAlert {
   /**
   * If `true` the alert is active.
   */
  @Prop() active?: boolean = true;

  /**
   * The color of the alert.
   */
  @Prop() color?: Color;

  render() {

    if (this.active) {
      return (
        <Host class={{
          'nv-alert': true,
          'nv-alert-primary': this.color === 'primary' ? true : false,
          'nv-alert-secondary': this.color === 'secondary' ? true : false,
          'nv-alert-warning': this.color === 'warning' ? true : false,
          'nv-alert-danger': this.color === 'danger' ? true : false,
          'nv-alert-success': this.color === 'success' ? true : false,
        }}>
          <slot />
        </Host>
      );
    }
  }
}
