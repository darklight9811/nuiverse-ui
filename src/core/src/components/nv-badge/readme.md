# nv-badge



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                            | Type                                                             | Default     |
| -------- | --------- | -------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `color`  | `color`   | The color for the badge.               | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `pill`   | `pill`    | if `true`, the badge has a pill style. | `boolean`                                                        | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
