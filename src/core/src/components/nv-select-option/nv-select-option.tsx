/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host, Event, EventEmitter, Element, Prop, Listen } from '@stencil/core';
import { SelectOptionSelectedDetail, SelectChangeDetail } from '../../interface';

@Component({
  tag: 'nv-select-option',
  shadow: false,
})
export class NvSelectOption {
  //
  // Reference to the host element.
  //

  @Element() el: HTMLNvSelectOptionElement;

  //
  // Props
  //

  /**
   * The text value of the option.
   */
  @Prop({ mutable: true }) value: any;

  /**
   * If `true`, the select option is selected.
   */
  @Prop({ mutable: true }) selected: boolean = false;

  /**
   * If `true`, the user cannot interact with the select option.
   */
  @Prop({ mutable: true }) disabled: boolean = false;

  /**
   * The text for the select label.
   */
  @Prop({ mutable: true }) textLabel: string | undefined;

  //
  // Events
  //

  /**
   * Emitted when this option is selected.
   * @intenal
   */
  @Event() nvSelectOptionSelect: EventEmitter<SelectOptionSelectedDetail>;

  @Listen('nvChange', { target: 'parent' })
  customEventHandler(event: CustomEvent<SelectChangeDetail>) {
    if (this.el !== event.detail.option && !event.detail.multiple) {
      this.selected = false;
    }
  }

  //
  // Methods
  //

  private onClick = (e: MouseEvent | TouchEvent): void => {
    e.preventDefault();
    if (this.selected) {
      this.selected = false;
      this.nvSelectOptionSelect.emit({
        option: this.el,
      });
      return;
    }
    this.selected = true;
    this.nvSelectOptionSelect.emit({
      option: this.el,
    });
  }

  render() {
    return (
      <Host
        class={{
          'nv-select-item': true,
          'nv-select-item-selected': this.selected,
          'nv-select-item-disabled': this.disabled,
        }}
        onMouseDown={ !this.disabled ? this.onClick : null }
      >
        <slot/>
      </Host>
    );
  }
}
