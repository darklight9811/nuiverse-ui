# nv-select-option



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                                                 | Type      | Default     |
| ----------- | ------------ | ----------------------------------------------------------- | --------- | ----------- |
| `disabled`  | `disabled`   | If `true`, the user cannot interact with the select option. | `boolean` | `false`     |
| `selected`  | `selected`   | If `true`, the select option is selected.                   | `boolean` | `false`     |
| `textLabel` | `text-label` | The text for the select label.                              | `string`  | `undefined` |
| `value`     | `value`      | The text value of the option.                               | `any`     | `undefined` |


## Events

| Event                  | Description                            | Type                                      |
| ---------------------- | -------------------------------------- | ----------------------------------------- |
| `nvSelectOptionSelect` | Emitted when this option has selected. | `CustomEvent<SelectOptionSelectedDetail>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
