# nv-radio-group



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                    | Type     | Default     |
| -------- | --------- | -------------------------------------------------------------- | -------- | ----------- |
| `name`   | `name`    | The name of the control, which is submitted with the form data | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
