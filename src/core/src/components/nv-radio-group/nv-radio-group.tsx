/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host, Element } from '@stencil/core';

@Component({
  tag: 'nv-radio-group',
  shadow: false,
})
export class NvRadioGroup {
  /**
   * Reference to the Host element.
   */
  @Element() el: HTMLNvRadioGroupElement;

  /**
   * The name of the control, which is submitted with the form data
   */
  @Prop() name?: string;

  private onSelect = (ev: Event) => {
    this.updateRadios(ev);
  }

  //
  // Lifecyles
  //
  componentDidLoad() {
    this.addName();
  }

  //
  // Methods
  //

  private async updateRadios(ev?: Event): Promise<void> {
    const radios: NodeListOf<HTMLNvRadioElement> = this.el.querySelectorAll('nv-radio');
    radios.forEach((r: HTMLNvRadioElement) => {
      console.log(r);
      if (r !== ev.target) {
        r.checked = false;
      }
    });
  }

  private async addName() {
    const radios: NodeListOf<HTMLInputElement> = this.el.querySelectorAll('input[type="radio"]');
    if (this.name) {
      radios.forEach((r: HTMLInputElement) => {
        r.name = this.name;
      });
    }
  }

  render() {
    return (
      <Host
        role='radiogroup'
        onNvSelect={ this.onSelect }
        class={{
          'nv-radio-group' : true,
        }}
      >
        <slot />
      </Host>
    );
  }
}
