/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  Prop,
  h,
  Event,
  EventEmitter,
  Watch,
  Host,
  State,
} from '@stencil/core';
import {
  CheckboxChangeEventDetail,
  Color,
} from '../../interface';
@Component({
  tag: 'nv-checkbox',
  shadow: false,
})
export class NvCheckbox {

  //
  // State
  //

  @State() el!: HTMLInputElement;

  //
  // Props
  //

  /**
   * If `true`, the user cannot interact with the checkbox.
   */
  @Prop() disabled?: boolean = true;

   /**
   * If `true` the checkbox is selected.
   */
  @Prop({ mutable: true }) checked!: boolean;

  /**
   * If `true` the checkbox will have a indeterminate visual.
   */
  @Prop() indeterminate!: boolean;

  /**
   * The name of the control, which is submitted with the form data
   */
  @Prop() name!: string;

  /**
   * The color to use from your application color palette.
   */
  @Prop() color!: Color;

  //
  // Events
  //

  /**
   * Emitted when the checkbox is selected.
   */
  @Event() nvSelect!: EventEmitter<CheckboxChangeEventDetail>;

  /**
   * Emitted when the checkbox is deselected.
   */
  @Event() nvDeselected!: EventEmitter<CheckboxChangeEventDetail>;

  //
  // Watch
  //

  @Watch('checked')
  handleChecked(isChecked: boolean) {
    if (isChecked) {
      this.nvSelect.emit({
        checked: true,
      });
    }
  }

  //
  // Methods
  //

  private updateCheck = () => {
    if (this.checked) {
      this.checked = false;
      this.nvDeselected.emit();
    } else {
      this.el.checked = true;
      this.checked = true;
    }
  }

  private makeIcon(): SVGElement {
    if (this.indeterminate) {
      return (
        <svg class='nv-checkbox-icon' viewBox='0 0 30 30'>
          <g transform='translate(0 -289.06)'>
            <path
              transform='translate(0 289.06)'
              d='m4.4668 14c-0.030724 0.32965-0.050781 0.66211-0.050781
              1s0.020057 0.67035 0.050781 1h21.066c0.030725-0.32965
              0.050781-0.66211 0.050781-1s-0.020056-0.67035-0.050781-1h-21.066z'
            />
          </g>
        </svg>
      );
    }

    return (
      <svg class='nv-checkbox-icon' viewBox='0 0 30 30'>
        <g transform='translate(0 -283.15)'>
          <path
            transform='translate(0 283.15)'
            d='m27 6.0195-15.932 18.988-8.0684-6.7695v2.6094l6.7832
            5.6914c0.83553 0.70109 2.1173 0.58944 2.8184-0.24609l14.398-17.16v-3.1133z'
          />
        </g>
      </svg>
    );
  }

  render() {
    const icon = this.makeIcon();
    return (
      <Host
        class={{
          'nv-checkbox' : true,
          'nv-checkbox-checked' : this.checked,
          'nv-checkbox-primary': this.color === 'primary',
          'nv-checkbox-secondary': this.color === 'secondary',
          'nv-checkbox-warning': this.color === 'warning',
          'nv-checkbox-danger': this.color === 'danger',
          'nv-checkbox-success': this.color === 'success',
          'nv-checkbox-indeterminate': this.indeterminate,
        }}
      onClick={ this.updateCheck }
      >
        <div class='nv-checkbox-container'>
          <input
            ref={ el => this.el = el }
            type='checkbox'
            name={ this.name }
            checked={ this.checked }
          />
          { icon }
        </div>
      </Host>
    );
  }
}
