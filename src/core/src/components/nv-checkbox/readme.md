# nv-checkbox



<!-- Auto Generated Below -->


## Properties

| Property                     | Attribute       | Description                                                    | Type                                                             | Default     |
| ---------------------------- | --------------- | -------------------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `checked` _(required)_       | `checked`       | If `true` the checkbox is selected.                            | `boolean`                                                        | `undefined` |
| `color` _(required)_         | `color`         | The color to use from your application color palette.          | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `disabled`                   | `disabled`      | If `true`, the user cannot interecat with the checkbox.        | `boolean`                                                        | `true`      |
| `indeterminate` _(required)_ | `indeterminate` | If `true` the checkbox will have a indeterminate visual.       | `boolean`                                                        | `undefined` |
| `name` _(required)_          | `name`          | The name of the control, which is submitted with the form data | `string`                                                         | `undefined` |


## Events

| Event          | Description                              | Type                                     |
| -------------- | ---------------------------------------- | ---------------------------------------- |
| `nvDeselected` | Emitted when the checkbox is deselected. | `CustomEvent<CheckboxChangeEventDetail>` |
| `nvSelect`     | Emitted when the checkbox is selected.   | `CustomEvent<CheckboxChangeEventDetail>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
