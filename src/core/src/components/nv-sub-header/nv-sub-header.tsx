/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element, Prop } from '@stencil/core';

@Component({
  tag: 'nv-sub-header',
  shadow: false,
})
export class NvSubHeader {
  /**
   * Reference to the host element
   */
  @Element() el: HTMLElement;

  /**
   * The aria-level.
   */
  @Prop() level?: number = 1;

  render() {
    return (
      <div class='nv-sub-header' aria-level={ this.level }>
        <div class='nv-sub-header-line'/>
        <div class='nv-sub-header-text'>
          <slot />
        </div>
        <div class='nv-sub-header-line'/>
      </div>
    );
  }
}
