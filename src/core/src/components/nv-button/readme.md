# nv-button



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute         | Description                                                                                                    | Type                                                             | Default     |
| ---------------- | ----------------- | -------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `block`          | `block`           | If `true`, this property change the width of the button for full.                                              | `boolean`                                                        | `undefined` |
| `color`          | `color`           | The button color.                                                                                              | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `dataFor`        | `data-for`        | The data for action for the button, this attribute is used for find Nuiverse components and display or hidden. | `string`                                                         | `undefined` |
| `disabled`       | `disabled`        | The button enable status.                                                                                      | `boolean`                                                        | `false`     |
| `disabledRipple` | `disabled-ripple` | The button ripple effect enable status                                                                         | `boolean`                                                        | `false`     |
| `outline`        | `outline`         | If `true`, this property apply a outline style for the button.                                                 | `boolean`                                                        | `undefined` |
| `rounded`        | `rounded`         | If `true`, this property apply rounded borders for the button.                                                 | `boolean`                                                        | `undefined` |
| `size`           | `size`            | This property specifies the size of the button.                                                                | `"big" \| "large" \| "medium" \| "small"`                        | `'medium'`  |
| `text`           | `text`            | The text style                                                                                                 | `boolean`                                                        | `undefined` |
| `textColor`      | `text-color`      | The text color of the button.                                                                                  | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `type`           | `type`            | Type of the button.                                                                                            | `"button" \| "reset" \| "submit"`                                | `'button'`  |


## Dependencies

### Used by

 - [nv-dialog-action](../nv-dialog-action)
 - [nv-dialog-alert](../nv-dialog-alert)
 - [nv-dialog-confirm](../nv-dialog-confirm)
 - [nv-dialog-prompt](../nv-dialog-prompt)
 - [nv-dialog-select](../nv-dialog-select)
 - [nv-tab-button](../nv-tab-button)
 - [nv-text-field](../nv-text-field)
 - [nv-upload](../nv-upload)

### Graph
```mermaid
graph TD;
  nv-dialog-action --> nv-button
  nv-dialog-alert --> nv-button
  nv-dialog-confirm --> nv-button
  nv-dialog-prompt --> nv-button
  nv-dialog-select --> nv-button
  nv-tab-button --> nv-button
  nv-text-field --> nv-button
  nv-upload --> nv-button
  style nv-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
