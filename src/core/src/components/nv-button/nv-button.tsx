/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Element, Host } from '@stencil/core';
import Ripple                                from '../../utils/ripple';
import { PredefinedButtonType, PredefinedGlobalSize, Color }  from '../../interface';

interface IcustomElement extends HTMLElement{
  toggle?: () => void;
}

@Component({
  tag: 'nv-button',
  shadow: false,
})
export class NvButton {

  /**
   *  Reference to the host element
   */
  @Element() el: HTMLElement;

  componentDidLoad() {
    // Get button element
    const btn: HTMLElement = this.el.querySelector('button');
    if (!this.disabledRipple) {
      // Add ripple effect
      Ripple(btn);
    }
  }

  //
  // Props
  //

  /**
   * The text color of the button.
   */
  @Prop() textColor?: Color;

  /**
   * The button color.
   */
  @Prop() color?: Color;

  /**
   * This property specifies the size of the button.
   */
  @Prop() size?: PredefinedGlobalSize = 'medium';

  /**
   * Type of the button.
   */
  @Prop() type?: PredefinedButtonType = 'button';

  /**
   * The text style
   */
  @Prop() text: boolean;

  /**
   * If `true`, this property will change the width of the button for full.
   */
  @Prop() block: boolean;

  /**
   * If `true`, this property will apply rounded borders for the button.
   */
  @Prop() rounded: boolean;

  /**
   * If `true`, this property will apply an outline style for the button.
   */
  @Prop() outline: boolean;

  /**
   * The button enable status.
   */
  @Prop() disabled?: boolean = false;

  /**
   * The button ripple effect enable status
   */
  @Prop() disabledRipple?: boolean = false;

  /**
   * The data's action for the button, this attribute is used to find Nuiverse components
   * and display or hide.
   */
  @Prop() dataFor: string;

  private actionData(): void {
    const data = this.dataFor;
    const element = document.querySelector(data) as IcustomElement;
    if (element) {

      // Dialogs
      if (element.tagName.match(/NV-DIALOG/)) {
        element.toggle();
      }

    }
  }

  render() {
    return (
      <Host
        class={{
          'nv-button': true,
          'nv-button-block': this.block,
        }}
      >
        <button
          class={{
            'nv-button-native': true,
            'nv-button-small': this.size === 'small' ? true : false,
            'nv-button-medium': this.size === 'medium' ? true : false,
            'nv-button-large': this.size === 'large' ? true : false,
            'nv-button-big': this.size === 'big' ? true : false,
            'nv-button-primary': this.color === 'primary' ? true : false,
            'nv-button-secondary': this.color === 'secondary' ? true : false,
            'nv-button-warning': this.color === 'warning' ? true : false,
            'nv-button-danger': this.color === 'danger' ? true : false,
            'nv-button-success': this.color === 'success' ? true : false,
            'nv-button-text': this.text,
            'nv-button-rounded': this.rounded,
            'nv-button-outline': this.outline,
          }}
          type={ this.type }
          disabled={this.disabled}
          onClick={ this.dataFor ? () => this.actionData() : null }>
            <span><slot /></span>
          </button>
        </Host>
    );
  }
}
