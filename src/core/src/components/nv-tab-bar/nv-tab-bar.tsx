/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Prop,
  Watch,
  Event,
  EventEmitter,
  Host,
}                                     from '@stencil/core';
import { NvTabBarChangedEventDetail } from '../../interface';

@Component({
  tag: 'nv-tab-bar',
  shadow: false,
})
export class NvTabBar {

  //
  // Props
  //

  /**
   * The selected tab component
   */
  @Prop() selectedTab?: string;

  //
  // Events
  //

  /** @internal */
  @Event() nvTabBarChanged!: EventEmitter<NvTabBarChangedEventDetail>;

  //
  // Lifecycles
  //

  componentWillLoad() {
    this.selectedTabChanged();
  }

  //
  // Watchs
  //
  @Watch('selectedTab')
  selectedTabChanged() {
    if (this.selectedTab !== undefined) {
      this.nvTabBarChanged.emit({
        tab: this.selectedTab,
      });
    }
  }

  render() {
    return (
      <Host class='nv-tab-bar'>
        <slot />
      </Host>
    );
  }
}
