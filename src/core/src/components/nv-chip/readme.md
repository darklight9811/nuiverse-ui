# nv-chip



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description                                 | Type      | Default     |
| --------- | --------- | ------------------------------------------- | --------- | ----------- |
| `rounded` | `rounded` | If `true` the chip will have a rouded style | `boolean` | `false`     |
| `src`     | `src`     | The src from the avatar                     | `string`  | `undefined` |
| `text`    | `text`    | The text from the avatar                    | `string`  | `undefined` |


## Dependencies

### Depends on

- [nv-avatar](../nv-avatar)

### Graph
```mermaid
graph TD;
  nv-chip --> nv-avatar
  style nv-chip fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
