/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element, Prop } from '@stencil/core';
import Ripple                          from '../../utils/ripple';

@Component({
  tag: 'nv-chip',
  shadow: false,
})
export class NvChip {
  /**
   * Reference to the host element
   */
  @Element() el: HTMLNvChipElement;

  /**
   * The src of the avatar
   */
  @Prop() src: string;

  /**
   * The text of the avatar
   */
  @Prop() text: string;

  /**
   * If `true` the chip will have a rounded style
   */
  @Prop() rounded: boolean = false;

  //
  // Lifecyles
  //

  componentDidLoad() {
    const chip: HTMLElement = this.el.querySelector('.nv-chip');
    Ripple(chip);
  }

  render() {
    return (
      <div class={{
        'nv-chip': true,
        'nv-chip-rounded': this.rounded,
      }}>
        <div class='nv-chip-avatar'>
          <nv-avatar src={ this.src } text={ this.text }/>
        </div>
        <span class='nv-chip-title'>
          <slot />
        </span>
      </div>
    );
  }
}
