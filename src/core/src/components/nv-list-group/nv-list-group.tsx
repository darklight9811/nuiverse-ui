/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element } from '@stencil/core';

@Component({
  tag: 'nv-list-group',
  shadow: false,
})
export class NvListGroup {
  @Element() el: HTMLElement;

  componentDidLoad() {
    const lastChild = this.el.querySelector('.nv-list-group > :last-child > div');
    if (lastChild) {
      lastChild.classList.add('last-child');
    }
  }

  render() {
    return (
      <div class='nv-list-group'>
        <slot />
      </div>
    );
  }
}
