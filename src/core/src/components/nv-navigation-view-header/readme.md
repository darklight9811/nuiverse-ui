# nv-navigation-view-header



<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [nv-navigation-view-items](../nv-navigation-view-items)

### Graph
```mermaid
graph TD;
  nv-navigation-view-items --> nv-navigation-view-header
  style nv-navigation-view-header fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
