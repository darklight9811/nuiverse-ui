/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host } from '@stencil/core';

@Component({
  tag: 'nv-navigation-view-header',
  shadow: false,
})
export class NvNavigationItemHeader {
  render() {
    return (
      <Host class='nv-nav-item-header'>
        <header>
          <h6 class='nv-nav-title'>
            <slot/>
          </h6>
        </header>
      </Host>
    );
  }
}
