/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Host,
  Prop,
  State,
  Listen,
  Method,
  Event,
  EventEmitter,
  Element,
} from '@stencil/core';
import {
  SelectOptionSelectedDetail,
  SelectChangeDetail,
  SelectedValues,
} from '../../interface';

@Component({
  tag: 'nv-select',
  shadow: false,
})
export class NvSelect {
  //
  // Reference to the host element.
  //

  @Element() el: HTMLNvSelectElement;

  //
  // State
  //

  @State() open: boolean = false;
  @State() selectedValues: SelectedValues[] = [];
  @State() options: HTMLNvSelectOptionElement[] = [];

  //
  // Props
  //

  /**
   * The text of the label for the select.
   */
  @Prop() textLabel?: string | undefined;

  /**
   * The name of the input, which is submitted with the form data.
   */
  @Prop() name?: string | undefined;

  /**
   * The value of the select.
   */
  @Prop() value?: string | string[] | undefined;

  /**
   * If `true`, the user cannot interact with the select.
   */
  @Prop() disabled?: boolean = false;

  /**
   * If `true`, the select can accept multiple values.
   */
  @Prop() multiple?: boolean = false;

  //
  // Lifecyles
  //

  componentDidLoad() {
    this.options = Array.from(this.el.querySelectorAll('nv-select-option'));
    if (this.value && typeof this.value === 'string') {
      this.initSelect().then(() => {
        this.options.forEach((option: HTMLNvSelectOptionElement): void => {
          if (option.value === this.value) {
            option.selected = true;
            this.getValues();
          }
        });
      });
    }
  }

  //
  // Events
  //

  @Listen('nvSelectOptionSelect')
  nvSelectOptionSelectHandler(event: CustomEvent<SelectOptionSelectedDetail>) {
    if (this.multiple) {
      this.nvChange.emit({
        option: event.detail.option,
        multiple: true,
      });
      this.getValues();
    } else {
      this.nvChange.emit({
        option: event.detail.option,
        multiple: false,
      });
      this.getValues();
      this.toggle();
    }
  }

  /**
   * Emitted when the value has changed.
   */
  @Event({
    eventName: 'nvChange',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) nvChange: EventEmitter<SelectChangeDetail>;

  //
  // Methods
  //

  /**
   * Close or open the select.
   */
  @Method()
  async toggle() {
    if (!this.disabled) {
      if (this.open) {
        this.open = false;
        this.removeEvent();
        return;
      }
      this.open = true;
      this.addEvent();
    }
  }

  private onClick = (): void => {
    this.toggle();
  }

  private addEvent (): void {
    if (!this.disabled) {
      window.addEventListener('keydown', this.onKeyDown);
      window.addEventListener('mousedown', this.onStart);
    }
  }

  private removeEvent(): void {
    if (!this.disabled) {
      window.removeEventListener('keydown', this.onKeyDown);
      window.removeEventListener('mousedown', this.onStart);
    }
  }

  private onKeyDown = (e: KeyboardEvent): void => {
    if (e.key === 'Escape') {
      this.toggle();
    }
  }

  private onStart = (): void  => {
    this.toggle();
  }

  private getValues(): void {
    const selectedValues: SelectedValues[] = [];
    const value: string[] = [];

    this.options.forEach((option) => {
      if (option.selected) {
        selectedValues.push({
          label: option.textLabel ? option.textLabel : option.innerText,
          value: option.value || undefined,
        });
        value.push(option.value || undefined);
      }
    });

    this.selectedValues = selectedValues;
    this.value = value;
  }

  private makeNativeSelect(): HTMLSelectElement | null {
    const options: HTMLOptionElement[] = [];
    if (this.name) {
      this.selectedValues.forEach((values) => {
        options.push(<option value={ values.value } selected={ true }/>);
      });

      return (
        <select name={ this.name } hidden disabled={ this.disabled }>
          { options }
        </select>
      );
    }
  }

  private async initSelect(): Promise<void> {
    // wait for all navItems to be ready.
    await Promise.all(this.options.map(item => item.componentOnReady()));
  }

  private getLabel(): string {
    const selectedValues = this.selectedValues;
    let label: string = '';

    if (!this.value || this.value.length <= 0) {
      label = this.textLabel;
    } else {
      label = [].map.call(selectedValues, (content: SelectedValues) => {
        return content.label;
      }).join(', ');
    }

    return label;
  }

  render() {
    const label = this.getLabel();

    const nativeSelect = this.makeNativeSelect();
    return (
      <Host
        class={{
          'nv-select': true,
          'nv-select-opened': this.open,
          'nv-select-closed': !this.open,
          'nv-select-disabled': this.disabled,
        }}
      >
        <div class='nv-select-el' onClick={ this.onClick }>
          <span class='nv-select-label'>{ label }</span>
        </div>
        <div class='nv-select-items'>
          <slot/>
        </div>
        { nativeSelect }
      </Host>
    );
  }
}
