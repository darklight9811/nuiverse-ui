# nv-select



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                                                   | Type                 | Default     |
| ----------- | ------------ | ------------------------------------------------------------- | -------------------- | ----------- |
| `disabled`  | `disabled`   | If `true`, the user cannot interact with the select.          | `boolean`            | `false`     |
| `multiple`  | `multiple`   | If `true`, the select can accept multiple values.             | `boolean`            | `false`     |
| `name`      | `name`       | The name of the input, which is submitted with the form data. | `string`             | `undefined` |
| `textLabel` | `text-label` | The text of the label for the select.                         | `string`             | `undefined` |
| `value`     | `value`      | The value of the select.                                      | `string \| string[]` | `undefined` |


## Events

| Event      | Description                         | Type                              |
| ---------- | ----------------------------------- | --------------------------------- |
| `nvChange` | Emitted when the value has changed. | `CustomEvent<SelectChangeDetail>` |


## Methods

### `toggle() => Promise<void>`

Close or open the select.

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
