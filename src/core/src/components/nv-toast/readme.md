# nv-toast



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute        | Description                                                  | Type      | Default     |
| -------------- | ---------------- | ------------------------------------------------------------ | --------- | ----------- |
| `autoClose`    | `auto-close`     | If `true`, after the `duration`, the toast will have closed. | `boolean` | `true`      |
| `closeOnClick` | `close-on-click` | If `true`, the toast will have closed on click.              | `boolean` | `true`      |
| `duration`     | `duration`       | The display duration in milliseconds of the alert.           | `number`  | `4000`      |
| `showProgress` | `show-progress`  | If `true`, the toast will have displayed the progress bar.   | `boolean` | `false`     |
| `textHeader`   | `text-header`    | The text header for the toast.                               | `string`  | `undefined` |


## Methods

### `close() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `destroy() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`



#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
