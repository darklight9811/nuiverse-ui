/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Element,
  Prop,
  Listen,
  Host,
  Method,
  State,
}                                  from '@stencil/core';
import { runNextAnimationFrame }   from '../../utils';

@Component({
  tag: 'nv-toast',
  shadow: false,
})
export class NvToast {
  public progress: HTMLDivElement;
  /**
   * Reference to the host element
   */
  @Element() el: HTMLElement;

  //
  // State
  //

  @State() hovered?: boolean = false;

  /**
   * The text header for the toast.
   */

  @Prop() textHeader?: string;

  /**
   * If `true`, the toast will display the progress bar.
   */

  @Prop() showProgress?: boolean = false;

  /**
   * The display duration in milliseconds of the alert.
   */

  @Prop() duration?: number = 4000;

  /**
   * If `true`, after the `duration`, the toast will be closed.
   */

  @Prop() autoClose: boolean = true;

  /**
   * If `true`, the toast will be closed on click.
   */

  @Prop() closeOnClick: boolean = true;

  //
  // Events
  //

  /**
   * Used to detect animation's end and close the toast.
   * @internal
   */
  @Listen('animationend', { target: this.progress })
  protected handleAnimEnd() {
    if (this.hovered) {
      return;
    }
    this.close();
  }

  /**
   * @internal
   * Used to detect the transition's end for the display toast.
   */
  @Listen('transitionend', { target: this.el })
  protected handleTranEnd(evt: TransitionEvent) {
    if (evt.type === 'transitionend') {

      if (evt.propertyName !== 'right') {
        return;
      }

      if (this.el.classList.contains('nv-toast-flying')) {
        this.el.classList.remove('nv-toast-flying');
        this.el.classList.add('nv-toast-opened');
      }

      // go back to the initial state after hiding
      if (this.el.classList.contains('nv-toast-hiding') ||
      this.el.classList.contains('nv-toast-closing')) {

        this.el.classList.remove('nv-toast-opened');
        this.el.classList.remove('nv-toast-opening');
        this.el.classList.remove('nv-toast-closing');
        this.el.classList.remove('nv-toast-flying');
        this.el.classList.remove('nv-toast-hiding');
        this.el.classList.add('nv-toast-closed');
        this.destroy();
      }
    }
  }

  //
  // Methods
  //
  @Method()
  async close(): Promise<void> {
    this.el.classList.add('nv-toast-hiding');
  }

  @Method()
  async destroy(): Promise<void> {
    this.el.remove();
  }

  @Method()
  async open(): Promise<void> {
    this.el.classList.remove('nv-toast-closed');
    this.el.classList.remove('nv-toast-closing');

    // Wait a frame once display is no longer "none", to establish basis for the animation
    runNextAnimationFrame(() => {
      // no operation during flying and hiding
      if (this.el.classList.contains('nv-toast-flying') ||
          this.el.classList.contains('nv-toast-hiding')) {
        return;
      }

      this.el.classList.add('nv-toast-flying');
    });
  }

  private makeProgress(): HTMLDivElement | null {
    if (this.autoClose) {
      return (
        <div
          ref={ el => this.progress = el }
          class='nv-toasts-progress'
          style={{
            animationDuration: `${this.duration}ms`,
          }}
        ></div>
      );
    }
  }

  private closeClick = () => {
    if (this.closeOnClick) {
      this.close();
    }
  }

  private onHover = () => {
    this.hovered = true;
  }

  private onLeave = () => {
    this.hovered = false;
  }

  render() {
    const progress = this.makeProgress();
    return (
      <Host
        onClick={ this.closeClick }
        class={{
          'nv-toast': true,
          'nv-toast-closed': true,
          'nv-toast-show-progress': this.showProgress,
          'nv-toast-hovered' : this.hovered,
        }}
        onMouseEnter={ this.onHover }
        onMouseLeave={ this.onLeave }
      >
        <h4>{ this.textHeader }</h4>
        <slot />
        { progress }
      </Host>
    );
  }
}
