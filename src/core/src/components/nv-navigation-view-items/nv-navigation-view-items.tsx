/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Host,
  Prop,
  Listen,
  Event,
  EventEmitter,
  Element,
  State,
} from '@stencil/core';
import {
  NvNavViewItemsDidChange,
  NvNavViewItemDidChange,
} from '../../interface';

@Component({
  tag: 'nv-navigation-view-items',
  shadow: false,
})
export class NvNavigationItems {
  //
  // Reference to the host elemenet.
  //

  @Element() el: HTMLNvNavigationViewItemsElement;

  //
  // States
  //

  @State() open: boolean                        = false;
  @State() contentScrollHeight: string | number = 0;
  @State() maxHeight: string                    = '0px';
  @State() items: HTMLNvNavigationViewItemElement[] = [];

  //
  // Props
  //

  /**
   * The text header for navigation items.
   */
  @Prop() textHeader?: string;

  //
  // Lifecycles
  //

  componentDidLoad() {
    this.items = Array.from(this.el.querySelectorAll('nv-navigation-view-item'));
    this.initSelect();
  }

  //
  // Events
  //

  @Listen('nvNavViewItemDidChange')
  protected nvNavViewItemDidChangeHandler(event: CustomEvent<NvNavViewItemDidChange>) {
    this.nvNavViewItemsDidChange.emit({
      item: event.detail.item,
      items: this.el,
    });

    this.items.forEach((item: HTMLNvNavigationViewItemElement) => {
      if (event.detail.item !== item) {
        item.removeActive();
      }
    });
  }

  @Listen('nvNavViewItemsActive')
  protected nvNavViewItemsActiveHandler(event: CustomEvent<NvNavViewItemsDidChange>) {
    if (event.detail.items !== this.el) {
      this.items.forEach((item: HTMLNvNavigationViewItemElement) => {
        item.removeActive();
      });
    }
  }

  /**
   * Emitted when a item is selected, this is used to remove the
   * active property from other items.
   */
  @Event() nvNavViewItemsDidChange!: EventEmitter<NvNavViewItemsDidChange>;

  //
  // Methods
  //

  private async initSelect(): Promise<void> {
    // wait for all items to be ready
    await Promise.all(this.items.map(tab => tab.componentOnReady()));
  }

  private makeHeader(): HTMLNvNavigationViewHeaderElement | null {
    if (this.textHeader) {
      return (
        <nv-navigation-view-header>
          { this.textHeader }
        </nv-navigation-view-header>
      );
    }
  }

  render() {
    const header = this.makeHeader();
    return (
      <Host
        class={{
          'nv-nav-items': true,
        }}
      >
        { header }
        <section>
          <slot />
        </section>
      </Host>
    );
  }
}
