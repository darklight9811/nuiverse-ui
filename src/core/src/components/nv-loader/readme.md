# nv-loader



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                        | Type                                                             | Default     |
| -------- | --------- | -------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `color`  | `color`   | The color of the loader.                           | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `'primary'` |
| `paused` | `paused`  | If `true`, the snipper's animarion will be paused. | `boolean`                                                        | `false`     |
| `size`   | `size`    | The size of the loader.                            | `"big" \| "large" \| "medium" \| "small"`                        | `'medium'`  |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
