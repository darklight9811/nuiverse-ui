/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host } from '@stencil/core';
import { Color, PredefinedGlobalSize } from '../../interface';

@Component({
  tag: 'nv-loader',
  shadow: false,
})
export class NvLoader {
  /**
   * The color of the loader.
   */
  @Prop() color?: Color = 'primary';

  /**
   * The size of the loader.
   */
  @Prop() size?: PredefinedGlobalSize = 'medium';

  /**
   * If `true`, the snipper's animation will be paused.
   */
  @Prop() paused?: boolean = false;

  render() {
    return (
      <Host
        class={{
          'nv-loader': true,
          'nv-loader-primary': this.color === 'primary' ? true : false,
          'nv-loader-secondary': this.color === 'secondary' ? true : false,
          'nv-loader-warning': this.color === 'warning' ? true : false,
          'nv-loader-danger': this.color === 'danger' ? true : false,
          'nv-loader-success': this.color === 'success' ? true : false,
          'nv-loader-anim-paused': this.paused,
          'nv-loader-small': this.size === 'small' ? true : false,
          'nv-loader-medium': this.size === 'medium' ? true : false,
          'nv-loader-large': this.size === 'large' ? true : false,
          'nv-loader-big': this.size === 'big' ? true : false,
        }}
      >
      </Host>
    );
  }
}
