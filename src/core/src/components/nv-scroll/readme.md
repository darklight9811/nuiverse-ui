# nv-scroll



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description                               | Type                             | Default    |
| --------- | ---------- | ----------------------------------------- | -------------------------------- | ---------- |
| `scrollX` | `scroll-x` | If `true`, the scrollX will be displayed. | `boolean`                        | `false`    |
| `scrollY` | `scroll-y` | If `true`, the scrollY will be displayed. | `boolean`                        | `true`     |
| `size`    | `size`     | The avatar size                           | `"large" \| "medium" \| "small"` | `'medium'` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
