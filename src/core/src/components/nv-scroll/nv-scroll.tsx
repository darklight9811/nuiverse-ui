/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host, Element, State } from '@stencil/core';
import ResizeObserver from 'resize-observer-polyfill';

@Component({
  tag: 'nv-scroll',
  shadow: false,
})
export class NvScroll {
  private content: HTMLDivElement;

  /**
   * Reference to the host element.
   */
  @Element() el: HTMLNvScrollElement;

  //
  // State
  //

  @State() scrollTopMax: number = 0;
  @State() scrollLeftMax: number = 0;
  @State() hover: boolean = false;
  @State() scrollTop: number = 0;
  @State() scrollVTop: number = 0;
  @State() scrollYHeight: number = 0;
  @State() scrollLeft: number = 0;
  @State() scrollVLeft: number = 0;
  @State() scrollXWidth: number = 0;
  @State() scrollActive: 'x' | 'y' | undefined;
  @State() startup: boolean = false;

  //
  // Lificycles
  //

  componentDidLoad() {
    this.getDimensions();

    const resizeObserver = new ResizeObserver(() => {
      this.getDimensions();
    });

    resizeObserver.observe(this.el);
    resizeObserver.observe(this.content);
    this.startup = true;
  }

  //
  // Props
  //

  /**
   * If `true`, the scrollY will be displayed.
   */
  @Prop() scrollY: boolean = true;

  /**
   * If `true`, the scrollX will be displayed.
   */
  @Prop() scrollX: boolean = false;

  /**
   * The avatar size
   */
  @Prop() size?: 'small' | 'medium' | 'large' = 'medium';

  private onOver = (): void => {
    this.hover = true;
  }

  private onLeave = (): void => {
    this.hover = false;
  }

  private onScroll = (e: Event | undefined = undefined): void => {
    const el = e ? e.target as HTMLElement : this.content;
    if (el && !this.scrollActive) {
      const scrollTop = el.scrollTop * 100 / this.scrollTopMax;
      this.scrollVTop = scrollTop * (this.el.offsetHeight - this.scrollYHeight) / 100;
      const scrollLeft = el.scrollLeft * 100 / this.scrollLeftMax;
      this.scrollVLeft = scrollLeft * (this.el.offsetWidth - this.scrollXWidth) / 100;
    }
  }

  private getDimensions(): void {
    const content: HTMLDivElement | null = this.el.querySelector('.nv-scroll-content-container');
    const elHeight: number = this.el.offsetHeight;
    const elWidth: number = this.el.offsetWidth;

    if (content) {
      this.scrollTopMax = content.offsetHeight - elHeight;
      this.scrollYHeight = elHeight * (elHeight / content.offsetHeight);
      this.scrollLeftMax = content.offsetWidth - elWidth;
      this.scrollXWidth = elWidth * (elWidth / content.offsetWidth);

      this.onScroll();
    }
  }

  //
  // Mouse/Touch events
  //

  private onStart = (e: Event | TouchEvent): void => {
    const el = e.target as HTMLHtmlElement;
    this.scrollActive = el.classList.contains('nv-v-scroll-y') ? 'y' : 'x';

    if (this.scrollActive === 'y') {
      const sliderBar = this.el.querySelector('.nv-v-scroll-y') as HTMLDivElement;
      const rect = sliderBar.getBoundingClientRect();
      const mouseEvt = e as MouseEvent;
      const touchEvt = e as TouchEvent;
      let currentPosY: number;

      if (e.type === 'touchstart') {
        currentPosY =
        touchEvt.targetTouches[0].clientY - rect.top;
      } else {
        currentPosY = mouseEvt.clientY - rect.top;
      }

      // Prevent error on the position
      if (Math.sign(currentPosY) === -1) {
        currentPosY = 0;
      } else if (currentPosY > sliderBar.clientHeight) {
        currentPosY = sliderBar.clientHeight;
      }

      // Get the percent position.
      const percentPosY = (currentPosY / sliderBar.clientHeight) * 100;
      this.updatePosition(percentPosY);
    } else {
      const sliderBar = this.el.querySelector('.nv-v-scroll-x') as HTMLDivElement;
      const rect = sliderBar.getBoundingClientRect();
      const mouseEvt = e as MouseEvent;
      const touchEvt = e as TouchEvent;
      let currentPosX: number;

      if (e.type === 'touchstart') {
        currentPosX =
        touchEvt.targetTouches[0].clientX - rect.left;
      } else {
        currentPosX = mouseEvt.clientX - rect.left;
      }

      // Prevent error on the position
      if (Math.sign(currentPosX) === -1) {
        currentPosX = 0;
      } else if (currentPosX > sliderBar.clientWidth) {
        currentPosX = sliderBar.clientWidth;
      }

      // Get the percent position.
      const percentPosX = (currentPosX / sliderBar.clientWidth) * 100;
      this.updatePosition(percentPosX);
    }

    this.addEvent();
  }

  private onEnd = (): void => {
    this.scrollActive = undefined;
    this.removeEvent();
  }

  private onMove = (e: MouseEvent | TouchEvent): void => {
    if (this.scrollActive === 'y') {
      const sliderBar = this.el.querySelector('.nv-v-scroll-y') as HTMLDivElement;
      const rect = sliderBar.getBoundingClientRect();
      const mouseEvt = e as MouseEvent;
      const touchEvt = e as TouchEvent;
      let currentPosY: number;

      if (e.type === 'touchstart') {
        currentPosY =
        touchEvt.targetTouches[0].clientY - rect.top;
      } else {
        currentPosY = mouseEvt.clientY - rect.top;
      }

      // Prevent error on the position
      if (Math.sign(currentPosY) === -1) {
        currentPosY = 0;
      } else if (currentPosY > sliderBar.clientHeight) {
        currentPosY = sliderBar.clientHeight;
      }

      // Get the percent position.
      const percentPosY = (currentPosY / sliderBar.clientHeight) * 100;
      this.updatePosition(percentPosY);
    } else {
      const sliderBar = this.el.querySelector('.nv-v-scroll-x') as HTMLDivElement;
      const rect = sliderBar.getBoundingClientRect();
      const mouseEvt = e as MouseEvent;
      const touchEvt = e as TouchEvent;
      let currentPosX: number;

      if (e.type === 'touchstart') {
        currentPosX =
        touchEvt.targetTouches[0].clientX - rect.left;
      } else {
        currentPosX = mouseEvt.clientX - rect.left;
      }

      // Prevent error on the position
      if (Math.sign(currentPosX) === -1) {
        currentPosX = 0;
      } else if (currentPosX > sliderBar.clientWidth) {
        currentPosX = sliderBar.clientWidth;
      }

      // Get the percent position.
      const percentPosX = (currentPosX / sliderBar.clientWidth) * 100;
      this.updatePosition(percentPosX);
    }
  }

  private addEvent(): void {
    window.addEventListener('mouseup', this.onEnd);
    window.addEventListener('touchend', this.onEnd);
    window.addEventListener('mousemove', this.onMove);
    window.addEventListener('touchmove', this.onMove);
  }

  private removeEvent(): void {
    window.removeEventListener('mousemove', this.onMove);
    window.removeEventListener('touchmove', this.onMove);
    window.removeEventListener('mouseup', this.onEnd);
    window.removeEventListener('touchend', this.onEnd);
  }

  private updatePosition(currentPos: number): void {
    if (this.scrollActive === 'y') {
      this.scrollTop = this.scrollTopMax * currentPos / 100;
      this.content.scrollTop = this.scrollTop;
      this.scrollVTop = currentPos * (this.el.offsetHeight - this.scrollYHeight) / 100;
    } else {
      this.scrollLeft = this.scrollLeftMax * currentPos / 100;
      this.content.scrollLeft = this.scrollLeft;
      this.scrollVLeft = currentPos * (this.el.offsetWidth - this.scrollXWidth) / 100;
    }
  }

  private makeScrollY(): HTMLDivElement | null {
    if ((this.el.offsetHeight !== this.scrollYHeight) && this.scrollY) {

      const style = (): any => {
        const style: any = {};

        style.top = `${this.scrollVTop}px`;
        style.height = `${this.scrollYHeight}px`;

        return style;
      };

      return (
        <div
          class='nv-v-scroll-y'
          onMouseDown={ this.onStart }
          onTouchStart={ this.onStart }
        >
          <div
            class='nv-scroll-y-slider'
            style={ style() }
          />
        </div>
      );
    }
  }

  private makeScrollX(): HTMLDivElement | null {
    if ((this.el.offsetWidth !== this.scrollXWidth) && this.scrollX && this.startup) {

      const style = () => {
        const style: any = {};
        style.left = `${this.scrollVLeft}px`;
        style.width = `${this.scrollXWidth}px`;

        return style;
      };

      return (
        <div
          class='nv-v-scroll-x'
          onMouseDown={ this.onStart }
          onTouchStart={ this.onStart }
        >
          <div class='nv-scroll-x-slider' style={ style() }></div>
        </div>
      );
    }
  }

  render() {
    const vScrollY = this.makeScrollY();
    const vScrollX = this.makeScrollX();
    return (
      <Host
        role='presentation'
        class={{
          'nv-scroll': true,
          'nv-scroll-y': this.scrollY,
          'nv-scroll-x': this.scrollX,
          'nv-scroll-display': this.hover,
          'nv-scroll-active': this.scrollActive !== undefined,
          'nv-scroll-y-active': this.scrollActive === 'y',
          'nv-scroll-x-active': this.scrollActive === 'x',
        }}
        onMouseOver={ this.onOver }
        onMouseLeave={ this.onLeave }
      >
        <div
          ref={el => this.content = el}
          class='nv-scroll-content'
          onScroll={ this.onScroll }
        >
          <div class='nv-scroll-content-container'>
            <slot></slot>
          </div>
        </div>

        { vScrollY }
        { vScrollX }
      </Host>
    );
  }
}
