/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h } from '@stencil/core';
import { Color }              from '../../interface';
@Component({
  tag: 'nv-avatar',
  shadow: false,
})
export class NvAvatar {

  /**
   * The text for the avatar.
   */
  @Prop() text?: string;

  /**
   * The image src for the `<img/>` element.
   */
  @Prop() src?: string;

  /**
   * The image alt
   */
  @Prop() alt?: string;

  /**
   * The avatar color
   */
  @Prop() color?: Color;

  /**
   * The text color
   */
  @Prop() textColor?: Color;

  /**
   * The avatar size
   */
  @Prop() size?: 'small' | 'medium' | 'large' = 'medium';

  private renderImg(): HTMLObjectElement | null {
    if (this.src) {
      return <img src={ this.src } alt={ this.alt } />;
    }
    return null;
  }

  private renderTxt(): HTMLObjectElement | null {
    if (this.text) {
      const text: string = this.text[0].toUpperCase();
      return (
        <span
          class={{
            'nv-text-primary': this.textColor === 'primary' ? true : false,
            'nv-text-secondary': this.textColor === 'secondary' ? true : false,
            'nv-text-warning': this.textColor === 'warning' ? true : false,
            'nv-text-danger': this.textColor === 'danger' ? true : false,
            'nv-text-success': this.textColor === 'success' ? true : false,
          }}>
            { text }
        </span>
      );
    }
    return null;
  }

  render() {
    return (
      <div class={{
        'nv-avatar': true,
        'nv-avatar-primary': this.color === 'primary' ? true : false,
        'nv-avatar-secondary': this.color === 'secondary' ? true : false,
        'nv-avatar-warning': this.color === 'warning' ? true : false,
        'nv-avatar-danger': this.color === 'danger' ? true : false,
        'nv-avatar-success': this.color === 'success' ? true : false,
        'nv-avatar-small': this.size === 'small' ? true : false,
        'nv-avatar-medium': this.size === 'medium' ? true : false,
        'nv-avatar-large': this.size === 'large' ? true : false,
      }}>
        <div class='nv-avatar-content'>
          { this.renderTxt() }
          { this.renderImg() }
        </div>
      </div>
    );
  }
}
