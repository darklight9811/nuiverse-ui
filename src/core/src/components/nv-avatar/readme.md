# nv-avatar



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                             | Type                                                             | Default     |
| ----------- | ------------ | --------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `alt`       | `alt`        | The image alt                           | `string`                                                         | `undefined` |
| `color`     | `color`      | The avatar color                        | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `size`      | `size`       | The avatar size                         | `"large" \| "medium" \| "small"`                                 | `'medium'`  |
| `src`       | `src`        | The image src for the `<img/>` element. | `string`                                                         | `undefined` |
| `text`      | `text`       | The text for the avatar.                | `string`                                                         | `undefined` |
| `textColor` | `text-color` | The text color                          | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |


## Dependencies

### Used by

 - [nv-chip](../nv-chip)

### Graph
```mermaid
graph TD;
  nv-chip --> nv-avatar
  style nv-avatar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
