# nv-card-icon



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute | Description   | Type     | Default     |
| ------------------ | --------- | ------------- | -------- | ----------- |
| `alt` _(required)_ | `alt`     | The alt image | `string` | `undefined` |
| `src` _(required)_ | `src`     | The src image | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
