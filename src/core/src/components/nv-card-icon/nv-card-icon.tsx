/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'nv-card-icon',
  shadow: false,
})
export class NvCardIcon {
  /**
   * The src image
   */
  @Prop() src!: string;

  /**
   * The alt image
   */
  @Prop() alt!: string;

  render() {
    if (this.src) {
      return (
        <div class='nv-card-icon'>
          <img src={ this.src } alt={ this.alt }/>
        </div>
      );
    }
  }
}
