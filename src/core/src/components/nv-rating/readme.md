# nv-rating



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                                                                               | Type                                                             | Default     |
| ------------- | -------------- | ------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `color`       | `color`        | The color of the alert.                                                                                                   | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `generalRate` | `general-rate` | If `true`, the user cannot interact with the rating and the rating accept float numbers for the display a general rating. | `boolean`                                                        | `false`     |
| `rate`        | `rate`         | The rate of the rating.                                                                                                   | `number`                                                         | `undefined` |
| `readonly`    | `readonly`     | If `true`, the user cannot interact with the rating.                                                                      | `boolean`                                                        | `false`     |
| `stars`       | `stars`        | The number of the stars.                                                                                                  | `number`                                                         | `5`         |


## Events

| Event      | Description                                      | Type                                   |
| ---------- | ------------------------------------------------ | -------------------------------------- |
| `nvChange` | Emitted when the value of the rating is changed. | `CustomEvent<RatingChangeEventDetail>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
