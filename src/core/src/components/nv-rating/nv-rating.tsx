/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host, State, Watch, Event, EventEmitter } from '@stencil/core';
import { Color, RatingChangeEventDetail } from '../../interface';

@Component({
  tag: 'nv-rating',
  shadow: false,
})
export class NvRating {
  //
  // State
  //

  // The initial value of the selected start
  @State() starSelected: number | undefined;

  //
  // Props
  //

  /**
   * The color of the alert.
   */
  @Prop() color?: Color;

  /**
   * The number of stars.
   */
  @Prop({ mutable: true }) stars?: number = 5;

  /**
   * If `true`, the user cannot interact with the rating.
   */
  @Prop() readonly?: boolean = false;

  /**
   * If `true`, the user cannot interact with the rating and
   * the rating accepts float numbers to display a general rating.
   */
  @Prop() generalRate?: boolean = false;

  /**
   * The rate of the rating.
   */
  @Prop({ mutable: true }) rate?: number | undefined;
  @Watch('rate')
  handdleValue(v: number) {
    if (v && !this.readonly) {
      this.starSelected = v;
      this.nvChange.emit({ value: v });
    }
  }

  //
  // Events
  //

  /**
   * Emitted when the value of the rating is changed.
   */
  @Event() nvChange!: EventEmitter<RatingChangeEventDetail>;

  //
  // Lifecycles
  //

  componentDidLoad() {
    this.starSelected = this.rate;

    if (this.generalRate) {
      this.readonly = true;
      this.starSelected = this.stars;
    }
  }

  private onOver (i: number): void {
    this.starSelected = i;
  }

  private onLeave (): void {
    this.starSelected = this.rate - 1;
  }

  private onClick(i: number): void {
    this.rate = i + 1;
    this.starSelected = i;
  }

  private makeStar(): SVGElement[] {
    const stars: SVGElement[] = [];
    for (let i = 0; i < this.stars; i += 1) {

      const events = !this.readonly ? {
        onMouseOver: () => this.onOver(i) ,
        onMouseLeave: () => this.onLeave(),
        onClick: () => this.onClick(i) ,
      } : null;

      const starContent = (
        <div
          class={{
            'nv-star-content': true,
            'nv-star-show': this.starSelected >= i,
          }}
          { ...events }
        >
          <svg viewBox='0 0 30 30'>
            <path d='m22.631 26.775-7.4756-3.7864-7.3572
            4.0116 1.291-8.2798-6.0888-5.7575 8.2735-1.3308 3.5941-7.57 3.8223
            7.4573 8.3101 1.079-5.9112 5.9397z' />
          </svg>
        </div>
      );

      stars.push(starContent);

    }

    return stars;
  }

  private makeStarBorder(): SVGElement[] {
    const stars: SVGElement[] = [];

    for (let i = 0; i < this.stars; i += 1) {
      stars.push(
        <svg viewBox='0 0 30 30'>
          <g transform='translate(0 -289.06)'>
            <path d='m14.867 293.12-3.5938 7.5703-8.2734 1.3301 6.0898
            5.7578-1.291 8.2793 7.3555-4.0117 7.4766 3.7871-0.37305-1.9922-1.168-6.2441
            5.9102-5.9394-8.3105-1.0801zm0.06836 4.5176 2.4551 4.7871
            5.3359 0.69336-3.7969 3.8125 0.99023 5.2871-4.7988-2.4316-4.7227 2.5762
            0.82812-5.3145-3.9102-3.6973 5.3125-0.85352z' />
          </g>
        </svg>,
      );
    }

    return stars;
  }

  render() {
    const starBoder = this.makeStarBorder();
    const star      = this.makeStar();
    const getStyle  = (): { width?: string | undefined; } => {
      const style: { width?: string | undefined; } = {};

      if (this.generalRate) {
        style.width = `${(this.rate * 100) / this.stars}%`;
      }

      return style;
    };

    return (
      <Host class={{
        'nv-rating': true,
        'nv-rating-primary': this.color === 'primary' ? true : false,
        'nv-rating-secondary': this.color === 'secondary' ? true : false,
        'nv-rating-warning': this.color === 'warning' ? true : false,
        'nv-rating-danger': this.color === 'danger' ? true : false,
        'nv-rating-success': this.color === 'success' ? true : false,
        'nv-rating-no-interact': this.readonly,
      }}>
        <div class='nv-stars-bkg'>
          { starBoder }
        </div>
        <div class='nv-stars-interact'>
          <div class='nv-stars-interact-container' style={ getStyle() }>
            { star }
          </div>
        </div>
      </Host>
    );
  }
}
