# nv-dialog



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                         | Type      | Default |
| -------- | --------- | ----------------------------------- | --------- | ------- |
| `opened` | `opened`  | If `true`, the dialog is displayed. | `boolean` | `false` |


## Methods

### `close() => Promise<void>`

Close the dialog

#### Returns

Type: `Promise<void>`



### `confirm() => Promise<void>`

Confirm and close the dialog.

#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `toggle() => Promise<void>`

Close or open dialog.

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
