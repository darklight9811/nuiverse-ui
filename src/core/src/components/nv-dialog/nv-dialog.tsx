/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element, Prop, Method } from '@stencil/core';

@Component({
  tag: 'nv-dialog',
  shadow: false,
})
export class NvDialog {

  show!: boolean;

  constructor() {
    this.show = false;
  }

  /**
   * Reference to the host element
   */
  @Element() el: HTMLElement;


  //
  // Props
  //

  /**
   * If `true`, the dialog will be displayed.
   */
  @Prop() opened: boolean = false;


  //
  // Lifecycles
  //

  componentDidLoad() {
    const dialog = this.el;
    if (dialog) {
      // Add Watch events
      dialog.addEventListener('transitionend', (e: Event) => this.watchEvents(e));
    }
  }

  private watchEvents(evt): void {
    const dialog = this.el;
    if (evt.type === 'transitionend') {

      // We only process 'opacity' because all states have this
      // change.
      if (evt.propertyName !== 'opacity') {
        return;
      }

      if (dialog.classList.contains('opening')) {
        dialog.classList.add('opened');
        dialog.classList.remove('opening');
      }  else if (dialog.classList.contains('flying')) {
        dialog.classList.remove('flying');
        this.open();
      }

      // go back to initial state after hiding
      if (dialog.classList.contains('hiding') || dialog.classList.contains('closing')) {
        dialog.classList.remove('opened');
        dialog.classList.remove('opening');
        dialog.classList.remove('closing');
        dialog.classList.remove('flying');
        dialog.classList.remove('hiding');
        dialog.classList.add('closed');
        dialog.style.display = 'none';
      }
    }
  }

  //
  // Utils
  //

  private runNextAnimationFrame(callback) {
    requestAnimationFrame(() => {
      setTimeout(callback, 0);
    });
  }

  //
  //  Methods
  //

  /**
   * Close the dialog
   */
  @Method()
  async close() {
    const dialog = this.el;
    dialog.classList.add('closing');
    this.show = false;
  }

  /**
   * Close or open dialog.
   */
  @Method()
  async toggle() {
    if (this.show) {
      this.close();
      return;
    }

    this.open();
  }

  /**
   * Confirm and close the dialog.
   */
  @Method()
  async confirm() {
    this.close();
  }

  @Method()
  async open() {
    const dialog = this.el;
    dialog.style.display = 'flex';

    // Wait a frame once display is no longer "none", to establish the animation
    this.runNextAnimationFrame(() => {
      // no operation during flying and hiding
      if (dialog.classList.contains('flying') ||
          dialog.classList.contains('hiding')) {
        return;
      }

      // If we get focus when we are closing the group, we need to cancel the closing
      // state.
      dialog.classList.remove('closed');
      dialog.classList.remove('closing');
      dialog.classList.add('opening');
      this.show = true;
    });
  }

  render() {
    return (
      <div
        class={{
          'nv-dialog': true,
          closed: !this.opened,
        }}
        style={{ display: !this.opened ? 'none' : null }}
        >
          <div class='nv-dialog-background' onClick={ () => this.close() }></div>
          <div class='nv-dialog-container nv-dialog-custom'>
            <header>
              <div class='nv-dialog-header'>
                <slot name='header'/>
              </div>
            </header>

            <section class='nv-dialog-body'>
              <slot name='content'/>
            </section>

            <footer class='nv-dialog-footer'>
              <slot name='footer'/>
            </footer>
        </div>
      </div>
    );
  }
}
