/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Prop, Element, State } from '@stencil/core';
import ColorThief                             from '../../utils/vendor/color-thief';

@Component({
  tag: 'nv-card-media',
  shadow: false,
})
export class NvCardMedia {

  /**
   *  Reference to the host element
   */
  @Element() el: HTMLElement;

  /**
   * State
   */
  @State() objectURL: string | null   = null;

  @State() colorImage: string | null  = null;

  /**
   * The type of the media, Video or image
   */
  @Prop() type: 'image' | 'video' = 'image';

  /**
   * The type of the card
   */
  @Prop() typeCard: string;

  /**
   * The src of the media
   */
  @Prop() src: string;

  /**
   * The alt of the media
   */
  @Prop() alt: string;

  /**
   * The title of the media
   */
  @Prop() titleImg: string;

  /**
   * Lifecyles events
   */
  componentWillLoad() {
    this.loadImg();
  }

  componentDidUnload() {
    // Remove file object from memory
    URL.revokeObjectURL(this.objectURL);
  }

  //
  // Methods
  //

  private async loadImg() {
    const url = this.src;

    if (url) {
      const image: string = await fetch(url)
        .then(response => response.blob())
        .then((blob: Blob): string => URL.createObjectURL(blob));

      this.objectURL = image;
    }
  }

  private loadColor() {
    const sourceImage = this.el.querySelector('.nv-card-internal-img');
    const colorThief  = new ColorThief();
    const color       = colorThief.getColor(sourceImage);
    this.colorImage   = `rgb(${color.join(', ')})`;
  }

  private makeMediaEl(): HTMLImageElement | HTMLVideoElement | null {

    if (this.src && this.type === 'image' && this.objectURL) {
      return <img
        class='nv-card-internal-img'
        src={this.objectURL}
        alt={this.alt}
        title={this.titleImg}
        onLoad={() => this.loadColor()} />;
    }

    if (this.src && this.type === 'video') {
      return <video
        src={this.src}
        title={this.titleImg} />;
    }
  }

  render() {
    if (this.src && !this.objectURL) {
      return false;
    }

    let colorBkg: HTMLElement | null;
    const mediaEl     = this.makeMediaEl();
    const colorImage  = this.colorImage;

    if (this.typeCard === 'compact') {
      colorBkg = <div class='nv-card-bkg-color' style={{ backgroundColor: colorImage }} />;
    }

    return (
      <div class='nv-card-media'>
        {colorBkg}
        {mediaEl}
        <slot />
      </div>
    );
  }
}
