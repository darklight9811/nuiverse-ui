# nv-card-media



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description                             | Type                 | Default     |
| ---------- | ----------- | --------------------------------------- | -------------------- | ----------- |
| `alt`      | `alt`       | The alt from the media                  | `string`             | `undefined` |
| `src`      | `src`       | The src from the media                  | `string`             | `undefined` |
| `titleImg` | `title-img` | The title from the media                | `string`             | `undefined` |
| `type`     | `type`      | The type from the media, Video or image | `"image" \| "video"` | `'image'`   |
| `typeCard` | `type-card` | The type from the card                  | `string`             | `undefined` |


## Dependencies

### Used by

 - [nv-card](../nv-card)

### Graph
```mermaid
graph TD;
  nv-card --> nv-card-media
  style nv-card-media fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
