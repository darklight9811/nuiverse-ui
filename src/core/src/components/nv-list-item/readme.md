# nv-list-item



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                                                                         | Type      | Default     |
| -------------- | --------------- | --------------------------------------------------------------------------------------------------- | --------- | ----------- |
| `company`      | `company`       | The company name used by **user** list.                                                             | `string`  | `undefined` |
| `firstName`    | `first-name`    | The first name used by **user** list.                                                               | `string`  | `undefined` |
| `lastName`     | `last-name`     | The last name used by **user** list.                                                                | `string`  | `undefined` |
| `textMessage`  | `text-message`  | The message text used by **email** list                                                             | `string`  | `undefined` |
| `textStamp`    | `text-stamp`    | The stemp text from the message, used by **email** list.                                            | `string`  | `undefined` |
| `textSubject`  | `text-subject`  | The subject text used by **email** list.                                                            | `string`  | `undefined` |
| `textSubtitle` | `text-subtitle` | The text for the subtitle text for the list item.                                                   | `string`  | `undefined` |
| `textTitle`    | `text-title`    | The text title for the list item.                                                                   | `string`  | `undefined` |
| `unread`       | `unread`        | If `true`, a small rounded element, will have displayed on left of the list Used by **email** list. | `boolean` | `false`     |


## Dependencies

### Used by

 - [nv-upload](../nv-upload)

### Graph
```mermaid
graph TD;
  nv-upload --> nv-list-item
  style nv-list-item fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
