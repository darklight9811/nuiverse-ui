/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Prop, Element } from '@stencil/core';
import Ripple                          from '../../utils/ripple';
import Fragment                        from 'stencil-fragment';

@Component({
  tag: 'nv-list-item',
  shadow: false,
})
export class NvListItem {
  nvList!: HTMLDivElement;
  /**
   * Reference to the host element
   */
  @Element() el: HTMLElement;

  /**
   * The text title for the list item.
   */
  @Prop() textTitle?: string;

  /**
   * The subtitle text's text for the list item.
   */
  @Prop() textSubtitle?: string;

  /**
   * The first name used by **user** list.
   */
  @Prop() firstName?: string;

  /**
   * The last name used by **user** list.
   */
  @Prop() lastName?: string;

  /**
   * The company name used by **user** list.
   */
  @Prop() company?: string;

  /**
   * The subject text used by **email** list.
   */
  @Prop() textSubject?: string;

  /**
   * The message text used by **email** list
   */
  @Prop() textMessage?: string;

  /**
   * A stamp text for the message, used by **email** list.
   */
  @Prop() textStamp?: string;

  /**
   * If `true`, a small rounded element, will be displayed on the left of the list
   * Used by **email** list.
   */
  @Prop() unread?: boolean = false;

  componentDidLoad() {
    Ripple(this.nvList);
  }

  private makeTitles(): HTMLElement | null {
    if (this.textTitle || this.textSubtitle) {
      return (
        <Fragment>
          <slot name='icon' />
          <div class='nv-list-title-area'>
            <div class='nv-list-title'>{ this.textTitle }</div>
            <div class='nv-list-sub-title'>{ this.textSubtitle }</div>
          </div>
        </Fragment>
      );
    }

    if (this.firstName || this.lastName) {
      return (
        <Fragment>
          <span class='nv-list-first-name'>{ this.firstName }</span>
          <span class='nv-list-last-name'> { this.lastName }</span>
        </Fragment>
      );
    }
  }

  private makeCompany(): HTMLElement | null {
    if (this.company) {
      return <span class='nv-list-company'>{ this.company }</span>;
    }
  }

  private makeSubject(): HTMLElement | null {
    if (this.textSubject) {
      return <small class='nv-list-subject'>{ this.textSubject }</small>;
    }
  }

  private makeMessage(): HTMLElement | null {
    if (this.textMessage) {
      return <small class='nv-list-message'>{ this.textMessage }</small>;
    }
  }

  private makeTime(): HTMLElement | null {
    if (this.textStamp) {
      return <time>{ this.textStamp }</time>;
    }
  }

  private makeAvatar(): HTMLElement | null {
    if (!this.textStamp) {
      return <slot name='avatar'/>;
    }
  }

  private makeUnread(): HTMLElement | null {
    if (this.unread) {
      return <div class='unread-email-badge'></div>;
    }
  }

  private makeSlot () {
    if (!this.firstName && !this.lastName && !this.textStamp && !this.unread) {
      return <slot />;
    }
  }

  render() {
    const title = this.makeTitles();
    const company = this.makeCompany();
    const subject = this.makeSubject();
    const textMessage = this.makeMessage();
    const time = this.makeTime();
    const avatar = this.makeAvatar();
    const unread = this.makeUnread();
    const slot = this.makeSlot();
    return (
      <div
        class={{
          'nv-list-item': true,
          'nv-list-email-unread': this.unread,
        }}
        ref={ el => this.nvList = el }
      >
        { unread }
        <div class='nv-list-item-container'>
          <header class='nv-list-title-container'>
            { title }
            { company }
            { subject }
            { textMessage }
          </header>
          { time }
          { avatar }
          { slot }
        </div>
      </div>
    );
  }
}
