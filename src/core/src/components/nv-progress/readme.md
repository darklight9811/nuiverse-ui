# nv-progress



<!-- Auto Generated Below -->


## Properties

| Property             | Attribute       | Description                                                                 | Type                                                             | Default     |
| -------------------- | --------------- | --------------------------------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `circle`             | `circle`        | If `true`, the progress bar will have a circle style.                       | `boolean`                                                        | `undefined` |
| `color`              | `color`         | The progress bar color.                                                     | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `'primary'` |
| `indeterminate`      | `indeterminate` | If `true`, the progress bar will have a indeterminate animation.            | `boolean`                                                        | `undefined` |
| `label`              | `label`         | The label for the progress bar.                                             | `boolean`                                                        | `undefined` |
| `rounded`            | `rounded`       | If `true`, the progress bar will have a rouded <style className=""></style> | `boolean`                                                        | `undefined` |
| `semiCircle`         | `semi-circle`   | If `true`, the progress bar will have a semi circle style.                  | `boolean`                                                        | `undefined` |
| `square`             | `square`        | If `true`, the progress bar will have a square style.                       | `boolean`                                                        | `undefined` |
| `value` _(required)_ | `value`         | The value of the progress bar.                                              | `number`                                                         | `undefined` |


## Methods

### `progress(newProgress: number) => Promise<void>`



#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [nv-upload](../nv-upload)

### Graph
```mermaid
graph TD;
  nv-upload --> nv-progress
  style nv-progress fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
