/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Element,
  Prop,
  State,
  Method,
  Event,
  EventEmitter,
  Host,
} from '@stencil/core';
import {
  TextFieldVariants,
} from '../../interface';

@Component({
  tag: 'nv-text-field',
  shadow: false,
})
export class NvTextField {

  input!: HTMLInputElement | HTMLTextAreaElement;

  /**
   * Reference to the host element
   */
  @Element() el: HTMLNvTextFieldElement;

  //
  // State
  //

  @State() inputFocus: boolean;
  @State() newValue: string | undefined;
  @State() valueLength: number;
  @State() prefixWidth: number;
  @State() inputType: 'text' | 'password';

  constructor() {
    this.inputFocus = false;
    this.valueLength = 0;
  }

  //
  // Props
  //

  /**
   * If `true`, the value of the input is readonly
   */
  @Prop() readonly?: boolean = false;

  /**
   * The name of the input, which is submitted with the form data.
   */
  @Prop() name?: string | undefined;

  /**
   * The label for the input.
   */
  @Prop() label?: string | undefined;

  /**
   * The helper text for the input.
   */
  @Prop() helperText?: string | undefined;

  /**
   * The style for the input.
   */
  @Prop() variant?: TextFieldVariants;

  /**
   * The max length for the input.
   */
  @Prop() max?: number | undefined;

  /**
   * The min length for the input.
   */
  @Prop() min?: number = 0;

  /**
   * If `true`, the counter is displayed.
   */
  @Prop() counter?: boolean = false;

  /**
   * The initial value for the input.
   */
  @Prop() value?: string;

  /**
   * The prefix for the input
   */
  @Prop() prefixText?: string;

  /**
   * The suffix for the input
   */
  @Prop() suffixText?: string;

  /**
   * The type of the input.
   */
  @Prop() type?: 'text' | 'password' = 'text';

  /**
   * If `true`, the text field will be a textarea.
   */
  @Prop() multiline?: boolean = false;

  /**
   * `Width` of the `textarea`
   */
  @Prop() width?: number;

  /**
   * `Width` of the `textarea`
   */
  @Prop() height?: number;

  /**
   * If `true` the height element will increase based on the element.
   * Available only for **multiline(textarea)**.
   */
  @Prop() autoGrow?: boolean = false;

  /**
   * The visible width of the text control, in average character widths.
   * If this is specified, it must be a positive integer.
   */
  @Prop() cols?: number | undefined;

  /**
   * The number of visible text lines for the control.
   */
  @Prop() rows?: number | undefined;

  /**
   * If `true`, the textarea cannot be resized.
   */
  @Prop() noResize?: boolean = false;

  /**
   * If `true`, the content of the password input will be displayed.
   */

  @Prop() passView?: boolean = false;
  //
  // Lifecyles
  //

  componentWillLoad() {
    if (this.value) {
      this.newValue = this.value;
    }
  }

  componentDidLoad() {
    if (this.prefixText) {
      const prefix: HTMLElement = this.el.querySelector('.nv-text-field-prefix');
      this.prefixWidth = prefix.offsetWidth;
    }

    this.inputType = this.type;
  }

  //
  // Events
  //

  /**
   * Emitted when the value of the input is changed.
   */
  @Event() onInput: EventEmitter;


  //
  // Methods
  //

  @Method()
  async selectText() {
    const valueLength = this.input.value.length;
    this.input.focus();
    this.input.setSelectionRange(0, valueLength);
  }

  private makeLabel(): HTMLElement | null {

    let activeStatus: boolean = false;
    if (this.label) {

      if (this.inputFocus || this.newValue || this.inputFocus && !this.newValue) {
        activeStatus = true;
      }

      return (
        <span class={{
          ['nv-text-field-label']: true,
          active: activeStatus,
        }}
        style={{
          left: `${this.prefixWidth}px`,
        }}>
          { this.label }
        </span>
      );
    }
  }

  private makeBorderBottom(): HTMLElement | null {
    return (
      <div class={{
        'nv-line-ripple': true,
        active: this.inputFocus,
      }}></div>
    );
  }

  private makeHelperText(): HTMLElement | null {
    return <small class='nv-text-field-helper-text'>{ this.helperText }</small>;
  }

  private makeCounter(): HTMLElement | null {
    if (this.counter && this.max) {
      return <small class='nv-text-field-counter'>{ this.valueLength } / { this.max }</small>;
    }
  }

  private makePrefix(): HTMLElement | null {
    if (this.prefixText) {
      return <span class='nv-text-field-prefix'>{ this.prefixText }</span>;
    }
  }

  private makeSuffix(): HTMLElement | null {
    if (this.suffixText) {
      return <span class='nv-text-field-suffix'>{ this.suffixText }</span>;
    }
  }

  private handleChange(event) {
    this.newValue = event.target.value;
    this.value = event.target.value;
    const el = event.target;

    if (this.counter) {
      this.valueLength = event.target.value.length;
    }
    if (this.autoGrow && this.multiline) {
      this.height = el.scrollHeight;
    }

    this.onInput.emit(event);
  }

  private handleFocus(value: boolean): void {
    this.inputFocus = value;

  }

  private showPassword = () => {
    if (this.type === 'password') {
      this.type = 'text';
      this.passView = true;
    } else {
      this.type = 'password';
      this.passView = false;
    }
  }

  private makeInput(): HTMLInputElement | HTMLTextAreaElement | null {
    if (this.multiline) {
      return (
        <textarea
          onFocus={ () => this.handleFocus(true) }
          onBlur={ () => this.handleFocus(false) }
          onInput={ event => this.handleChange(event)}
          maxLength={ this.max }
          minLength={ this.min }
          readonly={ this.readonly }
          cols={ this.cols }
          rows={ this.rows }
          style={{
            height: `${this.height}px`,
            resize: `${this.noResize ? 'none' : null}`,
          }}
          value={ this.newValue }
          ref={ el => this.input = el }
          name={ this.name }
        ></textarea>
      );
    }

    return (
      <input
        type={ this.type }
        onFocus={ () => this.handleFocus(true) }
        onBlur={ () => this.handleFocus(false) }
        onInput={ event => this.handleChange(event)}
        maxLength={ this.max }
        minLength={ this.min }
        readonly={ this.readonly }
        value={ this.newValue }
        ref={ el => this.input = el }
        name={ this.name }
      />
    );
  }

  private makeView(): SVGAElement {
    // tslint:disable: max-line-length
    if (this.inputType === 'password') {
      if (!this.passView) {
        return (
          <nv-button
            onClick={ this.showPassword }
            class='nv-txtfield-pass-view-btn'
            rounded
            text>
            <svg viewBox='0 0 30 30'>
              <g transform='translate(0 -289.06)'>
                <path
                  transform='translate(0 289.06)'
                  d='m25.607 4.3926-21.215 21.215 1.4141 1.4141 5.8438-5.8438a4
                  4 0 0 0 3.3496 1.8223 4 4 0 0 0 4-4 4 4 0 0 0-1.8203-3.3516l4.2812-4.2812c2.1638
                  1.8324 3.5391 4.5664 3.5391 7.6328h2c0-3.6056-1.6064-6.8373-4.1328-9.0391l4.1543-4.1543-1.4141-1.4141zm-10.607
                  2.6074c-6.6156 0-12 5.3844-12 12h2c0-5.5347 4.4653-10 10-10 0.96443 0 1.8939 0.14328 2.7754 0.39648l1.5684-1.5684c-1.3491-0.52662-2.8107-0.82812-4.3438-0.82812z' />
              </g>
            </svg>
          </nv-button>
        );
      }

      return (
        <nv-button
          onClick={ this.showPassword }
          class='nv-txtfield-pass-view-btn'
          rounded
          text
        >
          <svg viewBox='0 0 30 30'>
            <g transform='translate(0 -289.06)'>
              <path
                d='m15 296.06c-6.6156 0-12 5.3844-12
                12h2c0-5.5347 4.4653-10 10-10 5.5347 0
                10 4.4653 10 10h2c0-6.6156-5.3844-12-12-12zm0 8a4 4 0 0 0-4 4 4
                4 0 0 0 4 4 4 4 0 0 0 4-4 4 4 0 0 0-4-4z' />
            </g>
          </svg>
        </nv-button>
      );
    }
  }

  render() {
    const label = this.makeLabel();
    const borderBottom = this.makeBorderBottom();
    const helperText = this.makeHelperText();
    const counter = this.makeCounter();
    const prefix = this.makePrefix();
    const suffix = this.makeSuffix();
    const makeInput = this.makeInput();
    const view = this.makeView();

    return (
      <Host class={{
        'nv-text-field': true,
        'nv-text-field-password': this.type === 'password',
        'nv-input-shaped': this.variant === 'shaped' ? true : false,
        'nv-input-shaped-filled': this.variant === 'shaped-filled' ? true : false,
        'nv-input-outlined': this.variant === 'outlined' ? true : false,
        'nv-input-shaped-outlined': this.variant === 'shaped-outlined' ? true : false,
      }}>
        <div class='nv-text-field-container'>
          <div class='nv-text-field-elements'>
            {/* The prefix */}
            { prefix }

            {/* Input/Txtarea */}
            { makeInput }

            {/* Suffix */}
            { suffix }

            {/* View password */}
            { view }

            {/* Label */}
            { label }

          </div>
          {/* Line boder */}
          { borderBottom }
        </div>
        <div class='nv-text-field-helper-line'>
          {/* Helper text */}
          { helperText }

          {/* Counter */}
          { counter }
        </div>
      </Host>
    );
  }
}
