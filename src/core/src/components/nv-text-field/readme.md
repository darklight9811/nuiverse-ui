# nv-text-field



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                                                                                                            | Type                                                             | Default     |
| ------------ | ------------- | ---------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `autoGrow`   | `auto-grow`   | If `true` the height element will increase based on the element. Avalible only for **multiline(textarea)**.            | `boolean`                                                        | `false`     |
| `cols`       | `cols`        | The visible width of the text control, in average character widths. If it is specified, it must be a positive integer. | `number`                                                         | `undefined` |
| `counter`    | `counter`     | If `true`, the counter is displayed.                                                                                   | `boolean`                                                        | `false`     |
| `height`     | `height`      | `Width` of the `textarea`                                                                                              | `number`                                                         | `undefined` |
| `helperText` | `helper-text` | The relper text for the input.                                                                                         | `string`                                                         | `undefined` |
| `label`      | `label`       | The label for the input.                                                                                               | `string`                                                         | `undefined` |
| `max`        | `max`         | The max lenght for the input.                                                                                          | `number`                                                         | `undefined` |
| `min`        | `min`         | The min lenght for the input.                                                                                          | `number`                                                         | `0`         |
| `multiline`  | `multiline`   | If `true`, the text field will be a textarea.                                                                          | `boolean`                                                        | `false`     |
| `name`       | `name`        | The name of the input, which is submitted with the form data.                                                          | `string`                                                         | `undefined` |
| `noResize`   | `no-resize`   | If `true`, the textarea cannot will resized.                                                                           | `boolean`                                                        | `false`     |
| `passView`   | `pass-view`   | If `true`, the content of the password input will be displayed.                                                        | `boolean`                                                        | `false`     |
| `prefixText` | `prefix-text` | The prefix for the input                                                                                               | `string`                                                         | `undefined` |
| `readonly`   | `readonly`    | If `true`, the value of the input is readonly                                                                          | `boolean`                                                        | `false`     |
| `rows`       | `rows`        | The number of visible text lines for the control.                                                                      | `number`                                                         | `undefined` |
| `suffixText` | `suffix-text` | The suffix for the input                                                                                               | `string`                                                         | `undefined` |
| `type`       | `type`        | The type of the input.                                                                                                 | `"password" \| "text"`                                           | `'text'`    |
| `value`      | `value`       | The initial value for the input.                                                                                       | `string`                                                         | `undefined` |
| `variant`    | `variant`     | The style for the input.                                                                                               | `"outlined" \| "shaped" \| "shaped-filled" \| "shaped-outlined"` | `undefined` |
| `width`      | `width`       | `Width` of the `textarea`                                                                                              | `number`                                                         | `undefined` |


## Events

| Event     | Description                                     | Type               |
| --------- | ----------------------------------------------- | ------------------ |
| `onInput` | Emitted when the value of the input as changed. | `CustomEvent<any>` |


## Methods

### `selectText() => Promise<void>`



#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [nv-dialog-prompt](../nv-dialog-prompt)

### Depends on

- [nv-button](../nv-button)

### Graph
```mermaid
graph TD;
  nv-text-field --> nv-button
  nv-dialog-prompt --> nv-text-field
  style nv-text-field fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
