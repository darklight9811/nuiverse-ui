# nv-list-header



<!-- Auto Generated Below -->


## Dependencies

### Depends on

- [nv-sub-header](../nv-sub-header)

### Graph
```mermaid
graph TD;
  nv-list-header --> nv-sub-header
  style nv-list-header fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
