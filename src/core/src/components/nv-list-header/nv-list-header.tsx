/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h } from '@stencil/core';

@Component({
  tag: 'nv-list-header',
  shadow: false,
})
export class NvListHeader {

  render() {
    return (
      <div class='nv-list-header'>
        <nv-sub-header><slot /></nv-sub-header>
      </div>
    );
  }
}
