# nv-list



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                       | Type      | Default |
| -------------- | --------------- | ------------------------------------------------- | --------- | ------- |
| `email`        | `email`         | If `true`, the liste will have a **email** style. | `boolean` | `false` |
| `stickyHeader` | `sticky-header` | If `true`, the header will have a sicky effect.   | `boolean` | `false` |
| `users`        | `users`         | If `true`, the liste will have a **user** style.  | `boolean` | `false` |


## Events

| Event         | Description                             | Type               |
| ------------- | --------------------------------------- | ------------------ |
| `nvDidUpdate` | Emitted when the componente is updated. | `CustomEvent<any>` |


## Dependencies

### Used by

 - [nv-upload](../nv-upload)

### Graph
```mermaid
graph TD;
  nv-upload --> nv-list
  style nv-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
