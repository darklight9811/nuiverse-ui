/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Prop, Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'nv-list',
  shadow: false,
})
export class NvList {
  container!: HTMLDivElement;
  nvList!: HTMLDivElement;
  headerList!: NodeListOf<Element>;

  /**
   * If `true`, the list will have a **user** style.
   */
  @Prop() users?: boolean = false;

  /**
   * If `true`, the list will have a **email** style.
   */
  @Prop() email: boolean = false;

  /**
   * If `true`, the header will have a sticky effect.
   */
  @Prop() stickyHeader: boolean = false;

  //
  // Events
  //

  /**
   * Emitted when the component is updated.
   */
  @Event() nvDidUpdate: EventEmitter<any>;

  componentDidLoad() {
    if (this.stickyHeader) {
      const headers = this.container.querySelectorAll('.nv-list-header');
      this.headerList = headers;
    }
  }

  componentDidUpdate() {
    this.nvDidUpdate.emit();
  }

  render() {
    return (
      <div ref={ el => this.nvList = el } class={{
        'nv-list': true,
        'nv-list-users-type': this.users,
        'nv-list-email-type': this.email,
        'nv-list-sticky-header': this.stickyHeader,
      }}>
        <div class='nv-list-container' ref={ el => this.container = el }>
          <slot />
        </div>
      </div>
    );
  }
}
