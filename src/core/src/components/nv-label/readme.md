# nv-label



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                              | Type      | Default     |
| ---------- | ---------- | ---------------------------------------- | --------- | ----------- |
| `disabled` | `disabled` | If `true`, the label will have disabled. | `boolean` | `false`     |
| `htmlFor`  | `html-for` | The html for attrinute for the label.    | `string`  | `undefined` |
| `required` | `required` | If `true`, the label will dislay a `*`.  | `boolean` | `false`     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
