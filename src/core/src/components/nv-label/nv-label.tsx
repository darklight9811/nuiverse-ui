/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element, Prop } from '@stencil/core';

@Component({
  tag: 'nv-label',
  shadow: false,
})
export class NvLabel {
  /**
   * Reference to the host element
   */
  @Element() el: HTMLElement;

  /**
   * The attribute's html for the label.
   */
  @Prop() htmlFor?: string;

  /**
   * If `true`, the label will be disabled.
   */
  @Prop() disabled?: boolean = false;

  /**
   * If `true`, the label will dislay a `*`.
   */
  @Prop() required?: boolean = false;

  constructor() {
    this.disabled = false;
  }

  render() {
    return (
      <label
        htmlFor={ this.htmlFor }
        class={{
          'nv-label': true,
          disabled: this.disabled,
          required: this.required,
        }}
      >
        <slot></slot>
      </label>
    );
  }
}
