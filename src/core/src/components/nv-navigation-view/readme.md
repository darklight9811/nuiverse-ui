# nv-navigation-view



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                    | Type                                                             | Default     |
| ---------- | ---------- | -------------------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `color`    | `color`    | The color of the alert.                                        | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `mode`     | `mode`     | If `true`, the Navigation View will have a style like topbar.  | `"left" \| "top"`                                                | `'left'`    |
| `position` | `position` | If `true`, the Navigation View will have a fixed style on top. | `"fixed"`                                                        | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
