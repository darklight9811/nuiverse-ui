/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  Prop,
  h,
  Host,
  State,
  Element,
  Event,
  EventEmitter,
  Listen,
} from '@stencil/core';
import {
  Color,
  NvNavViewItemsDidChange,
} from '../../interface';

@Component({
  tag: 'nv-navigation-view',
  shadow: false,
})
export class NvNavigationView {
  /**
   * Reference to the element host.
   */
  @Element() el: HTMLNvNavigationViewElement;

  //
  // State
  //

  @State() navItems: HTMLNvNavigationViewItemsElement[] = [];
  @State() nvNavHeeader: HTMLNvNavigationViewHeaderElement;
  @State() initialWidth: number;
  @State() responsive: boolean = false;
  @State() items: NodeListOf<Element>;
  @State() mnMbActive: boolean = false;
  @State() heightMnMb: number = 0;
  @State() hostHeight: number = 0;

  /**
   * The color of the alert.
   */
  @Prop() color?: Color;

  /**
   * If `true`, the Navigation View will have a style like topbar.
   */
  @Prop() mode?: 'top' | 'left' = 'left';

  /**
   * If `true`, the Navigation View will have a fixed style on top.
   */
  @Prop() position?: 'fixed' | undefined;

  //
  // Lifecycles
  //

  componentDidLoad() {
    const nvNames: string = [
      'nv-navigation-view-header',
      'nv-navigation-view-items',
      'nv-navigation-view-item',
      'nv-navigation-view-separator',
    ].join(', ');

    this.navItems = Array.from(this.el.querySelectorAll(nvNames));

    this.nvNavHeeader = this.el.querySelector('nv-navigation-view-header');
    this.initSelect().then(() => {
      this.nvNavViewViewWillLoad.emit();
      this.getDimensions().then(() => {
        this.resize();
        this.getMnMbScrollHeight();
      });
    });
    this.addEvents();

    if (this.position === 'fixed' && this.mode === 'top') {
      document.querySelector('body').classList.add('nv-navigation-view-fixed-body');
    }
  }

  //
  // Events
  //

  /**
   * Emits when the navigationViewItems starts to load a component.
   * @internal
   */
  @Event() nvNavViewViewWillLoad: EventEmitter<any>;

  /**
   * Emitted when the NavigationItems is activated.
   * @internal
   */
  @Event({
    composed: true,
    cancelable: true,
    bubbles: true,
  }) nvNavViewItemsActive: EventEmitter<any>;


  @Listen('nvNavViewItemsDidChange')
  protected nvNavViewItemsDidChangeHandler(event: CustomEvent<NvNavViewItemsDidChange>) {
    this.nvNavViewItemsActive.emit({
      items: event.detail.items,
      item: event.detail.item,
    });
  }

  private async initSelect(): Promise<void> {
    // wait for all navItems to be ready.
    await Promise.all(this.navItems.map(item => item.componentOnReady()));
  }

  private makeRightContainer(): HTMLDivElement | null {
    if (this.mode === 'top') {
      return (
        <div class='nv-nav-view-right-content'>
          <slot name='search'/>
          <slot name='footer'/>
        </div>
      );
    }
  }

  private addEvents(): void {
    window.addEventListener('resize', this.resize);
  }

  private resize = (): void => {
    const winSize: number = window.innerWidth;
    const menuMb = this.el.querySelector('.nv-navigation-view-mb-menu');

    if (winSize < this.initialWidth && !this.responsive) {
      this.responsive = true;
      this.items.forEach((item: Element) => {
        if (item.tagName !== 'NV-NAVIGATION-VIEW-HEADER') {
          menuMb.appendChild(item);
        }
        this.getMnMbScrollHeight();
      });
      return;
    }

    if (winSize > this.initialWidth && this.responsive) {
      const leftContent = this.el.querySelector('.nv-nav-view-left-content');
      this.items.forEach((item: Element) => {
        if (item.tagName !== 'NV-NAVIGATION-VIEW-HEADER') {
          leftContent.appendChild(item);
        }
      });
      this.responsive = false;
      return;
    }

    this.getMnMbScrollHeight();
  }

  private async getDimensions(): Promise<void> {
    // Elements
    const leftContent: HTMLDivElement = this.el.querySelector('.nv-nav-view-left-content');
    const rightContent: HTMLDivElement = this.el.querySelector('.nv-nav-view-right-content');
    const menuMb: HTMLDivElement = this.el.querySelector('.nv-navigation-view-mb-menu');
    const styleHost: CSSStyleDeclaration = window.getComputedStyle(this.el);

    // Calc dimensions
    const widthLeftContent: number = leftContent.offsetWidth || 0;
    const widthRightContent: number = rightContent.offsetWidth || 0;
    const paddingLeft: number = parseInt(styleHost.paddingLeft, 10);
    const paddingRight: number = parseInt(styleHost.paddingRight, 10);

    // Apply dimensions
    this.items = this.el.querySelectorAll('.nv-nav-view-left-content > *');
    this.initialWidth = widthLeftContent + widthRightContent + paddingLeft + paddingRight;
    this.heightMnMb = menuMb.scrollHeight;
    this.hostHeight = this.el.offsetHeight;
  }

  private async getMnMbScrollHeight(): Promise<void> {
    console.log('resize...');
    if (this.position === 'fixed') {
      const winHeight: number = window.innerHeight || 0;
      this.heightMnMb = winHeight - this.hostHeight;
      return;
    }

    const menuMb = this.el.querySelector('.nv-navigation-view-mb-menu');
    await Promise.all(this.navItems.map(item => item.componentOnReady()));
    this.heightMnMb = menuMb.scrollHeight;
  }

  private openMnMb = (): void => {
    if (this.heightMnMb <= 0) {
      this.getMnMbScrollHeight().then(() => {
        this.openMnMb();
      });
      return;
    }

    if (this.mnMbActive) {
      this.mnMbActive = false;
      return;
    }
    this.mnMbActive = true;
  }

  render() {
    const rightContainer = this.makeRightContainer();
    return (
      <Host class={{
        'nv-navigation-view': true,
        'nv-navigation-view-left': this.mode === 'left',
        'nv-navigation-view-top': this.mode === 'top',
        'nv-navigation-view-top-responsive': this.mode === 'top' && this.responsive,
        'nv-navigation-view-top-fixed': this.mode === 'top' && this.position === 'fixed',
      }}>
        <div
          class='nv-navigation-view-menu'
          onClick={ this.openMnMb }>
          <svg width='30' height='30' viewBox='0 0 30 30'>
            <g transform='translate(0 -289.06)'>
              <path
                transform='translate(0 289.06)'
                d='m3 6v2h24v-2h-24zm0 8v2h24v-2h-24zm0 8v2h24v-2h-24z'
              />
            </g>
          </svg>
        </div>
        <div class='nv-nav-view-left-content'>
          <slot />
        </div>
        { rightContainer }
        <div
          class={{
            'nv-navigation-view-mb-menu': true,
            'nv-nav-view-mnmb-active': this.mnMbActive,
            'nv-nav-view-mnmb-hidden': !this.mnMbActive,
          }}
          style={{ height: `${this.heightMnMb}px` }}
        >
        </div>
      </Host>
    );
  }
}
