/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

export type KnobName = 'A' | 'B' | undefined;

export type RangeValue = number | { lower: number; upper: number; };

export interface KnobDetail {
  value: number;
  posX: number;
}

export interface RangeChangeEventDetail {
  value: RangeValue;
}
