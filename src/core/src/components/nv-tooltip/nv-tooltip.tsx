/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  Prop,
  h,
  Element,
  Host,
  State,
}                                 from '@stencil/core';
import { runNextAnimationFrame }  from '../../utils';
import { Color }                  from '../../interface';
@Component({
  tag: 'nv-tooltip',
  shadow: false,
})
export class NvTooltip {
  id!: string;

  /**
   * Reference to the host element
   */
  @Element() el: HTMLNvTooltipElement;

  //
  // State
  //

  @State() tooltipEl!: HTMLDivElement;

  /**
   * The color for this tooltip
   */
  @Prop() color: Color;

  /**
   * The position of the tooltip
   */
  @Prop() position: 'top' | 'bottom' | 'left' | 'right';

  /**
   * The text for this tooltip.
   */
  @Prop() text: string;

  constructor() {
    this.id = `tooltip-id-${new Date().getTime()}`;
  }

  //
  // Computed
  //

  private mouseEnter = (ev: MouseEvent) => {
    const el = ev.target as HTMLElement;
    const rootGetPos = el.getClientRects()[0];
    const tooltipGetPos = this.tooltipEl.getClientRects()[0];
    let posX = (rootGetPos.left + rootGetPos.width / 2) - tooltipGetPos.width / 2;
    let posY;

    if (this.position === 'top') {
      posY = rootGetPos.top - (tooltipGetPos.height + 6);
    }

    if (this.position === 'left') {
      posY = (rootGetPos.top + rootGetPos.height / 2) - tooltipGetPos.height / 2;
      posX = rootGetPos.left - (tooltipGetPos.width + 6);
    }

    if (this.position === 'right') {
      posY = (rootGetPos.top + rootGetPos.height / 2) - tooltipGetPos.height / 2;
      posX = rootGetPos.right + 6;
    }

    if (this.position === 'bottom') {
      posY = rootGetPos.bottom +  6;
    }

    this.tooltipEl.setAttribute('style', `left: ${posX}px; top: ${posY}px`);

    const body = document.querySelector('body');
    body.appendChild(this.tooltipEl);

    runNextAnimationFrame(() => {
      this.tooltipEl.classList.remove('nv-tooltip-hidden');
      this.tooltipEl.classList.add('nv-tooltip-display');
    });
  }

  private mouseLeave = () => {
    runNextAnimationFrame(() => {
      this.tooltipEl.classList.add('nv-tooltip-hidden');
      this.tooltipEl.classList.remove('nv-tooltip-display');
    });
  }

  render() {
    return (
      <Host
        onMouseEnter={ this.mouseEnter }
        onMouseLeave={ this.mouseLeave }
        class={{
          'nv-tooltip-container': true,
        }}
      >
        <div>
          <slot />
        </div>
        <div
          ref={ el => this.tooltipEl = el }
          class={{
            'nv-tooltip': true,
            'nv-tooltip-hidden': true,
            'nv-tooltip-top': this.position === 'top' ? true : false,
            'nv-tooltip-bottom': this.position === 'bottom' ? true : false,
            'nv-tooltip-left': this.position === 'left' ? true : false,
            'nv-tooltip-right': this.position === 'right' ? true : false,
            [this.id]: true,
          }}
        >
          { this.text }
        </div>
      </Host>
    );
  }
}
