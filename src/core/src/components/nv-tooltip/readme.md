# nv-tooltip



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                 | Type                                                             | Default     |
| ---------- | ---------- | --------------------------- | ---------------------------------------------------------------- | ----------- |
| `color`    | `color`    | The color for this tooltip  | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `position` | `position` | The position of the tooltip | `"bottom" \| "left" \| "right" \| "top"`                         | `undefined` |
| `text`     | `text`     | The text for this tooltip.  | `string`                                                         | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
