# nv-dialog-select



<!-- Auto Generated Below -->


## Properties

| Property             | Attribute      | Description                                           | Type                                                             | Default     |
| -------------------- | -------------- | ----------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `color` _(required)_ | `color`        | The color for the confirm button.                     | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `headerTitle`        | `header-title` | The header title for the dialog.                      | `string`                                                         | `undefined` |
| `multiple`           | `multiple`     | If `true` the list accept multiple selected elements. | `boolean`                                                        | `false`     |
| `opened`             | `opened`       | If `true`, the dialog is displayed.                   | `boolean`                                                        | `false`     |
| `textCancel`         | `text-cancel`  | The text for cancel button from this dialog           | `string`                                                         | `undefined` |
| `textConfirm`        | `text-confirm` | The text for confirm button from this dialog          | `string`                                                         | `undefined` |


## Events

| Event          | Description                      | Type               |
| -------------- | -------------------------------- | ------------------ |
| `selectedItem` | Emitted when a item is selected. | `CustomEvent<any>` |


## Methods

### `close() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `confirm() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `toggle() => Promise<void>`



#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [nv-button](../nv-button)

### Graph
```mermaid
graph TD;
  nv-dialog-select --> nv-button
  style nv-dialog-select fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
