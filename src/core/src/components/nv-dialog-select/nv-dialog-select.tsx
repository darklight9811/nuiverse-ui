/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Element,
  Prop,
  Method,
  EventEmitter,
  Event,
  Host,
 }                      from '@stencil/core';
import Fragment         from 'stencil-fragment';
import Ripple           from '../../utils/ripple';
import { Color }        from '../../interface';

@Component({
  tag: 'nv-dialog-select',
  shadow: false,
})
export class NvDialogSelect {
  list!: HTMLUListElement;
  show!: boolean;

  constructor() {
    this.show = false;
  }

  /**
   * Reference to the host element
   */

  @Element() el: HTMLElement;

  //
  // Props
  //

  /**
   * The header title for the dialog.
   */
  @Prop() headerTitle: string;

  /**
   * The cancel button's text for this dialog
   */
  @Prop() textCancel: string;

  /**
   * The color for the confirmation button.
   */
  @Prop() color!: Color;

  /**
   * If `true`, the dialog is displayed.
   */
  @Prop() opened?: boolean = false;

  /**
   * If `true` the list accepts multiple selected elements.
   */
  @Prop() multiple?: boolean = false;

  /**
   * The confirm button's text for this dialog
   */
  @Prop() textConfirm: string;

  //
  // Events
  //

  /**
   * Emitted when a item is selected.
   */
  @Event({
    eventName: 'selectedItem',
    composed: true,
    cancelable: false,
    bubbles: true,
  }) selectedItem: EventEmitter;

  //
  // Lifecycles
  //

  componentDidLoad() {
    const dialog = this.el;
    const list = this.list;
    const listItems = list.querySelectorAll('li');

    if (dialog) {
      // Add Watch events
      dialog.addEventListener('transitionend', e => this.watchEvents(e));
      list.addEventListener('click', e => this.listClick(e));
    }

    if (listItems) {
      listItems.forEach((item) => {
        Ripple(item);
      });
    }
  }

  //
  // Methods
  //

  @Method()
  async close() {
    const dialog = this.el;
    dialog.classList.add('closing');
    this.show = false;
  }

  @Method()
  async toggle() {
    if (this.show) {
      this.close();
      return;
    }

    this.open();
  }

  @Method()
  async confirm() {
    this.close();
  }

  @Method()
  async open() {
    const dialog = this.el;
    dialog.style.display = 'flex';

    // Wait a frame once display is no longer "none", to establish basis for the animation
    this.runNextAnimationFrame(() => {
      // no operation during flying and hiding
      if (dialog.classList.contains('flying') ||
          dialog.classList.contains('hiding')) {
        return;
      }

      // If we get focus when we are closing the group, we need to cancel the closing
      // state.
      dialog.classList.remove('closed');
      dialog.classList.remove('closing');
      dialog.classList.add('opening');
      this.show = true;
    });
  }

  //
  // Private methods
  //

  private listClick(e: MouseEvent | TouchEvent): void {
    const el = e.target as HTMLElement;
    const selected = el.getAttribute('aria-selected') === 'true';

    if (this.multiple) {
      this.onChangeMultiple(el, selected);
      return;
    }

    this.onChange(el, selected);
  }

  private onChange(el: HTMLElement, selected: boolean): void {
    this.clearSelected();
    if (!selected) {
      el.setAttribute('aria-selected', (!selected).toString());
    }

    this.fireChange();
  }

  private onChangeMultiple(el: HTMLElement, selected: boolean): void {
    el.setAttribute('aria-selected', (!selected).toString());
    this.fireChange();
  }

  private clearSelected(): void {
    [].forEach.call(this.getSelectedOptions(), (option: HTMLElement) => {
      option.removeAttribute('aria-selected');
    });
  }

  private fireChange(): void {
    this.selectedItem.emit({ value: this.getValueString() });
  }

  private makeHeader(): HTMLElement | null {
    if (this.headerTitle) {
      return (
        <header>
          <div class='nv-dialog-header'>
            <h3>{ this.headerTitle }</h3>
          </div>
        </header>
      );
    }
  }

  private watchEvents(evt): void {
    const dialog = this.el;
    if (evt.type === 'transitionend') {

      // We only process 'opacity' because all states have this
      // change.
      if (evt.propertyName !== 'opacity') {
        return;
      }

      if (dialog.classList.contains('opening')) {
        dialog.classList.add('opened');
        dialog.classList.remove('opening');
      }  else if (dialog.classList.contains('flying')) {
        dialog.classList.remove('flying');
        this.open();
      }

      // go back to initial state after hiding
      if (dialog.classList.contains('hiding') || dialog.classList.contains('closing')) {
        dialog.classList.remove('opened');
        dialog.classList.remove('opening');
        dialog.classList.remove('closing');
        dialog.classList.remove('flying');
        dialog.classList.remove('hiding');
        dialog.classList.add('closed');
        dialog.style.display = 'none';
      }
    }
  }

  //
  // Getters
  //

  private getSelectedOptions(): NodeListOf<Element> {
    return this.el.querySelectorAll('li[aria-selected="true"]');
  }

  private getSelected() {
    return this.getSelectedOptions()[0];
  }

  private getValueString(): string {
    const selected = this.getSelected();
    return selected && selected.textContent;
  }

  //
  // Utils
  //

  private runNextAnimationFrame(callback) {
    requestAnimationFrame(() => {
      setTimeout(callback, 0);
    });
  }

  private makeFooter(): HTMLElement | null {
    return (
      <Fragment>
      {/* Cancel */}
      <nv-button
        text
        disabled-ripple
        onClick={ () => this.close() }>
          <span>{ this.textCancel }</span>
      </nv-button>

      {/* Confirm */}
      <nv-button
        text
        disabled-ripple
        color={this.color}
        onClick={ () => this.confirm() }>
          <span>{ this.textConfirm }</span>
      </nv-button>
      <div class='nv-dialog-footer-divider'/>
    </Fragment>
    );
  }

  render() {
    const header = this.makeHeader();
    const footer = this.makeFooter();

    return (
      <Host
        class={{
          'nv-dialog': true,
          closed: !this.opened,
        }}
        style={{ display: !this.opened ? 'none' : null }}
        >
          <div class='nv-dialog-background' onClick={ () => this.close() }></div>
          <div class='nv-dialog-container nv-dialog-select'>
            {/* Header */}
            { header }

            <section class='nv-dialog-body no-padding'>
              <ul class='nv-list' ref={ el => this.list = el }>
                <slot />
              </ul>
            </section>

            <footer class='nv-dialog-footer'>
              { footer }
            </footer>
        </div>
      </Host>
    );
  }
}
