# nv-tabs



<!-- Auto Generated Below -->


## Events

| Event              | Description                                                                | Type                            |
| ------------------ | -------------------------------------------------------------------------- | ------------------------------- |
| `nvTabsDidChange`  | Emitted when the navigation has finished transitioning to a new component. | `CustomEvent<{ tab: string; }>` |
| `nvTabsWillChange` | Emitted when the navigation is about to transition to a new component.     | `CustomEvent<{ tab: string; }>` |


## Methods

### `getTab(tab: string | HTMLNvTabElement) => Promise<HTMLNvTabElement>`

Get a specific tab by the value of its `tab` property or an element reference.

#### Returns

Type: `Promise<HTMLNvTabElement>`



### `select(tab: string | HTMLNvTabElement) => Promise<boolean>`

Select a tab by the value of its `tab` property or an element reference.

#### Returns

Type: `Promise<boolean>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
