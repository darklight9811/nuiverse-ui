# nv-dialog-prompt



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                             | Type                                                             | Default     |
| ---------------- | ------------------ | ----------------------------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `autoSelect`     | `auto-select`      | If `true`, the text inside on text field is selected after open dialog. | `boolean`                                                        | `true`      |
| `color`          | `color`            | The color for the confirm button.                                       | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `headerTitle`    | `header-title`     | The header title for the dialog                                         | `boolean`                                                        | `false`     |
| `label`          | `label`            | The label for the text field.                                           | `string`                                                         | `undefined` |
| `opened`         | `opened`           | If `true`, the dialog is displayed.                                     | `boolean`                                                        | `false`     |
| `textCancel`     | `text-cancel`      | The text for the cancel button from this dialog.                        | `string`                                                         | `undefined` |
| `textConfirm`    | `text-confirm`     | The text for the confirm button from this dialog.                       | `string`                                                         | `undefined` |
| `textFieldValue` | `text-field-value` | The initial value for the text field.                                   | `string`                                                         | `undefined` |


## Methods

### `close() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `confirm() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `toggle() => Promise<void>`



#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [nv-button](../nv-button)
- [nv-text-field](../nv-text-field)

### Graph
```mermaid
graph TD;
  nv-dialog-prompt --> nv-button
  nv-dialog-prompt --> nv-text-field
  nv-text-field --> nv-button
  style nv-dialog-prompt fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
