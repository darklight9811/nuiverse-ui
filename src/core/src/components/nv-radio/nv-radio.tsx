/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Event, EventEmitter, Watch }  from '@stencil/core';
import { RadioChangeEventDetail, Color }     from '../../interface';

@Component({
  tag: 'nv-radio',
  shadow: false,
})
export class NvRadio {

  //
  // Props
  //

  /**
   * If `true`, the user cannot interact with the radio.
   */
  @Prop() disabled?: boolean = true;

  /**
   * The name of the control, which is submitted with the form data
   */
  @Prop() name?: string;

  /**
   * If `true`, the radio is selected.
   */
  @Prop({ mutable: true }) checked?: boolean;

  /**
   * The value of the radio
   */
  @Prop() value?: any | null;

  /**
   * The color to use from your application's color palette.
   */
  @Prop() color?: Color = 'primary';

  //
  // Events
  //

  /**
   * Emitted when the radio is selected.
   */
  @Event() nvSelect!: EventEmitter<RadioChangeEventDetail>;

  /**
   * Emitted when the radio is deselected.
   */
  @Event() nvDeselected!: EventEmitter<RadioChangeEventDetail>;

  //
  // Watch
  //

  @Watch('checked')
  handleChecked(isChecked: boolean) {
    if (isChecked) {
      this.nvSelect.emit({
        checked: true,
        value: this.value,
      });
    }
  }

  //
  // Methods
  //

  private updateCheck = () => {
    if (this.checked) {
      this.nvDeselected.emit();
    } else {
      this.checked = true;
    }
  }

  render() {
    return (
      <div
        role='radiogroup'
        class={{
          'nv-radio': true,
          'nv-radio-checked': this.checked,
          'nv-radio-disabled': this.disabled,
          'nv-radio-primary': this.color === 'primary' ? true : false,
          'nv-radio-secondary': this.color === 'secondary' ? true : false,
          'nv-radio-warning': this.color === 'warning' ? true : false,
          'nv-radio-danger': this.color === 'danger' ? true : false,
          'nv-radio-success': this.color === 'success' ? true : false,
        }}
        onClick={ this.updateCheck }
      >
        <input
          type='radio'
          name={ this.name }
          value={ this.value }
          checked={ this.checked }
        />
        <div class='checkmark-container'>
          <span class='checkmark'></span>
        </div>
      </div>
    );
  }
}
