# nv-radio



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                    | Type                                                             | Default     |
| ---------- | ---------- | -------------------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `checked`  | `checked`  | If `true`, the radio is selected.                              | `boolean`                                                        | `undefined` |
| `color`    | `color`    | The color to use from your application color palette.          | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `'primary'` |
| `disabled` | `disabled` | If `true`, the user cannot interecat with the radio.           | `boolean`                                                        | `true`      |
| `name`     | `name`     | The name of the control, which is submitted with the form data | `string`                                                         | `undefined` |
| `value`    | `value`    | The value of the radio                                         | `any`                                                            | `undefined` |


## Events

| Event          | Description                           | Type                                  |
| -------------- | ------------------------------------- | ------------------------------------- |
| `nvDeselected` | Emitted when the radio is deselected. | `CustomEvent<RadioChangeEventDetail>` |
| `nvSelect`     | Emitted when the radio is selected.   | `CustomEvent<RadioChangeEventDetail>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
