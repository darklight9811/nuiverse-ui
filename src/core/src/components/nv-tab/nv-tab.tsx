/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Host,
  Prop,
  Element,
  Method,
  Event,
  EventEmitter,
  Listen,
}                                from '@stencil/core';
import { runNextAnimationFrame } from '../../utils';

@Component({
  tag: 'nv-tab',
  shadow: false,
})
export class NvTab {
  public rootTab!: HTMLNvTabsElement;
  /**
   * Reference to the host element.
   */
  @Element() el!: HTMLNvTabElement;

  /** @internal */
  @Prop({ mutable: true }) active = false;

  /**
   * A tab id must be provided for each `nv-tab`. It's used internally to reference
   * the selected tab or by the router to switch between them.
   */
  @Prop() tab!: string;

  //
  // Events
  //

  /**
   * Emitted when the navigation from previuos tab has finished.
   */
  @Event() nvPrevTabDidChange!: EventEmitter<{tab: string}>;

  @Listen('animationend')
  handleScroll(evt) {
    if (evt.type === 'animationend') {

      if (this.el.classList.contains('nv-displaying-tab')) {
        this.el.classList.add('nv-tab-displayed');
        this.el.classList.remove('nv-displaying-tab');
      }

      // go back to initial state after hiding
      if (this.el.classList.contains('nv-hiding-tab')) {
        this.el.classList.remove('nv-tab-displayed');
        this.el.classList.remove('nv-displaying-tab');
        this.el.classList.remove('nv-hiding-tab');
        this.nvPrevTabDidChange.emit({
          tab: this.tab,
        });
        this.active = false;
      }
    }
  }

  //
  // Lificycles
  //
  componentWillLoad() {
    this.rootTab = this.el.parentElement as HTMLNvTabsElement;
  }

  //
  // Methods
  //

  /**
   * Set the active component for the tab
   */
  @Method()
  async setActive(): Promise<void> {
    const tab: HTMLNvTabElement = this.el;
    this.active = true;

    // Wait a frame once display is no longer "none", to establish basis for the animation
    runNextAnimationFrame(() => {
      // no operation during displaying and hiding
      if (tab.classList.contains('nv-displaying-tab') ||
      tab.classList.contains('nv-hiding-tab')) {
        return;
      }

      tab.classList.add('nv-displaying-tab');
    });
  }

  /**
   * Remove the active component of the tab.
   */
  @Method()
  async removeActive(): Promise<void> {
    this.el.classList.add('nv-hiding-tab');
  }

  render() {
    return (
      <Host
        role='tabpanel'
        aria-hidden={!this.active ? 'true' : null}
        aria-labelledby={`tab-button-${this.tab}`}
        class={{
          'nv-tab': true,
          'nv-tab-hidden': !this.active,
        }}
      >
        <div class='nv-tab-content'>
          <slot />
        </div>
      </Host>
    );
  }
}
