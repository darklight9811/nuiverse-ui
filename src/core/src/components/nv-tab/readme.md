# nv-tab



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute | Description                                                                                                                              | Type     | Default     |
| ------------------ | --------- | ---------------------------------------------------------------------------------------------------------------------------------------- | -------- | ----------- |
| `tab` _(required)_ | `tab`     | A tab id must be provided for each `nv-tab`. It's used internally to reference the selected tab or by the router to switch between them. | `string` | `undefined` |


## Events

| Event                | Description                                                 | Type                            |
| -------------------- | ----------------------------------------------------------- | ------------------------------- |
| `nvPrevTabDidChange` | Emitted when the navigation from previuos tab has finished. | `CustomEvent<{ tab: string; }>` |


## Methods

### `removeActive() => Promise<void>`

Remote the active componente for the tab.

#### Returns

Type: `Promise<void>`



### `setActive() => Promise<void>`

Set the active component for the tab

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
