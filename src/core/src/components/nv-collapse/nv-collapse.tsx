/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Element,
  Prop,
  Method }                from '@stencil/core';
import { ICollapseItem }  from './types';

@Component({
  tag: 'nv-collapse',
  shadow: false,
})
export class NvCollapse {

  children: HTMLCollection;
  /**
   * Reference the host element
   */
  @Element() el: HTMLElement;


  // Lifecycles

  componentDidLoad() {
    const nvCollapse = this.el.querySelector('.nv-collapse');
    if (nvCollapse) {
      this.children = nvCollapse.children;
      if (this.accordion) {
        for (const child of this.children) {
          const item = child as HTMLElement;
          item.setAttribute('accordion', '');
        }
      }
    }
  }

  //
  // Props
  //

  /**
   * If `true`, the collapse will have a accordion effect.
   */
  @Prop() accordion: boolean = false;

  //
  // Methods
  //

  /**
   * Call this method to close all Collapse Itens.
   */
  @Method()
  async closeAllItems(el: HTMLElement) {
    for (const child of this.children) {
      const item = child as ICollapseItem;
      if (item !== el) {
        item.close();
      }
    }
  }

  render() {
    return (
      <div class='nv-collapse'>
        <slot />
      </div>
    );
  }
}
