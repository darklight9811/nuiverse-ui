/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

//
// Interface for the root element, and to add public APIs.
//

export interface IRootEl extends HTMLElement {
  closeAllItems?: (el: HTMLElement) => void;
}

export interface ICollapseItem extends HTMLElement {
  toggleContent?: () => void;
  close?: () => void;
}
