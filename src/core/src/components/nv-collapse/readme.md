# nv-collapse



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute   | Description                                  | Type      | Default |
| ----------- | ----------- | -------------------------------------------- | --------- | ------- |
| `accordion` | `accordion` | If `true`, the will have a accordion effect. | `boolean` | `false` |


## Methods

### `closeAllItems(el: HTMLElement) => Promise<void>`

Call this method for close all Collapse Itens.

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
