# nv-card



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description                    | Type      | Default |
| --------- | --------- | ------------------------------ | --------- | ------- |
| `compact` | `compact` | The compact mode for the card. | `boolean` | `false` |


## Dependencies

### Depends on

- [nv-card-media](../nv-card-media)

### Graph
```mermaid
graph TD;
  nv-card --> nv-card-media
  style nv-card fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
