/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element, Prop } from '@stencil/core';

@Component({
  tag: 'nv-card',
  shadow: false,
})
export class NvCard {
  /**
   *  Reference to the host element
   */
  @Element() el: HTMLElement;

  /**
   * The compact mode for the card.
   */
  @Prop() compact?: boolean = false;

  componentDidLoad() {
    // Set attribute on media
    if (this.compact) {
      let media: HTMLElement = this.el.querySelector('nv-card-media');

      if (!media) {
        media = document.createElement('nv-card-media');
        media.setAttribute('type', 'compact');
        this.el.querySelector('.nv-card-body').append(media);
        return;
      }

      media.setAttribute('type-card', 'compact');
    }
  }

  render() {
    return (
      <div
        class={{
          'nv-card': true,
          'nv-card-compact-mode': this.compact,
        }}
      >
        <slot />
      </div>
    );
  }
}
