# nv-dialog-action



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                  | Type                                                             | Default     |
| ------------- | -------------- | -------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `color`       | `color`        | The color for the confirm button             | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `headerTitle` | `header-title` | The header title for the dialog              | `boolean`                                                        | `false`     |
| `opened`      | `opened`       | If `true`, the dialog is displayed.          | `boolean`                                                        | `false`     |
| `textCancel`  | `text-cancel`  | The text for cancel button from this dialog  | `string`                                                         | `undefined` |
| `textConfirm` | `text-confirm` | The text for confirm button from this dialog | `string`                                                         | `undefined` |
| `type`        | `type`         | The type from the dialog                     | `string`                                                         | `'confirm'` |


## Methods

### `close() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `confirm() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `toggle() => Promise<void>`



#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [nv-button](../nv-button)

### Graph
```mermaid
graph TD;
  nv-dialog-action --> nv-button
  style nv-dialog-action fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
