/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Prop,
  Host,
  Element,
  Event,
  EventEmitter,
  Listen,
}                                       from '@stencil/core';
import {
  NvTabButtonClickEventDetail,
  NvTabBarChangedEventDetail }          from '../../interface';

@Component({
  tag: 'nv-tab-button',
  shadow: false,
})
export class NvTabButton {
  /**
   * Reference to the Host element
   */
  @Element() el: HTMLNvTabButtonElement;

  /**
   * If `true`, the user cannot interact with the tab button.
   */
  @Prop() disabled = false;

  /**
   * A tab id must be provided for each `nv-tab`. It's used internally to reference
   * the selected tab or by the router to switch between them.
   */
  @Prop() tab?: string;

  /**
   * The selected tab component
   */
  @Prop() selected?: boolean;

  //
  // Events
  //

  /**
   * Emitted when the tab bar is clicked
   * @internal
   */
  @Event() nvTabButtonClick!: EventEmitter<NvTabButtonClickEventDetail>;

  @Listen('nvTabBarChanged', { target: 'parent' })
  onTabBarChanged(ev: CustomEvent<NvTabBarChangedEventDetail>) {
    this.selected = this.tab === ev.detail.tab;
  }

  private selectTab(ev: Event | KeyboardEvent) {
    if (this.tab !== undefined) {
      if (!this.disabled) {
        this.nvTabButtonClick.emit({
          tab: this.tab,
          selected: this.selected,
        });
      }
      ev.preventDefault();
    }
  }

  private onClick = (ev: Event): void => {
    if (!this.selected) {
      this.selectTab(ev);
    }
  }

  private get tabIndex() {
    if (this.disabled) {
      return -1;
    }

    const hasTabIndex = this.el.hasAttribute('tabindex');

    if (hasTabIndex) {
      return this.el.getAttribute('tabindex');
    }

    return 0;
  }

  render() {
    return (
      <Host
        role='tab'
        tabindex={ this.tabIndex }
        aria-selected={ this.selected ? 'true' : null }
        id={ this.tab !== undefined ? `tab-button-${ this.tab }` : null}
        onClick={ this.onClick }
        class={{
          'nv-tab-button': true,
          'nv-tab-button-active': this.selected,
        }}
      >
        <nv-button text>
          <slot />
        </nv-button>
      </Host>
    );
  }
}
