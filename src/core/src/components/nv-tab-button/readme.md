# nv-tab-button



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                                                                                              | Type      | Default     |
| ---------- | ---------- | ---------------------------------------------------------------------------------------------------------------------------------------- | --------- | ----------- |
| `disabled` | `disabled` | If `true`, the user cannot interact with the tab button.                                                                                 | `boolean` | `false`     |
| `selected` | `selected` | The selected tab component                                                                                                               | `boolean` | `undefined` |
| `tab`      | `tab`      | A tab id must be provided for each `nv-tab`. It's used internally to reference the selected tab or by the router to switch between them. | `string`  | `undefined` |


## Dependencies

### Depends on

- [nv-button](../nv-button)

### Graph
```mermaid
graph TD;
  nv-tab-button --> nv-button
  style nv-tab-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
