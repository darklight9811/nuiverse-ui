/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  Prop,
  h,
  Host,
  Event,
  EventEmitter,
  Method,
  Element,
} from '@stencil/core';
import {
  NvNavViewItemDidChange,
} from '../../interface';

@Component({
  tag: 'nv-navigation-view-item',
  shadow: false,
})
export class NvNavigationItem {
  //
  // Reference to the element hosy
  //

  @Element() el: HTMLNvNavigationViewItemElement;

  //
  // Props
  //

  /**
   * If `true`, the navigation items is selected.
   */
  @Prop({ mutable: true }) active?: boolean = false;

  //
  // Events
  //

  /**
   * @Internal - Emitted when the NavItem is selected.
   */
  @Event() nvNavViewItemDidChange: EventEmitter<NvNavViewItemDidChange>;


  //
  // Methods
  //

  /**
   * Set the NavigationItem active.
   */
  @Method()
  async setActive(): Promise<void> {
    this.active = true;
    this.nvNavViewItemDidChange.emit({ item: this.el });
  }

  /**
   * Remove the NavigationItem active.
   */
  @Method()
  async removeActive(): Promise<void> {
    this.active = false;
  }

  private onClick = (): void => {
    this.active = true;
    this.nvNavViewItemDidChange.emit({ item: this.el });
  }

  render() {
    return (
      <Host
        class={{
          'nv-navigation-view-item': true,
          'nv-navigation-view-item-active': this.active,
        }}
        onClick={ this.onClick }
      >
        <slot/>
      </Host>
    );
  }
}
