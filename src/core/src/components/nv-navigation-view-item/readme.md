# nv-navigation-view-item



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                  | Type      | Default |
| -------- | --------- | -------------------------------------------- | --------- | ------- |
| `active` | `active`  | If `true`, the navigation items is selected. | `boolean` | `false` |


## Events

| Event                    | Description | Type                                  |
| ------------------------ | ----------- | ------------------------------------- |
| `nvNavViewItemDidChange` |             | `CustomEvent<NvNavViewItemDidChange>` |


## Methods

### `removeActive() => Promise<void>`

Remove the NavigationItem active.

#### Returns

Type: `Promise<void>`



### `setActive() => Promise<void>`

Set the NavigationItem active.

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
