/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Config }    from '@stencil/core';
import { postcss }   from '@stencil/postcss';
import autoprefixer  from 'autoprefixer';
import { sass }      from '@stencil/sass';

export const config: Config = {
  namespace: 'nuiverse-ui',
  plugins: [
    sass(),
    postcss({
      plugins: [autoprefixer()],
    }),
  ],
  globalStyle: 'src/style/nuiverse.scss',
  outputTargets: [
    {
      type: 'dist',
      copy: [
        { src: 'fonts', dest: 'assets/fonts' },
      ],
      esmLoaderPath: '../loader',
    },
    {
      type: 'docs-readme',
      strict: true,
    },
    {
      type: 'docs-json',
      file: '../docs/core.json',
    },
    {
      type: 'dist-hydrate-script',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
      copy: [
        { src: 'fonts', dest: 'collection/assets/fonts' },
        { src: 'demos', dest: 'demos' },
      ],
    },
  ],
  preamble: '(C) Nuiverse UI by Nuinalp - BSD-3-Clause',
  enableCache: true,
};
