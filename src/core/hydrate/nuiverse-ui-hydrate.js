'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

/**
 * Default style mode id
 */
/**
 * Reusable empty obj/array
 * Don't add values to these!!
 */
const EMPTY_OBJ = {};
/**
 * Namespaces
 */
const SVG_NS = 'http://www.w3.org/2000/svg';

const isDef = (v) => v != null;
const toLowerCase = (str) => str.toLowerCase();
const isComplexType = (o) => {
    // https://jsperf.com/typeof-fn-object/5
    o = typeof o;
    return o === 'object' || o === 'function';
};

let scopeId;
let contentRef;
let hostTagName;
let useNativeShadowDom = false;
let checkSlotFallbackVisibility = false;
let checkSlotRelocate = false;
let isSvgMode = false;
const parsePropertyValue = (propValue, propType) => {
    // ensure this value is of the correct prop type
    if (propValue != null && !isComplexType(propValue)) {
        if ( propType & 4 /* Boolean */) {
            // per the HTML spec, any string value means it is a boolean true value
            // but we'll cheat here and say that the string "false" is the boolean false
            return (propValue === 'false' ? false : propValue === '' || !!propValue);
        }
        if ( propType & 2 /* Number */) {
            // force it to be a number
            return parseFloat(propValue);
        }
        if ( propType & 1 /* String */) {
            // could have been passed as a number or boolean
            // but we still want it as a string
            return String(propValue);
        }
        // redundant return here for better minification
        return propValue;
    }
    // not sure exactly what type we want
    // so no need to change to a different type
    return propValue;
};
const CONTENT_REF_ID = 'r';
const ORG_LOCATION_ID = 'o';
const SLOT_NODE_ID = 's';
const TEXT_NODE_ID = 't';
const HYDRATED_CLASS = 'hydrated';
const HYDRATE_ID = 's-id';
const HYDRATE_CHILD_ID = 'c-id';
const XLINK_NS = 'http://www.w3.org/1999/xlink';
/**
 * Production h() function based on Preact by
 * Jason Miller (@developit)
 * Licensed under the MIT License
 * https://github.com/developit/preact/blob/master/LICENSE
 *
 * Modified for Stencil's compiler and vdom
 */
// const stack: any[] = [];
// export function h(nodeName: string | d.FunctionalComponent, vnodeData: d.PropsType, child?: d.ChildType): d.VNode;
// export function h(nodeName: string | d.FunctionalComponent, vnodeData: d.PropsType, ...children: d.ChildType[]): d.VNode;
const h = (nodeName, vnodeData, ...children) => {
    let child = null;
    let simple = false;
    let lastSimple = false;
    let key;
    let slotName;
    let vNodeChildren = [];
    const walk = (c) => {
        for (let i = 0; i < c.length; i++) {
            child = c[i];
            if (Array.isArray(child)) {
                walk(child);
            }
            else if (child != null && typeof child !== 'boolean') {
                if (simple = typeof nodeName !== 'function' && !isComplexType(child)) {
                    child = String(child);
                }
                if (simple && lastSimple) {
                    // If the previous child was simple (string), we merge both
                    vNodeChildren[vNodeChildren.length - 1].$text$ += child;
                }
                else {
                    // Append a new vNode, if it's text, we create a text vNode
                    vNodeChildren.push(simple ? { $flags$: 0, $text$: child } : child);
                }
                lastSimple = simple;
            }
        }
    };
    walk(children);
    if ( vnodeData) {
        // normalize class / classname attributes
        {
            key = vnodeData.key || undefined;
        }
        {
            slotName = vnodeData.name;
        }
        {
            const classData = vnodeData.className || vnodeData.class;
            if (classData) {
                vnodeData.class = typeof classData !== 'object'
                    ? classData
                    : Object.keys(classData)
                        .filter(k => classData[k])
                        .join(' ');
            }
        }
    }
    if ( typeof nodeName === 'function') {
        // nodeName is a functional component
        return nodeName(vnodeData, vNodeChildren, vdomFnUtils);
    }
    const vnode = {
        $flags$: 0,
        $tag$: nodeName,
        $children$: vNodeChildren.length > 0 ? vNodeChildren : null,
        $elm$: undefined,
        $attrs$: vnodeData,
    };
    {
        vnode.$key$ = key;
    }
    {
        vnode.$name$ = slotName;
    }
    return vnode;
};
const Host = {};
const isHost = (node) => {
    return node && node.$tag$ === Host;
};
const vdomFnUtils = {
    'forEach': (children, cb) => children.map(convertToPublic).forEach(cb),
    'map': (children, cb) => children.map(convertToPublic).map(cb).map(convertToPrivate)
};
const convertToPublic = (node) => {
    return {
        vattrs: node.$attrs$,
        vchildren: node.$children$,
        vkey: node.$key$,
        vname: node.$name$,
        vtag: node.$tag$,
        vtext: node.$text$
    };
};
const convertToPrivate = (node) => {
    return {
        $flags$: 0,
        $attrs$: node.vattrs,
        $children$: node.vchildren,
        $key$: node.vkey,
        $name$: node.vname,
        $tag$: node.vtag,
        $text$: node.vtext
    };
};
/**
 * Production setAccessor() function based on Preact by
 * Jason Miller (@developit)
 * Licensed under the MIT License
 * https://github.com/developit/preact/blob/master/LICENSE
 *
 * Modified for Stencil's compiler and vdom
 */
const setAccessor = (elm, memberName, oldValue, newValue, isSvg, flags) => {
    if (oldValue === newValue) {
        return;
    }
    if ( memberName === 'class') {
        const classList = elm.classList;
        parseClassList(oldValue).forEach(cls => classList.remove(cls));
        parseClassList(newValue).forEach(cls => classList.add(cls));
    }
    else if ( memberName === 'style') {
        // update style attribute, css properties and values
        {
            for (const prop in oldValue) {
                if (!newValue || newValue[prop] == null) {
                    {
                        elm.style[prop] = '';
                    }
                }
            }
        }
        for (const prop in newValue) {
            if (!oldValue || newValue[prop] !== oldValue[prop]) {
                {
                    elm.style[prop] = newValue[prop];
                }
            }
        }
    }
    else if ( memberName === 'key')
        ;
    else if ( memberName === 'ref') {
        // minifier will clean this up
        if (newValue) {
            newValue(elm);
        }
    }
    else if ( memberName.startsWith('on') && !isMemberInElement(elm, memberName)) {
        // Event Handlers
        // so if the member name starts with "on" and the 3rd characters is
        // a capital letter, and it's not already a member on the element,
        // then we're assuming it's an event listener
        if (isMemberInElement(elm, toLowerCase(memberName))) {
            // standard event
            // the JSX attribute could have been "onMouseOver" and the
            // member name "onmouseover" is on the element's prototype
            // so let's add the listener "mouseover", which is all lowercased
            memberName = toLowerCase(memberName.substring(2));
        }
        else {
            // custom event
            // the JSX attribute could have been "onMyCustomEvent"
            // so let's trim off the "on" prefix and lowercase the first character
            // and add the listener "myCustomEvent"
            // except for the first character, we keep the event name case
            memberName = toLowerCase(memberName[2]) + memberName.substring(3);
        }
        if (oldValue) {
            plt.rel(elm, memberName, oldValue, false);
        }
        if (newValue) {
            plt.ael(elm, memberName, newValue, false);
        }
    }
    else {
        // Set property if it exists and it's not a SVG
        const isProp = isMemberInElement(elm, memberName);
        const isComplex = isComplexType(newValue);
        if ((isProp || (isComplex && newValue !== null)) && !isSvg) {
            try {
                if (!elm.tagName.includes('-')) {
                    const n = newValue == null ? '' : newValue;
                    // Workaround for Safari, moving the <input> caret when re-assigning the same valued
                    if (elm[memberName] !== n) {
                        elm[memberName] = n;
                    }
                }
                else {
                    elm[memberName] = newValue;
                }
            }
            catch (e) { }
        }
        /**
         * Need to manually update attribute if:
         * - memberName is not an attribute
         * - if we are rendering the host element in order to reflect attribute
         * - if it's a SVG, since properties might not work in <svg>
         * - if the newValue is null/undefined or 'false'.
         */
        const isXlinkNs =  isSvg && (memberName !== (memberName = memberName.replace(/^xlink\:?/, ''))) ? true : false;
        if (newValue == null || newValue === false) {
            if (isXlinkNs) {
                elm.removeAttributeNS(XLINK_NS, toLowerCase(memberName));
            }
            else {
                elm.removeAttribute(memberName);
            }
        }
        else if ((!isProp || (flags & 4 /* isHost */) || isSvg) && !isComplex) {
            newValue = newValue === true ? '' : newValue.toString();
            if (isXlinkNs) {
                elm.setAttributeNS(XLINK_NS, toLowerCase(memberName), newValue);
            }
            else {
                elm.setAttribute(memberName, newValue);
            }
        }
    }
};
const parseClassList = (value) => (!value) ? [] : value.split(/\s+/).filter(c => c);
const updateElement = (oldVnode, newVnode, isSvgMode, memberName) => {
    // if the element passed in is a shadow root, which is a document fragment
    // then we want to be adding attrs/props to the shadow root's "host" element
    // if it's not a shadow root, then we add attrs/props to the same element
    const elm = (newVnode.$elm$.nodeType === 11 /* DocumentFragment */ && newVnode.$elm$.host) ? newVnode.$elm$.host : newVnode.$elm$;
    const oldVnodeAttrs = (oldVnode && oldVnode.$attrs$) || EMPTY_OBJ;
    const newVnodeAttrs = newVnode.$attrs$ || EMPTY_OBJ;
    {
        // remove attributes no longer present on the vnode by setting them to undefined
        for (memberName in oldVnodeAttrs) {
            if (!(memberName in newVnodeAttrs)) {
                setAccessor(elm, memberName, oldVnodeAttrs[memberName], undefined, isSvgMode, newVnode.$flags$);
            }
        }
    }
    // add new & update changed attributes
    for (memberName in newVnodeAttrs) {
        setAccessor(elm, memberName, oldVnodeAttrs[memberName], newVnodeAttrs[memberName], isSvgMode, newVnode.$flags$);
    }
};
const createElm = (oldParentVNode, newParentVNode, childIndex, parentElm) => {
    // tslint:disable-next-line: prefer-const
    let newVNode = newParentVNode.$children$[childIndex];
    let i = 0;
    let elm;
    let childNode;
    let oldVNode;
    if ( !useNativeShadowDom) {
        // remember for later we need to check to relocate nodes
        checkSlotRelocate = true;
        if (newVNode.$tag$ === 'slot') {
            if (scopeId) {
                // scoped css needs to add its scoped id to the parent element
                parentElm.classList.add(scopeId + '-s');
            }
            if (!newVNode.$children$) {
                // slot element does not have fallback content
                // create an html comment we'll use to always reference
                // where actual slot content should sit next to
                newVNode.$flags$ |= 1 /* isSlotReference */;
            }
            else {
                // slot element has fallback content
                // still create an element that "mocks" the slot element
                newVNode.$flags$ |= 2 /* isSlotFallback */;
            }
        }
    }
    if (isDef(newVNode.$text$)) {
        // create text node
        newVNode.$elm$ = doc.createTextNode(newVNode.$text$);
    }
    else if ( newVNode.$flags$ & 1 /* isSlotReference */) {
        // create a slot reference node
        newVNode.$elm$ =  doc.createComment(`slot-reference:${hostTagName}`) ;
    }
    else {
        // create element
        elm = newVNode.$elm$ = (( (isSvgMode || newVNode.$tag$ === 'svg'))
            ? doc.createElementNS(SVG_NS, newVNode.$tag$)
            : doc.createElement(( newVNode.$flags$ & 2 /* isSlotFallback */) ? 'slot-fb' : newVNode.$tag$));
        {
            isSvgMode = newVNode.$tag$ === 'svg' ? true : (newVNode.$tag$ === 'foreignObject' ? false : isSvgMode);
        }
        // add css classes, attrs, props, listeners, etc.
        {
            updateElement(null, newVNode, isSvgMode);
        }
        if (newVNode.$children$) {
            for (i = 0; i < newVNode.$children$.length; ++i) {
                // create the node
                childNode = createElm(oldParentVNode, newVNode, i, elm);
                // return node could have been null
                if (childNode) {
                    // append our new node
                    elm.appendChild(childNode);
                }
            }
        }
        {
            if (newVNode.$tag$ === 'svg') {
                // Only reset the SVG context when we're exiting <svg> element
                isSvgMode = false;
            }
            else if (newVNode.$elm$.tagName === 'foreignObject') {
                // Reenter SVG context when we're exiting <foreignObject> element
                isSvgMode = true;
            }
        }
    }
    {
        newVNode.$elm$['s-hn'] = hostTagName;
        if (newVNode.$flags$ & (2 /* isSlotFallback */ | 1 /* isSlotReference */)) {
            // remember the content reference comment
            newVNode.$elm$['s-sr'] = true;
            // remember the content reference comment
            newVNode.$elm$['s-cr'] = contentRef;
            // remember the slot name, or empty string for default slot
            newVNode.$elm$['s-sn'] = newVNode.$name$ || '';
            // check if we've got an old vnode for this slot
            oldVNode = oldParentVNode && oldParentVNode.$children$ && oldParentVNode.$children$[childIndex];
            if (oldVNode && oldVNode.$tag$ === newVNode.$tag$ && oldParentVNode.$elm$) {
                // we've got an old slot vnode and the wrapper is being replaced
                // so let's move the old slot content back to it's original location
                putBackInOriginalLocation(oldParentVNode.$elm$, false);
            }
        }
    }
    return newVNode.$elm$;
};
const putBackInOriginalLocation = (parentElm, recursive) => {
    plt.$flags$ |= 1 /* isTmpDisconnected */;
    const oldSlotChildNodes = parentElm.childNodes;
    for (let i = oldSlotChildNodes.length - 1; i >= 0; i--) {
        const childNode = oldSlotChildNodes[i];
        if (childNode['s-hn'] !== hostTagName && childNode['s-ol']) {
            // // this child node in the old element is from another component
            // // remove this node from the old slot's parent
            // childNode.remove();
            // and relocate it back to it's original location
            parentReferenceNode(childNode).insertBefore(childNode, referenceNode(childNode));
            // remove the old original location comment entirely
            // later on the patch function will know what to do
            // and move this to the correct spot in need be
            childNode['s-ol'].remove();
            childNode['s-ol'] = undefined;
            checkSlotRelocate = true;
        }
        if (recursive) {
            putBackInOriginalLocation(childNode, recursive);
        }
    }
    plt.$flags$ &= ~1 /* isTmpDisconnected */;
};
const addVnodes = (parentElm, before, parentVNode, vnodes, startIdx, endIdx) => {
    let containerElm = (( parentElm['s-cr'] && parentElm['s-cr'].parentNode) || parentElm);
    let childNode;
    for (; startIdx <= endIdx; ++startIdx) {
        if (vnodes[startIdx]) {
            childNode = createElm(null, parentVNode, startIdx, parentElm);
            if (childNode) {
                vnodes[startIdx].$elm$ = childNode;
                containerElm.insertBefore(childNode,  referenceNode(before) );
            }
        }
    }
};
const removeVnodes = (vnodes, startIdx, endIdx, elm) => {
    for (; startIdx <= endIdx; ++startIdx) {
        if (isDef(vnodes[startIdx])) {
            elm = vnodes[startIdx].$elm$;
            callNodeRefs(vnodes[startIdx], true);
            {
                // we're removing this element
                // so it's possible we need to show slot fallback content now
                checkSlotFallbackVisibility = true;
                if (elm['s-ol']) {
                    // remove the original location comment
                    elm['s-ol'].remove();
                }
                else {
                    // it's possible that child nodes of the node
                    // that's being removed are slot nodes
                    putBackInOriginalLocation(elm, true);
                }
            }
            // remove the vnode's element from the dom
            elm.remove();
        }
    }
};
const updateChildren = (parentElm, oldCh, newVNode, newCh) => {
    let oldStartIdx = 0;
    let newStartIdx = 0;
    let idxInOld = 0;
    let i = 0;
    let oldEndIdx = oldCh.length - 1;
    let oldStartVnode = oldCh[0];
    let oldEndVnode = oldCh[oldEndIdx];
    let newEndIdx = newCh.length - 1;
    let newStartVnode = newCh[0];
    let newEndVnode = newCh[newEndIdx];
    let node;
    let elmToMove;
    while (oldStartIdx <= oldEndIdx && newStartIdx <= newEndIdx) {
        if (oldStartVnode == null) {
            // Vnode might have been moved left
            oldStartVnode = oldCh[++oldStartIdx];
        }
        else if (oldEndVnode == null) {
            oldEndVnode = oldCh[--oldEndIdx];
        }
        else if (newStartVnode == null) {
            newStartVnode = newCh[++newStartIdx];
        }
        else if (newEndVnode == null) {
            newEndVnode = newCh[--newEndIdx];
        }
        else if (isSameVnode(oldStartVnode, newStartVnode)) {
            patch(oldStartVnode, newStartVnode);
            oldStartVnode = oldCh[++oldStartIdx];
            newStartVnode = newCh[++newStartIdx];
        }
        else if (isSameVnode(oldEndVnode, newEndVnode)) {
            patch(oldEndVnode, newEndVnode);
            oldEndVnode = oldCh[--oldEndIdx];
            newEndVnode = newCh[--newEndIdx];
        }
        else if (isSameVnode(oldStartVnode, newEndVnode)) {
            // Vnode moved right
            if ( (oldStartVnode.$tag$ === 'slot' || newEndVnode.$tag$ === 'slot')) {
                putBackInOriginalLocation(oldStartVnode.$elm$.parentNode, false);
            }
            patch(oldStartVnode, newEndVnode);
            parentElm.insertBefore(oldStartVnode.$elm$, oldEndVnode.$elm$.nextSibling);
            oldStartVnode = oldCh[++oldStartIdx];
            newEndVnode = newCh[--newEndIdx];
        }
        else if (isSameVnode(oldEndVnode, newStartVnode)) {
            // Vnode moved left
            if ( (oldStartVnode.$tag$ === 'slot' || newEndVnode.$tag$ === 'slot')) {
                putBackInOriginalLocation(oldEndVnode.$elm$.parentNode, false);
            }
            patch(oldEndVnode, newStartVnode);
            parentElm.insertBefore(oldEndVnode.$elm$, oldStartVnode.$elm$);
            oldEndVnode = oldCh[--oldEndIdx];
            newStartVnode = newCh[++newStartIdx];
        }
        else {
            // createKeyToOldIdx
            idxInOld = -1;
            {
                for (i = oldStartIdx; i <= oldEndIdx; ++i) {
                    if (oldCh[i] && isDef(oldCh[i].$key$) && oldCh[i].$key$ === newStartVnode.$key$) {
                        idxInOld = i;
                        break;
                    }
                }
            }
            if ( idxInOld >= 0) {
                elmToMove = oldCh[idxInOld];
                if (elmToMove.$tag$ !== newStartVnode.$tag$) {
                    node = createElm(oldCh && oldCh[newStartIdx], newVNode, idxInOld, parentElm);
                }
                else {
                    patch(elmToMove, newStartVnode);
                    oldCh[idxInOld] = undefined;
                    node = elmToMove.$elm$;
                }
                newStartVnode = newCh[++newStartIdx];
            }
            else {
                // new element
                node = createElm(oldCh && oldCh[newStartIdx], newVNode, newStartIdx, parentElm);
                newStartVnode = newCh[++newStartIdx];
            }
            if (node) {
                {
                    parentReferenceNode(oldStartVnode.$elm$).insertBefore(node, referenceNode(oldStartVnode.$elm$));
                }
            }
        }
    }
    if (oldStartIdx > oldEndIdx) {
        addVnodes(parentElm, (newCh[newEndIdx + 1] == null ? null : newCh[newEndIdx + 1].$elm$), newVNode, newCh, newStartIdx, newEndIdx);
    }
    else if ( newStartIdx > newEndIdx) {
        removeVnodes(oldCh, oldStartIdx, oldEndIdx);
    }
};
const isSameVnode = (vnode1, vnode2) => {
    // compare if two vnode to see if they're "technically" the same
    // need to have the same element tag, and same key to be the same
    if (vnode1.$tag$ === vnode2.$tag$) {
        if ( vnode1.$tag$ === 'slot') {
            return vnode1.$name$ === vnode2.$name$;
        }
        {
            return vnode1.$key$ === vnode2.$key$;
        }
        return true;
    }
    return false;
};
const referenceNode = (node) => {
    // this node was relocated to a new location in the dom
    // because of some other component's slot
    // but we still have an html comment in place of where
    // it's original location was according to it's original vdom
    return (node && node['s-ol']) || node;
};
const parentReferenceNode = (node) => (node['s-ol'] ? node['s-ol'] : node).parentNode;
const patch = (oldVNode, newVNode) => {
    const elm = newVNode.$elm$ = oldVNode.$elm$;
    const oldChildren = oldVNode.$children$;
    const newChildren = newVNode.$children$;
    let defaultHolder;
    {
        // test if we're rendering an svg element, or still rendering nodes inside of one
        // only add this to the when the compiler sees we're using an svg somewhere
        isSvgMode = elm &&
            isDef(elm.parentNode) &&
            elm.ownerSVGElement !== undefined;
        isSvgMode = newVNode.$tag$ === 'svg' ? true : (newVNode.$tag$ === 'foreignObject' ? false : isSvgMode);
    }
    if (!isDef(newVNode.$text$)) {
        // element node
        {
            if ( newVNode.$tag$ === 'slot')
                ;
            else {
                // either this is the first render of an element OR it's an update
                // AND we already know it's possible it could have changed
                // this updates the element's css classes, attrs, props, listeners, etc.
                updateElement(oldVNode, newVNode, isSvgMode);
            }
        }
        if ( isDef(oldChildren) && isDef(newChildren)) {
            // looks like there's child vnodes for both the old and new vnodes
            updateChildren(elm, oldChildren, newVNode, newChildren);
        }
        else if (isDef(newChildren)) {
            // no old child vnodes, but there are new child vnodes to add
            if ( isDef(oldVNode.$text$)) {
                // the old vnode was text, so be sure to clear it out
                elm.textContent = '';
            }
            // add the new vnode children
            addVnodes(elm, null, newVNode, newChildren, 0, newChildren.length - 1);
        }
        else if ( isDef(oldChildren)) {
            // no new child vnodes, but there are old child vnodes to remove
            removeVnodes(oldChildren, 0, oldChildren.length - 1);
        }
    }
    else if ( (defaultHolder = elm['s-cr'])) {
        // this element has slotted content
        defaultHolder.parentNode.textContent = newVNode.$text$;
    }
    else if ( oldVNode.$text$ !== newVNode.$text$) {
        // update the text content for the text only vnode
        // and also only if the text is different than before
        elm.textContent = newVNode.$text$;
    }
    if ( isSvgMode && newVNode.$tag$ === 'svg') {
        isSvgMode = false;
    }
};
const updateFallbackSlotVisibility = (elm, childNode, childNodes, i, ilen, j, slotNameAttr, nodeType) => {
    childNodes = elm.childNodes;
    for (i = 0, ilen = childNodes.length; i < ilen; i++) {
        childNode = childNodes[i];
        if (childNode.nodeType === 1 /* ElementNode */) {
            if (childNode['s-sr']) {
                // this is a slot fallback node
                // get the slot name for this slot reference node
                slotNameAttr = childNode['s-sn'];
                // by default always show a fallback slot node
                // then hide it if there are other slots in the light dom
                childNode.hidden = false;
                for (j = 0; j < ilen; j++) {
                    if (childNodes[j]['s-hn'] !== childNode['s-hn']) {
                        // this sibling node is from a different component
                        nodeType = childNodes[j].nodeType;
                        if (slotNameAttr !== '') {
                            // this is a named fallback slot node
                            if (nodeType === 1 /* ElementNode */ && slotNameAttr === childNodes[j].getAttribute('slot')) {
                                childNode.hidden = true;
                                break;
                            }
                        }
                        else {
                            // this is a default fallback slot node
                            // any element or text node (with content)
                            // should hide the default fallback slot node
                            if (nodeType === 1 /* ElementNode */ || (nodeType === 3 /* TextNode */ && childNodes[j].textContent.trim() !== '')) {
                                childNode.hidden = true;
                                break;
                            }
                        }
                    }
                }
            }
            // keep drilling down
            updateFallbackSlotVisibility(childNode);
        }
    }
};
const relocateNodes = [];
const relocateSlotContent = (elm) => {
    // tslint:disable-next-line: prefer-const
    let childNodes = elm.childNodes;
    let ilen = childNodes.length;
    let i = 0;
    let j = 0;
    let nodeType = 0;
    let childNode;
    let node;
    let hostContentNodes;
    let slotNameAttr;
    for (ilen = childNodes.length; i < ilen; i++) {
        childNode = childNodes[i];
        if (childNode['s-sr'] && (node = childNode['s-cr'])) {
            // first got the content reference comment node
            // then we got it's parent, which is where all the host content is in now
            hostContentNodes = node.parentNode.childNodes;
            slotNameAttr = childNode['s-sn'];
            for (j = hostContentNodes.length - 1; j >= 0; j--) {
                node = hostContentNodes[j];
                if (!node['s-cn'] && !node['s-nr'] && node['s-hn'] !== childNode['s-hn']) {
                    // let's do some relocating to its new home
                    // but never relocate a content reference node
                    // that is suppose to always represent the original content location
                    nodeType = node.nodeType;
                    if (((nodeType === 3 /* TextNode */ || nodeType === 8 /* CommentNode */) && slotNameAttr === '') ||
                        (nodeType === 1 /* ElementNode */ && node.getAttribute('slot') === null && slotNameAttr === '') ||
                        (nodeType === 1 /* ElementNode */ && node.getAttribute('slot') === slotNameAttr)) {
                        // it's possible we've already decided to relocate this node
                        if (!relocateNodes.some(r => r.$nodeToRelocate$ === node)) {
                            // made some changes to slots
                            // let's make sure we also double check
                            // fallbacks are correctly hidden or shown
                            checkSlotFallbackVisibility = true;
                            node['s-sn'] = slotNameAttr;
                            // add to our list of nodes to relocate
                            relocateNodes.push({
                                $slotRefNode$: childNode,
                                $nodeToRelocate$: node
                            });
                        }
                    }
                }
            }
        }
        if (childNode.nodeType === 1 /* ElementNode */) {
            relocateSlotContent(childNode);
        }
    }
};
const callNodeRefs = (vNode, isDestroy) => {
    if ( vNode) {
        vNode.$attrs$ && vNode.$attrs$.ref && vNode.$attrs$.ref(isDestroy ? null : vNode.$elm$);
        vNode.$children$ && vNode.$children$.forEach(vChild => {
            callNodeRefs(vChild, isDestroy);
        });
    }
};
const renderVdom = (hostElm, hostRef, cmpMeta, renderFnResults) => {
    hostTagName = toLowerCase(hostElm.tagName);
    const oldVNode = hostRef.$vnode$ || { $flags$: 0 };
    const rootVnode = isHost(renderFnResults)
        ? renderFnResults
        : h(null, null, renderFnResults);
    if ( cmpMeta.$attrsToReflect$) {
        rootVnode.$attrs$ = rootVnode.$attrs$ || {};
        cmpMeta.$attrsToReflect$.forEach(([propName, attribute]) => rootVnode.$attrs$[attribute] = hostElm[propName]);
    }
    rootVnode.$tag$ = null;
    rootVnode.$flags$ |= 4 /* isHost */;
    hostRef.$vnode$ = rootVnode;
    rootVnode.$elm$ = oldVNode.$elm$ = ( hostElm);
    {
        contentRef = hostElm['s-cr'];
        useNativeShadowDom = supportsShadowDom ;
        // always reset
        checkSlotRelocate = checkSlotFallbackVisibility = false;
    }
    // synchronous patch
    patch(oldVNode, rootVnode);
    {
        if (checkSlotRelocate) {
            relocateSlotContent(rootVnode.$elm$);
            for (let i = 0; i < relocateNodes.length; i++) {
                const relocateNode = relocateNodes[i];
                if (!relocateNode.$nodeToRelocate$['s-ol']) {
                    // add a reference node marking this node's original location
                    // keep a reference to this node for later lookups
                    const orgLocationNode =  doc.createComment(`org-loc`)
                        ;
                    orgLocationNode['s-nr'] = relocateNode.$nodeToRelocate$;
                    relocateNode.$nodeToRelocate$.parentNode.insertBefore((relocateNode.$nodeToRelocate$['s-ol'] = orgLocationNode), relocateNode.$nodeToRelocate$);
                }
            }
            // while we're moving nodes around existing nodes, temporarily disable
            // the disconnectCallback from working
            plt.$flags$ |= 1 /* isTmpDisconnected */;
            for (let i = 0; i < relocateNodes.length; i++) {
                const relocateNode = relocateNodes[i];
                // by default we're just going to insert it directly
                // after the slot reference node
                const parentNodeRef = relocateNode.$slotRefNode$.parentNode;
                let insertBeforeNode = relocateNode.$slotRefNode$.nextSibling;
                let orgLocationNode = relocateNode.$nodeToRelocate$['s-ol'];
                while (orgLocationNode = orgLocationNode.previousSibling) {
                    let refNode = orgLocationNode['s-nr'];
                    if (refNode &&
                        refNode['s-sn'] === relocateNode.$nodeToRelocate$['s-sn'] &&
                        parentNodeRef === refNode.parentNode) {
                        refNode = refNode.nextSibling;
                        if (!refNode || !refNode['s-nr']) {
                            insertBeforeNode = refNode;
                            break;
                        }
                    }
                }
                if ((!insertBeforeNode && parentNodeRef !== relocateNode.$nodeToRelocate$.parentNode) ||
                    (relocateNode.$nodeToRelocate$.nextSibling !== insertBeforeNode)) {
                    // we've checked that it's worth while to relocate
                    // since that the node to relocate
                    // has a different next sibling or parent relocated
                    if (relocateNode.$nodeToRelocate$ !== insertBeforeNode) {
                        // add it back to the dom but in its new home
                        parentNodeRef.insertBefore(relocateNode.$nodeToRelocate$, insertBeforeNode);
                    }
                }
            }
            // done moving nodes around
            // allow the disconnect callback to work again
            plt.$flags$ &= ~1 /* isTmpDisconnected */;
        }
        if (checkSlotFallbackVisibility) {
            updateFallbackSlotVisibility(rootVnode.$elm$);
        }
        // always reset
        relocateNodes.length = 0;
    }
};
const scheduleUpdate = (elm, hostRef, cmpMeta, isInitialLoad) => {
    {
        hostRef.$flags$ |= 16 /* isQueuedForUpdate */;
    }
    const instance =  hostRef.$lazyInstance$ ;
    const update = () => updateComponent(elm, hostRef, cmpMeta, instance, isInitialLoad);
    let promise;
    if (isInitialLoad) {
        {
            hostRef.$flags$ |= 256 /* isListenReady */;
        }
        if ( hostRef.$queuedListeners$) {
            hostRef.$queuedListeners$.forEach(([methodName, event]) => safeCall(instance, methodName, event));
            hostRef.$queuedListeners$ = null;
        }
        {
            promise = safeCall(instance, 'componentWillLoad');
        }
    }
    else {
        {
            promise = safeCall(instance, 'componentWillUpdate');
        }
    }
    // there is no ancestorc omponent or the ancestor component
    // has already fired off its lifecycle update then
    // fire off the initial update
    return then(promise,  () => writeTask(update)
        );
};
const updateComponent = (elm, hostRef, cmpMeta, instance, isInitialLoad) => {
    // updateComponent
    {
        hostRef.$flags$ &= ~16 /* isQueuedForUpdate */;
    }
    {
        elm['s-lr'] = false;
    }
    {
        {
            // tell the platform we're actively rendering
            // if a value is changed within a render() then
            // this tells the platform not to queue the change
            hostRef.$flags$ |= 4 /* isActiveRender */;
            try {
                // looks like we've got child nodes to render into this host element
                // or we need to update the css class/attrs on the host element
                // DOM WRITE!
                renderVdom(elm, hostRef, cmpMeta,  instance.render() );
            }
            catch (e) {
                consoleError(e);
            }
            hostRef.$flags$ &= ~4 /* isActiveRender */;
        }
    }
    {
        try {
            // manually connected child components during server-side hydrate
            serverSideConnected(elm);
            if (isInitialLoad && (cmpMeta.$flags$ & 1 /* shadowDomEncapsulation */)) {
                // using only during server-side hydrate
                elm['s-sd'] = true;
            }
        }
        catch (e) {
            consoleError(e);
        }
    }
    // set that this component lifecycle rendering has completed
    {
        elm['s-lr'] = true;
    }
    {
        hostRef.$flags$ |= 2 /* hasRendered */;
    }
    if ( elm['s-rc'].length > 0) {
        // ok, so turns out there are some child host elements
        // waiting on this parent element to load
        // let's fire off all update callbacks waiting
        elm['s-rc'].forEach(cb => cb());
        elm['s-rc'].length = 0;
    }
    postUpdateComponent(elm, hostRef);
};
const postUpdateComponent = (elm, hostRef, ancestorsActivelyLoadingChildren) => {
    if ( !elm['s-al']) {
        const instance =  hostRef.$lazyInstance$ ;
        const ancestorComponent = hostRef.$ancestorComponent$;
        if (!(hostRef.$flags$ & 64 /* hasLoadedComponent */)) {
            hostRef.$flags$ |= 64 /* hasLoadedComponent */;
            {
                // DOM WRITE!
                // add the css class that this element has officially hydrated
                elm.classList.add(HYDRATED_CLASS);
            }
            {
                safeCall(instance, 'componentDidLoad');
            }
            {
                hostRef.$onReadyResolve$(elm);
            }
            if ( !ancestorComponent) {
                appDidLoad();
            }
        }
        else {
            {
                // we've already loaded this component
                // fire off the user's componentDidUpdate method (if one was provided)
                // componentDidUpdate runs AFTER render() has been called
                // and all child components have finished updating
                safeCall(instance, 'componentDidUpdate');
            }
        }
        // load events fire from bottom to top
        // the deepest elements load first then bubbles up
        if ( ancestorComponent) {
            // ok so this element already has a known ancestor component
            // let's make sure we remove this element from its ancestor's
            // known list of child elements which are actively loading
            if (ancestorsActivelyLoadingChildren = ancestorComponent['s-al']) {
                // remove this element from the actively loading map
                ancestorsActivelyLoadingChildren.delete(elm);
                // the ancestor's initializeComponent method will do the actual checks
                // to see if the ancestor is actually loaded or not
                // then let's call the ancestor's initializeComponent method if there's no length
                // (which actually ends up as this method again but for the ancestor)
                if (ancestorsActivelyLoadingChildren.size === 0) {
                    ancestorComponent['s-al'] = undefined;
                    ancestorComponent['s-init']();
                }
            }
            hostRef.$ancestorComponent$ = undefined;
        }
        // ( •_•)
        // ( •_•)>⌐■-■
        // (⌐■_■)
    }
};
const appDidLoad = () => {
    // on appload
    // we have finish the first big initial render
    {
        doc.documentElement.classList.add(HYDRATED_CLASS);
    }
};
const safeCall = (instance, method, arg) => {
    if (instance && instance[method]) {
        try {
            return instance[method](arg);
        }
        catch (e) {
            consoleError(e);
        }
    }
    return undefined;
};
const then = (promise, thenFn) => {
    return promise && promise.then ? promise.then(thenFn) : thenFn();
};
const serverSideConnected = (elm) => {
    const children = elm.children;
    if (children != null) {
        for (let i = 0, ii = children.length; i < ii; i++) {
            const childElm = children[i];
            if (typeof childElm.connectedCallback === 'function') {
                childElm.connectedCallback();
            }
            serverSideConnected(childElm);
        }
    }
};
const getValue = (ref, propName) => getHostRef(ref).$instanceValues$.get(propName);
const setValue = (ref, propName, newVal, cmpMeta) => {
    // check our new property value against our internal value
    const hostRef = getHostRef(ref);
    const elm =  hostRef.$hostElement$ ;
    const oldVal = hostRef.$instanceValues$.get(propName);
    const flags = hostRef.$flags$;
    newVal = parsePropertyValue(newVal, cmpMeta.$members$[propName][0]);
    if (newVal !== oldVal && ( !(flags & 8 /* isConstructingInstance */) || oldVal === undefined)) {
        // gadzooks! the property's value has changed!!
        // set our new value!
        hostRef.$instanceValues$.set(propName, newVal);
        if ( hostRef.$lazyInstance$) {
            // get an array of method names of watch functions to call
            if ( cmpMeta.$watchers$ && flags & 128 /* isWatchReady */) {
                const watchMethods = cmpMeta.$watchers$[propName];
                if (watchMethods) {
                    // this instance is watching for when this property changed
                    watchMethods.forEach(watchMethodName => {
                        try {
                            // fire off each of the watch methods that are watching this property
                            ( hostRef.$lazyInstance$ )[watchMethodName].call(( hostRef.$lazyInstance$ ), newVal, oldVal, propName);
                        }
                        catch (e) {
                            consoleError(e);
                        }
                    });
                }
            }
            if ( (flags & (4 /* isActiveRender */ | 2 /* hasRendered */ | 16 /* isQueuedForUpdate */)) === 2 /* hasRendered */) {
                // looks like this value actually changed, so we've got work to do!
                // but only if we've already rendered, otherwise just chill out
                // queue that we need to do an update, but don't worry about queuing
                // up millions cuz this function ensures it only runs once
                scheduleUpdate(elm, hostRef, cmpMeta, false);
            }
        }
    }
};
const proxyComponent = (Cstr, cmpMeta, flags) => {
    if ( cmpMeta.$members$) {
        if ( Cstr.watchers) {
            cmpMeta.$watchers$ = Cstr.watchers;
        }
        // It's better to have a const than two Object.entries()
        const members = Object.entries(cmpMeta.$members$);
        const prototype = Cstr.prototype;
        members.forEach(([memberName, [memberFlags]]) => {
            if ( ((memberFlags & 31 /* Prop */) ||
                (( flags & 2 /* proxyState */) &&
                    (memberFlags & 32 /* State */)))) {
                // proxyComponent - prop
                Object.defineProperty(prototype, memberName, {
                    get() {
                        // proxyComponent, get value
                        return getValue(this, memberName);
                    },
                    set(newValue) {
                        // proxyComponent, set value
                        setValue(this, memberName, newValue, cmpMeta);
                    },
                    configurable: true,
                    enumerable: true
                });
            }
            else if ( (flags & 1 /* isElementConstructor */) && (memberFlags & 64 /* Method */)) {
                // proxyComponent - method
                Object.defineProperty(prototype, memberName, {
                    value(...args) {
                        const ref = getHostRef(this);
                        return ref.$onReadyPromise$.then(() => ref.$lazyInstance$[memberName](...args));
                    }
                });
            }
        });
        if ( ( flags & 1 /* isElementConstructor */)) {
            const attrNameToPropName = new Map();
            prototype.attributeChangedCallback = function (attrName, _oldValue, newValue) {
                plt.jmp(() => {
                    const propName = attrNameToPropName.get(attrName);
                    this[propName] = newValue === null && typeof this[propName] === 'boolean'
                        ? false
                        : newValue;
                });
            };
            // create an array of attributes to observe
            // and also create a map of html attribute name to js property name
            Cstr.observedAttributes = members
                .filter(([_, m]) => m[0] & 15 /* HasAttribute */) // filter to only keep props that should match attributes
                .map(([propName, m]) => {
                const attrName = m[1] || propName;
                attrNameToPropName.set(attrName, propName);
                if ( m[0] & 512 /* ReflectAttr */) {
                    cmpMeta.$attrsToReflect$.push([propName, attrName]);
                }
                return attrName;
            });
        }
    }
    return Cstr;
};
const addEventListeners = (elm, hostRef, listeners) => {
    hostRef.$queuedListeners$ = hostRef.$queuedListeners$ || [];
    const removeFns = listeners.map(([flags, name, method]) => {
        const target = ( getHostListenerTarget(elm, flags) );
        const handler = hostListenerProxy(hostRef, method);
        const opts = hostListenerOpts(flags);
        plt.ael(target, name, handler, opts);
        return () => plt.rel(target, name, handler, opts);
    });
    return () => removeFns.forEach(fn => fn());
};
const hostListenerProxy = (hostRef, methodName) => {
    return (ev) => {
        {
            if (hostRef.$flags$ & 256 /* isListenReady */) {
                // instance is ready, let's call it's member method for this event
                hostRef.$lazyInstance$[methodName](ev);
            }
            else {
                hostRef.$queuedListeners$.push([methodName, ev]);
            }
        }
    };
};
const getHostListenerTarget = (elm, flags) => {
    if ( flags & 8 /* TargetWindow */)
        return win;
    if ( flags & 16 /* TargetParent */)
        return elm.parentElement;
    return elm;
};
const hostListenerOpts = (flags) =>  (flags & 2 /* Capture */) !== 0;
const initializeComponent = async (elm, hostRef, cmpMeta, hmrVersionId, Cstr) => {
    // initializeComponent
    if ( (hostRef.$flags$ & 32 /* hasInitializedComponent */) === 0) {
        // we haven't initialized this element yet
        hostRef.$flags$ |= 32 /* hasInitializedComponent */;
        if ( hostRef.$modeName$) {
            elm.setAttribute('s-mode', hostRef.$modeName$);
        }
        {
            // lazy loaded components
            // request the component's implementation to be
            // wired up with the host element
            Cstr = loadModule(cmpMeta);
            if (Cstr.then) {
                // Await creates a micro-task avoid if possible
                Cstr = await Cstr;
            }
            if ( !Cstr.isProxied) {
                // we'eve never proxied this Constructor before
                // let's add the getters/setters to its prototype before
                // the first time we create an instance of the implementation
                {
                    cmpMeta.$watchers$ = Cstr.watchers;
                }
                proxyComponent(Cstr, cmpMeta, 2 /* proxyState */);
                Cstr.isProxied = true;
            }
            // ok, time to construct the instance
            // but let's keep track of when we start and stop
            // so that the getters/setters don't incorrectly step on data
            {
                hostRef.$flags$ |= 8 /* isConstructingInstance */;
            }
            // construct the lazy-loaded component implementation
            // passing the hostRef is very important during
            // construction in order to directly wire together the
            // host element and the lazy-loaded instance
            try {
                new Cstr(hostRef);
            }
            catch (e) {
                consoleError(e);
            }
            {
                hostRef.$flags$ &= ~8 /* isConstructingInstance */;
            }
            {
                hostRef.$flags$ |= 128 /* isWatchReady */;
            }
        }
    }
    // we've successfully created a lazy instance
    const ancestorComponent = hostRef.$ancestorComponent$;
    const schedule = () => scheduleUpdate(elm, hostRef, cmpMeta, true);
    if ( ancestorComponent && ancestorComponent['s-lr'] === false && ancestorComponent['s-rc']) {
        // this is the intial load and this component it has an ancestor component
        // but the ancestor component has NOT fired its will update lifecycle yet
        // so let's just cool our jets and wait for the ancestor to continue first
        // this will get fired off when the ancestor component
        // finally gets around to rendering its lazy self
        // fire off the initial update
        ancestorComponent['s-rc'].push(schedule);
    }
    else {
        schedule();
    }
};
const connectedCallback = (elm, cmpMeta) => {
    if ((plt.$flags$ & 1 /* isTmpDisconnected */) === 0) {
        // connectedCallback
        const hostRef = getHostRef(elm);
        if ( cmpMeta.$listeners$) {
            // initialize our event listeners on the host element
            // we do this now so that we can listening to events that may
            // have fired even before the instance is ready
            hostRef.$rmListeners$ = addEventListeners(elm, hostRef, cmpMeta.$listeners$);
        }
        if (!(hostRef.$flags$ & 1 /* hasConnected */)) {
            // first time this component has connected
            hostRef.$flags$ |= 1 /* hasConnected */;
            let hostId;
            if ( !hostId) {
                // initUpdate
                // if the slot polyfill is required we'll need to put some nodes
                // in here to act as original content anchors as we move nodes around
                // host element has been connected to the DOM
                {
                    setContentReference(elm);
                }
            }
            {
                // find the first ancestor component (if there is one) and register
                // this component as one of the actively loading child components for its ancestor
                let ancestorComponent = elm;
                while ((ancestorComponent = (ancestorComponent.parentNode || ancestorComponent.host))) {
                    // climb up the ancestors looking for the first
                    // component that hasn't finished its lifecycle update yet
                    if ( (ancestorComponent['s-init'] && ancestorComponent['s-lr'] === false)) {
                        // we found this components first ancestor component
                        // keep a reference to this component's ancestor component
                        hostRef.$ancestorComponent$ = ancestorComponent;
                        // ensure there is an array to contain a reference to each of the child components
                        // and set this component as one of the ancestor's child components it should wait on
                        (ancestorComponent['s-al'] = ancestorComponent['s-al'] || new Set()).add(elm);
                        break;
                    }
                }
            }
            {
                // connectedCallback, taskQueue, initialLoad
                // angular sets attribute AFTER connectCallback
                // https://github.com/angular/angular/issues/18909
                // https://github.com/angular/angular/issues/19940
                nextTick(() => initializeComponent(elm, hostRef, cmpMeta));
            }
        }
    }
};
const setContentReference = (elm, contentRefElm) => {
    // only required when we're NOT using native shadow dom (slot)
    // or this browser doesn't support native shadow dom
    // and this host element was NOT created with SSR
    // let's pick out the inner content for slot projection
    // create a node to represent where the original
    // content was first placed, which is useful later on
    contentRefElm = elm['s-cr'] = doc.createComment( '');
    contentRefElm['s-cn'] = true;
    elm.insertBefore(contentRefElm, elm.firstChild);
};
const createEvent = (ref, name, flags) => {
    const elm = getElement(ref);
    return {
        emit: (detail) => {
            return elm.dispatchEvent(new ( win.CustomEvent )(name, {
                bubbles: !!(flags & 4 /* Bubbles */),
                composed: !!(flags & 2 /* Composed */),
                cancelable: !!(flags & 1 /* Cancellable */),
                detail
            }));
        }
    };
};
const getElement = (ref) =>  getHostRef(ref).$hostElement$ ;
const insertVdomAnnotations = (doc) => {
    if (doc != null) {
        const docData = {
            hostIds: 0,
            rootLevelIds: 0
        };
        const orgLocationNodes = [];
        parseVNodeAnnotations(doc, doc.body, docData, orgLocationNodes);
        orgLocationNodes.forEach(orgLocationNode => {
            if (orgLocationNode != null) {
                const nodeRef = orgLocationNode['s-nr'];
                let hostId = nodeRef['s-host-id'];
                let nodeId = nodeRef['s-node-id'];
                let childId = `${hostId}.${nodeId}`;
                if (hostId == null) {
                    hostId = 0;
                    docData.rootLevelIds++;
                    nodeId = docData.rootLevelIds;
                    childId = `${hostId}.${nodeId}`;
                    if (nodeRef.nodeType === 1 /* ElementNode */) {
                        nodeRef.setAttribute(HYDRATE_CHILD_ID, childId);
                    }
                    else if (nodeRef.nodeType === 3 /* TextNode */) {
                        if (hostId === 0) {
                            const textContent = nodeRef.nodeValue.trim();
                            if (textContent === '') {
                                // useless whitespace node at the document root
                                orgLocationNode.remove();
                                return;
                            }
                        }
                        const commentBeforeTextNode = doc.createComment(childId);
                        commentBeforeTextNode.nodeValue = `${TEXT_NODE_ID}.${childId}`;
                        nodeRef.parentNode.insertBefore(commentBeforeTextNode, nodeRef);
                    }
                }
                let orgLocationNodeId = `${ORG_LOCATION_ID}.${childId}`;
                const orgLocationParentNode = orgLocationNode.parentElement;
                if (orgLocationParentNode && orgLocationParentNode['s-sd']) {
                    // ending with a . means that the parent element
                    // of this node's original location is a shadow dom element
                    // and this node is apart of the root level light dom
                    orgLocationNodeId += `.`;
                }
                orgLocationNode.nodeValue = orgLocationNodeId;
            }
        });
    }
};
const parseVNodeAnnotations = (doc, node, docData, orgLocationNodes) => {
    if (node == null) {
        return;
    }
    if (node['s-nr'] != null) {
        orgLocationNodes.push(node);
    }
    if (node.nodeType === 1 /* ElementNode */) {
        node.childNodes.forEach(childNode => {
            const hostRef = getHostRef(childNode);
            if (hostRef != null) {
                const cmpData = {
                    nodeIds: 0
                };
                insertVNodeAnnotations(doc, childNode, hostRef.$vnode$, docData, cmpData);
            }
            parseVNodeAnnotations(doc, childNode, docData, orgLocationNodes);
        });
    }
};
const insertVNodeAnnotations = (doc, hostElm, vnode, docData, cmpData) => {
    if (vnode != null) {
        const hostId = ++docData.hostIds;
        hostElm.setAttribute(HYDRATE_ID, hostId);
        if (hostElm['s-cr'] != null) {
            hostElm['s-cr'].nodeValue = `${CONTENT_REF_ID}.${hostId}`;
        }
        if (vnode.$children$ != null) {
            const depth = 0;
            vnode.$children$.forEach((vnodeChild, index) => {
                insertChildVNodeAnnotations(doc, vnodeChild, cmpData, hostId, depth, index);
            });
        }
    }
};
const insertChildVNodeAnnotations = (doc, vnodeChild, cmpData, hostId, depth, index) => {
    const childElm = vnodeChild.$elm$;
    if (childElm == null) {
        return;
    }
    const nodeId = cmpData.nodeIds++;
    const childId = `${hostId}.${nodeId}.${depth}.${index}`;
    childElm['s-host-id'] = hostId;
    childElm['s-node-id'] = nodeId;
    if (childElm.nodeType === 1 /* ElementNode */) {
        childElm.setAttribute(HYDRATE_CHILD_ID, childId);
    }
    else if (childElm.nodeType === 3 /* TextNode */) {
        const parentNode = childElm.parentNode;
        if (parentNode.nodeName !== 'STYLE') {
            const textNodeId = `${TEXT_NODE_ID}.${childId}`;
            const commentBeforeTextNode = doc.createComment(textNodeId);
            parentNode.insertBefore(commentBeforeTextNode, childElm);
        }
    }
    else if (childElm.nodeType === 8 /* CommentNode */) {
        if (childElm['s-sr']) {
            const slotName = (childElm['s-sn'] || '');
            const slotNodeId = `${SLOT_NODE_ID}.${childId}.${slotName}`;
            childElm.nodeValue = slotNodeId;
        }
    }
    if (vnodeChild.$children$ != null) {
        const childDepth = depth + 1;
        vnodeChild.$children$.forEach((vnode, index) => {
            insertChildVNodeAnnotations(doc, vnode, cmpData, hostId, childDepth, index);
        });
    }
};

function proxyHostElement(elm, cmpMeta) {
    if (typeof elm.componentOnReady !== 'function') {
        elm.componentOnReady = componentOnReady;
    }
    if (typeof elm.forceUpdate !== 'function') {
        elm.forceUpdate = forceUpdate;
    }
    if (cmpMeta.$members$ != null) {
        const hostRef = getHostRef(elm);
        const members = Object.entries(cmpMeta.$members$);
        members.forEach(([memberName, m]) => {
            const memberFlags = m[0];
            if (memberFlags & 31) {
                const attributeName = (m[1] || memberName);
                const attrValue = elm.getAttribute(attributeName);
                if (attrValue != null) {
                    const parsedAttrValue = parsePropertyValue(attrValue, memberFlags);
                    hostRef.$instanceValues$.set(memberName, parsedAttrValue);
                }
                const ownValue = elm[memberName];
                if (ownValue !== undefined) {
                    hostRef.$instanceValues$.set(memberName, ownValue);
                    delete elm[memberName];
                }
                Object.defineProperty(elm, memberName, {
                    get() {
                        return getValue(this, memberName);
                    },
                    set(newValue) {
                        setValue(this, memberName, newValue, cmpMeta);
                    },
                    configurable: true,
                    enumerable: true
                });
            }
            else if (memberFlags & 64) {
                Object.defineProperty(elm, memberName, {
                    value() {
                        const ref = getHostRef(this);
                        const args = arguments;
                        return ref.$onReadyPromise$.then(() => ref.$lazyInstance$[memberName].apply(ref.$lazyInstance$, args)).catch(consoleError);
                    }
                });
            }
        });
    }
}
function componentOnReady() {
    return getHostRef(this).$onReadyPromise$;
}
function forceUpdate() { }

function hydrateComponent(win, results, tagName, elm, waitPromises) {
    const Cstr = getComponent(tagName);
    if (Cstr != null) {
        const cmpMeta = Cstr.cmpMeta;
        if (cmpMeta != null) {
            const hydratePromise = new Promise(async (resolve) => {
                try {
                    registerHost(elm);
                    proxyHostElement(elm, cmpMeta);
                    connectedCallback(elm, cmpMeta);
                    await elm.componentOnReady();
                    results.hydratedCount++;
                    const ref = getHostRef(elm);
                    const modeName = !ref.$modeName$ ? '$' : ref.$modeName$;
                    if (!results.hydratedComponents.some(c => c.tag === tagName && c.mode === modeName)) {
                        results.hydratedComponents.push({
                            tag: tagName,
                            mode: modeName
                        });
                    }
                }
                catch (e) {
                    win.console.error(e);
                }
                resolve();
            });
            waitPromises.push(hydratePromise);
        }
    }
}

function bootstrapHydrate(win, opts, done) {
    const results = {
        hydratedCount: 0,
        hydratedComponents: []
    };
    plt.$resourcesUrl$ = new URL(opts.resourcesUrl || './', doc.baseURI).href;
    try {
        const connectedElements = new Set();
        const waitPromises = [];
        const patchedConnectedCallback = function patchedConnectedCallback() {
            connectElements(win, opts, results, this, connectedElements, waitPromises);
        };
        const patchedComponentInit = function patchedComponentInit() {
            const hostRef = getHostRef(this);
            if (hostRef != null) {
                postUpdateComponent(this, hostRef);
            }
        };
        const patchComponent = function (elm) {
            const tagName = elm.nodeName.toLowerCase();
            if (elm.tagName.includes('-')) {
                const Cstr = getComponent(tagName);
                if (Cstr != null) {
                    if (typeof elm.connectedCallback !== 'function') {
                        elm.connectedCallback = patchedConnectedCallback;
                    }
                    if (typeof elm['s-init'] !== 'function') {
                        elm['s-rc'] = [];
                        elm['s-init'] = patchedComponentInit;
                    }
                }
            }
        };
        let orgDocumentCreateElement = win.document.createElement;
        win.document.createElement = function patchedCreateElement(tagName) {
            const elm = orgDocumentCreateElement.call(win.document, tagName);
            patchComponent(elm);
            return elm;
        };
        const patchChild = (elm) => {
            if (elm != null && elm.nodeType === 1) {
                patchComponent(elm);
                const children = elm.children;
                for (let i = 0, ii = children.length; i < ii; i++) {
                    patchChild(children[i]);
                }
            }
        };
        patchChild(win.document.body);
        const initConnectElement = (elm) => {
            if (elm != null && elm.nodeType === 1) {
                if (typeof elm.connectedCallback === 'function') {
                    elm.connectedCallback();
                }
                const children = elm.children;
                for (let i = 0, ii = children.length; i < ii; i++) {
                    initConnectElement(children[i]);
                }
            }
        };
        initConnectElement(win.document.body);
        Promise.all(waitPromises)
            .then(() => {
            try {
                waitPromises.length = 0;
                connectedElements.clear();
                if (opts.clientHydrateAnnotations) {
                    insertVdomAnnotations(win.document);
                }
                win.document.createElement = orgDocumentCreateElement;
                win = opts = orgDocumentCreateElement = null;
            }
            catch (e) {
                win.console.error(e);
            }
            done(results);
        })
            .catch(e => {
            try {
                win.console.error(e);
                waitPromises.length = 0;
                connectedElements.clear();
                win.document.createElement = orgDocumentCreateElement;
                win = opts = orgDocumentCreateElement = null;
            }
            catch (e) { }
            done(results);
        });
    }
    catch (e) {
        win.console.error(e);
        win = opts = null;
        done(results);
    }
}
function connectElements(win, opts, results, elm, connectedElements, waitPromises) {
    if (elm != null && elm.nodeType === 1 && results.hydratedCount < opts.maxHydrateCount && shouldHydrate(elm)) {
        const tagName = elm.nodeName.toLowerCase();
        if (tagName.includes('-') && !connectedElements.has(elm)) {
            connectedElements.add(elm);
            hydrateComponent(win, results, tagName, elm, waitPromises);
        }
        const children = elm.children;
        if (children != null) {
            for (let i = 0, ii = children.length; i < ii; i++) {
                connectElements(win, opts, results, children[i], connectedElements, waitPromises);
            }
        }
    }
}
function shouldHydrate(elm) {
    if (elm.nodeType === 9) {
        return true;
    }
    if (NO_HYDRATE_TAGS.has(elm.nodeName)) {
        return false;
    }
    if (elm.hasAttribute('no-prerender')) {
        return false;
    }
    const parentNode = elm.parentNode;
    if (parentNode == null) {
        return true;
    }
    return shouldHydrate(parentNode);
}
const NO_HYDRATE_TAGS = new Set([
    'CODE',
    'HEAD',
    'IFRAME',
    'INPUT',
    'OBJECT',
    'OUTPUT',
    'NOSCRIPT',
    'PRE',
    'SCRIPT',
    'SELECT',
    'STYLE',
    'TEMPLATE',
    'TEXTAREA'
]);

const cstrs = new Map();
const loadModule = (cmpMeta, _hostRef, _hmrVersionId) => {
    return new Promise(resolve => {
        resolve(cstrs.get(cmpMeta.$tagName$));
    });
};
const getComponent = (tagName) => {
    return cstrs.get(tagName);
};
const isMemberInElement = (elm, memberName) => {
    if (elm != null) {
        if (memberName in elm) {
            return true;
        }
        const hostRef = getComponent(elm.nodeName.toLowerCase());
        if (hostRef != null && hostRef.cmpMeta != null && hostRef.cmpMeta.$members$ != null) {
            return memberName in hostRef.cmpMeta.$members$;
        }
    }
    return false;
};
const registerComponents = (Cstrs) => {
    Cstrs.forEach(Cstr => {
        cstrs.set(Cstr.cmpMeta.$tagName$, Cstr);
    });
};
const win = window;
const doc = win.document;
const writeTask = (cb) => {
    process.nextTick(() => {
        try {
            cb();
        }
        catch (e) {
            consoleError(e);
        }
    });
};
const nextTick = (cb) => Promise.resolve().then(cb);
const consoleError = (e) => {
    if (e != null) {
        console.error(e.stack || e.message || e);
    }
};
const plt = {
    $flags$: 0,
    $resourcesUrl$: '',
    jmp: (h) => h(),
    raf: (h) => requestAnimationFrame(h),
    ael: (el, eventName, listener, opts) => el.addEventListener(eventName, listener, opts),
    rel: (el, eventName, listener, opts) => el.removeEventListener(eventName, listener, opts),
};
const supportsShadowDom = false;
const hostRefs = new WeakMap();
const getHostRef = (ref) => hostRefs.get(ref);
const registerInstance = (lazyInstance, hostRef) => hostRefs.set(hostRef.$lazyInstance$ = lazyInstance, hostRef);
const registerHost = (elm) => {
    const hostRef = {
        $flags$: 0,
        $hostElement$: elm,
        $instanceValues$: new Map(),
    };
    hostRef.$onReadyPromise$ = new Promise(r => hostRef.$onReadyResolve$ = r);
    return hostRefs.set(elm, hostRef);
};

class NvAlert {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
        * If `true` the alert is active.
        */
        this.active = true;
    }
    render() {
        if (this.active) {
            return (h(Host, { class: {
                    'nv-alert': true,
                    'nv-alert-primary': this.color === 'primary' ? true : false,
                    'nv-alert-secondary': this.color === 'secondary' ? true : false,
                    'nv-alert-warning': this.color === 'warning' ? true : false,
                    'nv-alert-danger': this.color === 'danger' ? true : false,
                    'nv-alert-success': this.color === 'success' ? true : false,
                } }, h("slot", null)));
        }
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-alert",
        "$members$": {
            "active": [4],
            "color": [1]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvAlert$1 {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h("div", { class: 'nv-alert-header' }, h("slot", null)));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-alert-header",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvAvatar {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * The avatar size
         */
        this.size = 'medium';
    }
    renderImg() {
        if (this.src) {
            return h("img", { src: this.src, alt: this.alt });
        }
        return null;
    }
    renderTxt() {
        if (this.text) {
            const text = this.text[0].toUpperCase();
            return (h("span", { class: {
                    'nv-text-primary': this.textColor === 'primary' ? true : false,
                    'nv-text-secondary': this.textColor === 'secondary' ? true : false,
                    'nv-text-warning': this.textColor === 'warning' ? true : false,
                    'nv-text-danger': this.textColor === 'danger' ? true : false,
                    'nv-text-success': this.textColor === 'success' ? true : false,
                } }, text));
        }
        return null;
    }
    render() {
        return (h("div", { class: {
                'nv-avatar': true,
                'nv-avatar-primary': this.color === 'primary' ? true : false,
                'nv-avatar-secondary': this.color === 'secondary' ? true : false,
                'nv-avatar-warning': this.color === 'warning' ? true : false,
                'nv-avatar-danger': this.color === 'danger' ? true : false,
                'nv-avatar-success': this.color === 'success' ? true : false,
                'nv-avatar-small': this.size === 'small' ? true : false,
                'nv-avatar-medium': this.size === 'medium' ? true : false,
                'nv-avatar-large': this.size === 'large' ? true : false,
            } }, h("div", { class: 'nv-avatar-content' }, this.renderTxt(), this.renderImg())));
    }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-avatar",
        "$members$": {
            "text": [1],
            "src": [1],
            "alt": [1],
            "color": [1],
            "textColor": [1, "text-color"],
            "size": [1]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvBadge {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h(Host, { class: {
                'nv-badge': true,
                'nv-badge-primary': this.color === 'primary' ? true : false,
                'nv-badge-secondary': this.color === 'secondary' ? true : false,
                'nv-badge-warning': this.color === 'warning' ? true : false,
                'nv-badge-danger': this.color === 'danger' ? true : false,
                'nv-badge-success': this.color === 'success' ? true : false,
                'nv-badge-pill': this.pill,
            } }, h("slot", null)));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-badge",
        "$members$": {
            "color": [1],
            "pill": [4]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvBreadcrumb {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    makeItem() {
        if (this.href) {
            return (h("a", { class: 'nv-bc-link', href: this.href }, h("slot", null)));
        }
        return h("slot", null);
    }
    render() {
        const item = this.makeItem();
        const current = !this.href ? 'page' : null;
        return (h("li", { class: 'nv-breadcrumb-item', "aria-current": current }, item));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-breadcrumb",
        "$members$": {
            "href": [1]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvBreadcrumbs {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h("nav", null, h("ol", { class: 'nv-breadcrumbs' }, h("slot", null))));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-breadcrumbs",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
function inserted(el) {
    const computed = window.getComputedStyle(el);
    if (computed && (computed.position === 'relative' || computed.position === 'absolute')) {
        const ripples = document.createElement('div');
        ripples.setAttribute('class', 'nv-ripples');
        el.appendChild(ripples);
        addRipple(el, true);
    }
}
function addRipple(el, rippleOk) {
    if (rippleOk) {
        const isTouchSupported = 'ontouchstart' in document;
        if (isTouchSupported) {
            el.addEventListener('touchstart', rippleShow, { passive: true });
        }
        else {
            el.addEventListener('mousedown', rippleShow);
        }
    }
}
function isTouchEvent(e) {
    return e.constructor.name === 'TouchEvent';
}
function rippleShow(e) {
    const element = e.currentTarget;
    const computed = window.getComputedStyle(element);
    const computedWidth = computed.width.replace('px', '');
    const computedHeight = computed.height.replace('px', '');
    const width = parseInt(computedWidth, 10);
    const height = parseInt(computedHeight, 10);
    const rippleWidth = width > height ? width : height;
    const halfRippleWidth = rippleWidth / 2;
    const { left, top } = element.getBoundingClientRect();
    const rippleId = new Date().toISOString();
    const target = isTouchEvent(e) ? e.touches[e.touches.length - 1] : e;
    const containerRipples = element.querySelector('.nv-ripples');
    const ripple = document.createElement('div');
    ripple.setAttribute('style', `width : ${rippleWidth}px;
    height: ${rippleWidth}px;
    left  : ${(target.clientX) - left - halfRippleWidth}px;
    top   : ${target.clientY - top - halfRippleWidth}px;`);
    ripple.setAttribute('id', rippleId);
    ripple.setAttribute('class', 'nv-ripple nv-ripple-enter');
    containerRipples.appendChild(ripple);
    const typeAnim = checkAnimation();
    ripple.addEventListener(typeAnim, (e) => watchEvents(e, element));
}
function watchEvents(evt, btn) {
    const element = evt.currentTarget;
    if (evt.type === checkAnimation()) {
        btn.querySelector('.nv-ripples').removeChild(element);
    }
}
// Check animation support for transtions and animations
function checkAnimation() {
    let t;
    const el = document.createElement('fakeelement');
    const transitions = {
        animation: 'animationend',
        OAnimation: 'oAnimationEnd',
        MozOAnimation: 'animationend',
        WebkitOAnimation: 'webkitAnimationEnd',
    };
    for (t in transitions) {
        if (el.style[t] !== undefined) {
            return transitions[t];
        }
    }
}

class NvButton {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * This property specifies the size of the button.
         */
        this.size = 'medium';
        /**
         * Type of the button.
         */
        this.type = 'button';
        /**
         * The button enable status.
         */
        this.disabled = false;
        /**
         * The button ripple effect enable status
         */
        this.disabledRipple = false;
    }
    componentDidLoad() {
        // Get button element
        const btn = this.el.querySelector('button');
        if (!this.disabledRipple) {
            // Add ripple effect
            inserted(btn);
        }
    }
    actionData() {
        const data = this.dataFor;
        const element = document.querySelector(data);
        if (element) {
            // Dialogs
            if (element.tagName.match(/NV-DIALOG/)) {
                element.toggle();
            }
        }
    }
    render() {
        return (h(Host, { class: {
                'nv-button': true,
                'nv-button-block': this.block,
            } }, h("button", { class: {
                'nv-button-native': true,
                'nv-button-small': this.size === 'small' ? true : false,
                'nv-button-medium': this.size === 'medium' ? true : false,
                'nv-button-large': this.size === 'large' ? true : false,
                'nv-button-big': this.size === 'big' ? true : false,
                'nv-button-primary': this.color === 'primary' ? true : false,
                'nv-button-secondary': this.color === 'secondary' ? true : false,
                'nv-button-warning': this.color === 'warning' ? true : false,
                'nv-button-danger': this.color === 'danger' ? true : false,
                'nv-button-success': this.color === 'success' ? true : false,
                'nv-button-text': this.text,
                'nv-button-rounded': this.rounded,
                'nv-button-outline': this.outline,
            }, type: this.type, disabled: this.disabled, onClick: this.dataFor ? () => this.actionData() : null }, h("span", null, h("slot", null)))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-button",
        "$members$": {
            "textColor": [1, "text-color"],
            "color": [1],
            "size": [1],
            "type": [1],
            "text": [4],
            "block": [4],
            "rounded": [4],
            "outline": [4],
            "disabled": [4],
            "disabledRipple": [4, "disabled-ripple"],
            "dataFor": [1, "data-for"]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCard {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * The compact mode for the card.
         */
        this.compact = false;
    }
    componentDidLoad() {
        // Set attribute on media
        if (this.compact) {
            let media = this.el.querySelector('nv-card-media');
            if (!media) {
                media = document.createElement('nv-card-media');
                media.setAttribute('type', 'compact');
                this.el.querySelector('.nv-card-body').append(media);
                return;
            }
            media.setAttribute('type-card', 'compact');
        }
    }
    render() {
        return (h("div", { class: {
                'nv-card': true,
                'nv-card-compact-mode': this.compact,
            } }, h("slot", null)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-card",
        "$members$": {
            "compact": [4]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCardBody {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    componentDidLoad() {
        // Get button element
        const cardBody = this.el.querySelector('.nv-card-body');
        // Add ripple effect
        inserted(cardBody);
    }
    render() {
        return (h("div", { class: 'nv-card-body' }, h("slot", null)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-card-body",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCardContent {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h("div", { class: 'nv-card-content' }, h("slot", null)));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-card-content",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCardFooter {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h("div", { class: 'nv-card-footer' }, h("slot", null)));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-card-footer",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCardHeader {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h("div", { class: 'nv-card-header' }, h("slot", null)));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-card-header",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCardIcon {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        if (this.src) {
            return (h("div", { class: 'nv-card-icon' }, h("img", { src: this.src, alt: this.alt })));
        }
    }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-card-icon",
        "$members$": {
            "src": [1],
            "alt": [1]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

if(!t)var t={map:function(t,r){var n={};return r?t.map(function(t,o){return n.index=o,r.call(n,t)}):t.slice()},naturalOrder:function(t,r){return t<r?-1:t>r?1:0},sum:function(t,r){var n={};return t.reduce(r?function(t,o,e){return n.index=e,t+r.call(n,o)}:function(t,r){return t+r},0)},max:function(r,n){return Math.max.apply(null,n?t.map(r,n):r)}};var r=function(){var r=5,n=8-r,o=1e3;function e(t,n,o){return (t<<2*r)+(n<<r)+o}function i(t){var r=[],n=!1;function o(){r.sort(t),n=!0;}return {push:function(t){r.push(t),n=!1;},peek:function(t){return n||o(),void 0===t&&(t=r.length-1),r[t]},pop:function(){return n||o(),r.pop()},size:function(){return r.length},map:function(t){return r.map(t)},debug:function(){return n||o(),r}}}function u(t,r,n,o,e,i,u){this.r1=t,this.r2=r,this.g1=n,this.g2=o,this.b1=e,this.b2=i,this.histo=u;}function a(){this.vboxes=new i(function(r,n){return t.naturalOrder(r.vbox.count()*r.vbox.volume(),n.vbox.count()*n.vbox.volume())});}function s(r,n){if(n.count()){var o=n.r2-n.r1+1,i=n.g2-n.g1+1,u=t.max([o,i,n.b2-n.b1+1]);if(1==n.count())return [n.copy()];var a,s,h,c,f=0,v=[],l=[];if(u==o)for(a=n.r1;a<=n.r2;a++){for(c=0,s=n.g1;s<=n.g2;s++)for(h=n.b1;h<=n.b2;h++)c+=r[e(a,s,h)]||0;v[a]=f+=c;}else if(u==i)for(a=n.g1;a<=n.g2;a++){for(c=0,s=n.r1;s<=n.r2;s++)for(h=n.b1;h<=n.b2;h++)c+=r[e(s,a,h)]||0;v[a]=f+=c;}else for(a=n.b1;a<=n.b2;a++){for(c=0,s=n.r1;s<=n.r2;s++)for(h=n.g1;h<=n.g2;h++)c+=r[e(s,h,a)]||0;v[a]=f+=c;}return v.forEach(function(t,r){l[r]=f-t;}),function(t){var r,o,e,i,u,s=t+"1",h=t+"2",c=0;for(a=n[s];a<=n[h];a++)if(v[a]>f/2){for(e=n.copy(),i=n.copy(),u=(r=a-n[s])<=(o=n[h]-a)?Math.min(n[h]-1,~~(a+o/2)):Math.max(n[s],~~(a-1-r/2));!v[u];)u++;for(c=l[u];!c&&v[u-1];)c=l[--u];return e[h]=u,i[s]=e[h]+1,[e,i]}}(u==o?"r":u==i?"g":"b")}}return u.prototype={volume:function(t){return this._volume&&!t||(this._volume=(this.r2-this.r1+1)*(this.g2-this.g1+1)*(this.b2-this.b1+1)),this._volume},count:function(t){var r=this.histo;if(!this._count_set||t){var n,o,i,u=0;for(n=this.r1;n<=this.r2;n++)for(o=this.g1;o<=this.g2;o++)for(i=this.b1;i<=this.b2;i++)u+=r[e(n,o,i)]||0;this._count=u,this._count_set=!0;}return this._count},copy:function(){return new u(this.r1,this.r2,this.g1,this.g2,this.b1,this.b2,this.histo)},avg:function(t){var n=this.histo;if(!this._avg||t){var o,i,u,a,s=0,h=1<<8-r,c=0,f=0,v=0;for(i=this.r1;i<=this.r2;i++)for(u=this.g1;u<=this.g2;u++)for(a=this.b1;a<=this.b2;a++)s+=o=n[e(i,u,a)]||0,c+=o*(i+.5)*h,f+=o*(u+.5)*h,v+=o*(a+.5)*h;this._avg=s?[~~(c/s),~~(f/s),~~(v/s)]:[~~(h*(this.r1+this.r2+1)/2),~~(h*(this.g1+this.g2+1)/2),~~(h*(this.b1+this.b2+1)/2)];}return this._avg},contains:function(t){var r=t[0]>>n;return gval=t[1]>>n,bval=t[2]>>n,r>=this.r1&&r<=this.r2&&gval>=this.g1&&gval<=this.g2&&bval>=this.b1&&bval<=this.b2}},a.prototype={push:function(t){this.vboxes.push({vbox:t,color:t.avg()});},palette:function(){return this.vboxes.map(function(t){return t.color})},size:function(){return this.vboxes.size()},map:function(t){for(var r=this.vboxes,n=0;n<r.size();n++)if(r.peek(n).vbox.contains(t))return r.peek(n).color;return this.nearest(t)},nearest:function(t){for(var r,n,o,e=this.vboxes,i=0;i<e.size();i++)((n=Math.sqrt(Math.pow(t[0]-e.peek(i).color[0],2)+Math.pow(t[1]-e.peek(i).color[1],2)+Math.pow(t[2]-e.peek(i).color[2],2)))<r||void 0===r)&&(r=n,o=e.peek(i).color);return o},forcebw:function(){var r=this.vboxes;r.sort(function(r,n){return t.naturalOrder(t.sum(r.color),t.sum(n.color))});var n=r[0].color;n[0]<5&&n[1]<5&&n[2]<5&&(r[0].color=[0,0,0]);var o=r.length-1,e=r[o].color;e[0]>251&&e[1]>251&&e[2]>251&&(r[o].color=[255,255,255]);}},{quantize:function(h,c){if(!h.length||c<2||c>256)return !1;var f=function(t){var o,i=new Array(1<<3*r);return t.forEach(function(t){o=e(t[0]>>n,t[1]>>n,t[2]>>n),i[o]=(i[o]||0)+1;}),i}(h);f.forEach(function(){});var v=function(t,r){var o,e,i,a=1e6,s=0,h=1e6,c=0,f=1e6,v=0;return t.forEach(function(t){(o=t[0]>>n)<a?a=o:o>s&&(s=o),(e=t[1]>>n)<h?h=e:e>c&&(c=e),(i=t[2]>>n)<f?f=i:i>v&&(v=i);}),new u(a,s,h,c,f,v,r)}(h,f),l=new i(function(r,n){return t.naturalOrder(r.count(),n.count())});function g(t,r){for(var n,e=t.size(),i=0;i<o;){if(e>=r)return;if(i++>o)return;if((n=t.pop()).count()){var u=s(f,n),a=u[0],h=u[1];if(!a)return;t.push(a),h&&(t.push(h),e++);}else t.push(n),i++;}}l.push(v),g(l,.75*c);for(var p=new i(function(r,n){return t.naturalOrder(r.count()*r.volume(),n.count()*n.volume())});l.size();)p.push(l.pop());g(p,c);for(var b=new a;p.size();)b.push(p.pop());return b}}}().quantize,n=function(t){this.canvas=document.createElement("canvas"),this.context=this.canvas.getContext("2d"),this.width=this.canvas.width=t.width,this.height=this.canvas.height=t.height,this.context.drawImage(t,0,0,this.width,this.height);};n.prototype.getImageData=function(){return this.context.getImageData(0,0,this.width,this.height)};var o=function(){};o.prototype.getColor=function(t,r){return void 0===r&&(r=10),this.getPalette(t,5,r)[0]},o.prototype.getPalette=function(t,o,e){var i=function(t){var r=t.colorCount,n=t.quality;if(void 0!==r&&Number.isInteger(r)){if(1===r)throw new Error("colorCount should be between 2 and 20. To get one color, call getColor() instead of getPalette()");r=Math.max(r,2),r=Math.min(r,20);}else r=10;return void 0===n||Number.isInteger(n)?n=10:n<1&&(n=10),{colorCount:r,quality:n}}({colorCount:o,quality:e}),u=new n(t),a=function(t,r,n){for(var o=t,e=[],i=0,u=void 0,a=void 0,s=void 0,h=void 0,c=void 0;i<r;i+=n)a=o[0+(u=4*i)],s=o[u+1],h=o[u+2],(void 0===(c=o[u+3])||c>=125)&&(a>250&&s>250&&h>250||e.push([a,s,h]));return e}(u.getImageData().data,u.width*u.height,i.quality),s=r(a,i.colorCount);return s?s.palette():null},o.prototype.getColorFromUrl=function(t,r,n){var o=document.createElement("img"),e=this;o.addEventListener("load",function(){var i=e.getPalette(o,5,n);r(i[0],t);}),o.src=t;},o.prototype.getImageData=function(t,r){var n=new XMLHttpRequest;n.open("GET",t,!0),n.responseType="arraybuffer",n.onload=function(){if(200==this.status){var t=new Uint8Array(this.response);o=t.length;for(var n=new Array(o),o=0;o<t.length;o++)n[o]=String.fromCharCode(t[o]);var e=n.join(""),i=window.btoa(e);r("data:image/png;base64,"+i);}},n.send();},o.prototype.getColorAsync=function(t,r,n){var o=this;this.getImageData(t,function(t){var e=document.createElement("img");e.addEventListener("load",function(){var t=o.getPalette(e,5,n);r(t[0],this);}),e.src=t;});};

class NvCardMedia {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * State
         */
        this.objectURL = null;
        this.colorImage = null;
        /**
         * The type from the media, Video or image
         */
        this.type = 'image';
    }
    /**
     * Lifecyles events
     */
    componentWillLoad() {
        this.loadImg();
    }
    componentDidUnload() {
        // Remove file object from memory
        URL.revokeObjectURL(this.objectURL);
    }
    //
    // Methods
    //
    async loadImg() {
        const url = this.src;
        if (url) {
            const image = await fetch(url)
                .then(response => response.blob())
                .then((blob) => URL.createObjectURL(blob));
            this.objectURL = image;
        }
    }
    loadColor() {
        const sourceImage = this.el.querySelector('.nv-card-internal-img');
        const colorThief = new o();
        const color = colorThief.getColor(sourceImage);
        this.colorImage = `rgb(${color.join(', ')})`;
    }
    makeMediaEl() {
        if (this.src && this.type === 'image' && this.objectURL) {
            return h("img", { class: 'nv-card-internal-img', src: this.objectURL, alt: this.alt, title: this.titleImg, onLoad: () => this.loadColor() });
        }
        if (this.src && this.type === 'video') {
            return h("video", { src: this.src, title: this.titleImg });
        }
    }
    render() {
        if (this.src && !this.objectURL) {
            return false;
        }
        let colorBkg;
        const mediaEl = this.makeMediaEl();
        const colorImage = this.colorImage;
        if (this.typeCard === 'compact') {
            colorBkg = h("div", { class: 'nv-card-bkg-color', style: { backgroundColor: colorImage } });
        }
        return (h("div", { class: 'nv-card-media' }, colorBkg, mediaEl, h("slot", null)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-card-media",
        "$members$": {
            "type": [1],
            "typeCard": [1, "type-card"],
            "src": [1],
            "alt": [1],
            "titleImg": [1, "title-img"],
            "objectURL": [32],
            "colorImage": [32]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCardText {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return h("p", { class: 'nv-card-text' }, h("slot", null));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-card-text",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCardTitle {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return h("h5", { class: 'nv-card-title' }, h("slot", null));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-card-title",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCheckbox {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Props
        //
        /**
         * If `true`, the user cannot interecat with the checkbox.
         */
        this.disabled = true;
        //
        // Methods
        //
        this.updateCheck = () => {
            if (this.checked) {
                this.checked = false;
                this.nvDeselected.emit();
            }
            else {
                this.el.checked = true;
                this.checked = true;
            }
        };
        this.nvSelect = createEvent(this, "nvSelect", 7);
        this.nvDeselected = createEvent(this, "nvDeselected", 7);
    }
    //
    // Watch
    //
    handleChecked(isChecked) {
        if (isChecked) {
            this.nvSelect.emit({
                checked: true,
            });
        }
    }
    makeIcon() {
        if (this.indeterminate) {
            return (h("svg", { class: 'nv-checkbox-icon', viewBox: '0 0 30 30' }, h("g", { transform: 'translate(0 -289.06)' }, h("path", { transform: 'translate(0 289.06)', d: 'm4.4668 14c-0.030724 0.32965-0.050781 0.66211-0.050781\n              1s0.020057 0.67035 0.050781 1h21.066c0.030725-0.32965\n              0.050781-0.66211 0.050781-1s-0.020056-0.67035-0.050781-1h-21.066z' }))));
        }
        return (h("svg", { class: 'nv-checkbox-icon', viewBox: '0 0 30 30' }, h("g", { transform: 'translate(0 -283.15)' }, h("path", { transform: 'translate(0 283.15)', d: 'm27 6.0195-15.932 18.988-8.0684-6.7695v2.6094l6.7832\n            5.6914c0.83553 0.70109 2.1173 0.58944 2.8184-0.24609l14.398-17.16v-3.1133z' }))));
    }
    render() {
        const icon = this.makeIcon();
        return (h(Host, { class: {
                'nv-checkbox': true,
                'nv-checkbox-checked': this.checked,
                'nv-checkbox-primary': this.color === 'primary',
                'nv-checkbox-secondary': this.color === 'secondary',
                'nv-checkbox-warning': this.color === 'warning',
                'nv-checkbox-danger': this.color === 'danger',
                'nv-checkbox-success': this.color === 'success',
                'nv-checkbox-indeterminate': this.indeterminate,
            }, onClick: this.updateCheck }, h("div", { class: 'nv-checkbox-container' }, h("input", { ref: el => this.el = el, type: 'checkbox', name: this.name, checked: this.checked }), icon)));
    }
    static get watchers() { return {
        "checked": ["handleChecked"]
    }; }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-checkbox",
        "$members$": {
            "disabled": [4],
            "checked": [1028],
            "indeterminate": [4],
            "name": [1],
            "color": [1],
            "el": [32]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvChip {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * If `true` the chip will have a rouded style
         */
        this.rounded = false;
    }
    //
    // Lifecyles
    //
    componentDidLoad() {
        const chip = this.el.querySelector('.nv-chip');
        inserted(chip);
    }
    render() {
        return (h("div", { class: {
                'nv-chip': true,
                'nv-chip-rounded': this.rounded,
            } }, h("div", { class: 'nv-chip-avatar' }, h("nv-avatar", { src: this.src, text: this.text })), h("span", { class: 'nv-chip-title' }, h("slot", null))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-chip",
        "$members$": {
            "src": [1],
            "text": [1],
            "rounded": [4]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCollapse {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Props
        //
        /**
         * If `true`, the will have a accordion effect.
         */
        this.accordion = false;
    }
    // Lifecycles
    componentDidLoad() {
        const nvCollapse = this.el.querySelector('.nv-collapse');
        if (nvCollapse) {
            this.children = nvCollapse.children;
            if (this.accordion) {
                for (const child of this.children) {
                    const item = child;
                    item.setAttribute('accordion', '');
                }
            }
        }
    }
    //
    // Methods
    //
    /**
     * Call this method for close all Collapse Itens.
     */
    async closeAllItems(el) {
        for (const child of this.children) {
            const item = child;
            if (item !== el) {
                item.close();
            }
        }
    }
    render() {
        return (h("div", { class: 'nv-collapse' }, h("slot", null)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-collapse",
        "$members$": {
            "accordion": [4],
            "closeAllItems": [64]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvCollapseItem {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // State
        //
        this.open = false;
        this.contentScrollHeight = 0;
        this.maxHeight = '0px';
        //
        // Props
        //
        /**
         * This props is used by `nv-collapse` to apply a accordion login.
         */
        this.accordion = false;
        /**
         * If `true`, the user cannot interact with the callapse item.
         */
        this.disabled = false;
    }
    //
    // Lifecycles
    //
    componentWillLoad() {
        this.rootEl = this.el.parentElement.parentElement;
    }
    componentDidLoad() {
        this.contentScrollHeight = this.el.querySelector('.nv-collapse-item-container').scrollHeight;
        const header = this.el.querySelector('header');
        // Apply Ripple effect to header
        inserted(header);
        if (this.open) {
            this.maxHeight = `${this.contentScrollHeight}px`;
        }
    }
    //
    // Events
    //
    handleResize() {
        this.changeHeight();
    }
    //
    // Methods
    //
    changeHeight() {
        const height = this.contentScrollHeight;
        if (this.maxHeight !== '0px') {
            this.maxHeight = `${height}px`;
        }
    }
    /**
     * Call this method for te close this collapse.
     */
    async close() {
        this.maxHeight = '0px';
        this.open = false;
    }
    /**
     * Call this method for the open or close this collapse.
     */
    async toggleContent() {
        if (this.disabled) {
            return;
        }
        if (this.accordion) {
            this.rootEl.closeAllItems(this.el);
        }
        const height = this.contentScrollHeight;
        if (this.maxHeight === '0px') {
            this.maxHeight = `${height}px`;
            this.open = true;
        }
        else {
            this.maxHeight = `0px`;
            this.open = false;
        }
    }
    render() {
        return (h("div", { class: {
                'nv-collapse-item': true,
                disabled: this.disabled,
            } }, h("header", { onClick: () => this.toggleContent() }, h("div", { class: 'header-container' }, h("slot", { name: 'header' }))), h("div", { class: {
                'nv-collapse-item-container': true,
                show: this.open,
            }, style: { maxHeight: this.maxHeight } }, h("slot", { name: 'content' }))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-collapse-item",
        "$members$": {
            "accordion": [4],
            "disabled": [4],
            "open": [32],
            "contentScrollHeight": [32],
            "maxHeight": [32],
            "rootEl": [32],
            "close": [64],
            "toggleContent": [64]
        },
        "$listeners$": [[9, "resize", "handleResize"]],
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvDialog {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Props
        //
        /**
         * If `true`, the dialog is displayed.
         */
        this.opened = false;
        this.show = false;
    }
    //
    // Lifecycles
    //
    componentDidLoad() {
        const dialog = this.el;
        if (dialog) {
            // Add Watch events
            dialog.addEventListener('transitionend', (e) => this.watchEvents(e));
        }
    }
    watchEvents(evt) {
        const dialog = this.el;
        if (evt.type === 'transitionend') {
            // We only process 'opacity' because all states have this
            // change.
            if (evt.propertyName !== 'opacity') {
                return;
            }
            if (dialog.classList.contains('opening')) {
                dialog.classList.add('opened');
                dialog.classList.remove('opening');
            }
            else if (dialog.classList.contains('flying')) {
                dialog.classList.remove('flying');
                this.open();
            }
            // go back to initial state after hiding
            if (dialog.classList.contains('hiding') || dialog.classList.contains('closing')) {
                dialog.classList.remove('opened');
                dialog.classList.remove('opening');
                dialog.classList.remove('closing');
                dialog.classList.remove('flying');
                dialog.classList.remove('hiding');
                dialog.classList.add('closed');
                dialog.style.display = 'none';
            }
        }
    }
    //
    // Utils
    //
    runNextAnimationFrame(callback) {
        requestAnimationFrame(() => {
            setTimeout(callback, 0);
        });
    }
    //
    //  Methods
    //
    /**
     * Close the dialog
     */
    async close() {
        const dialog = this.el;
        dialog.classList.add('closing');
        this.show = false;
    }
    /**
     * Close or open dialog.
     */
    async toggle() {
        if (this.show) {
            this.close();
            return;
        }
        this.open();
    }
    /**
     * Confirm and close the dialog.
     */
    async confirm() {
        this.close();
    }
    async open() {
        const dialog = this.el;
        dialog.style.display = 'flex';
        // Wait a frame once display is no longer "none", to establish basis for animation
        this.runNextAnimationFrame(() => {
            // no operation during flying and hiding
            if (dialog.classList.contains('flying') ||
                dialog.classList.contains('hiding')) {
                return;
            }
            // If we get focus when we closing the group, we need to cancel the closing
            // state.
            dialog.classList.remove('closed');
            dialog.classList.remove('closing');
            dialog.classList.add('opening');
            this.show = true;
        });
    }
    render() {
        return (h("div", { class: {
                'nv-dialog': true,
                closed: !this.opened,
            }, style: { display: !this.opened ? 'none' : null } }, h("div", { class: 'nv-dialog-background', onClick: () => this.close() }), h("div", { class: 'nv-dialog-container nv-dialog-custom' }, h("header", null, h("div", { class: 'nv-dialog-header' }, h("slot", { name: 'header' }))), h("section", { class: 'nv-dialog-body' }, h("slot", { name: 'content' })), h("footer", { class: 'nv-dialog-footer' }, h("slot", { name: 'footer' })))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-dialog",
        "$members$": {
            "opened": [4],
            "close": [64],
            "toggle": [64],
            "confirm": [64],
            "open": [64]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvDialogAction {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Props
        //
        /**
         * The header title for the dialog
         */
        this.headerTitle = false;
        /**
         * The type from the dialog
         */
        this.type = 'confirm';
        /**
         * If `true`, the dialog is displayed.
         */
        this.opened = false;
        this.show = false;
    }
    async close() {
        const dialog = this.el;
        dialog.classList.add('closing');
        this.show = false;
    }
    async toggle() {
        if (this.show) {
            this.close();
            return;
        }
        this.open();
    }
    async confirm() {
        this.close();
    }
    async open() {
        const dialog = this.el;
        dialog.style.display = 'flex';
        // Wait a frame once display is no longer "none", to establish basis for animation
        this.runNextAnimationFrame(() => {
            // no operation during flying and hiding
            if (dialog.classList.contains('flying') ||
                dialog.classList.contains('hiding')) {
                return;
            }
            // If we get focus when we closing the group, we need to cancel the closing
            // state.
            dialog.classList.remove('closed');
            dialog.classList.remove('closing');
            dialog.classList.add('opening');
            this.show = true;
        });
    }
    //
    // Lifecycles
    //
    componentDidLoad() {
        const dialog = this.el;
        if (dialog) {
            // Add Watch events
            dialog.addEventListener('transitionend', (e) => this.watchEvents(e));
        }
    }
    watchEvents(evt) {
        const dialog = this.el;
        if (evt.type === 'transitionend') {
            // We only process 'opacity' because all states have this
            // change.
            if (evt.propertyName !== 'opacity') {
                return;
            }
            if (dialog.classList.contains('opening')) {
                dialog.classList.add('opened');
                dialog.classList.remove('opening');
            }
            else if (dialog.classList.contains('flying')) {
                dialog.classList.remove('flying');
                this.open();
            }
            // go back to initial state after hiding
            if (dialog.classList.contains('hiding') || dialog.classList.contains('closing')) {
                dialog.classList.remove('opened');
                dialog.classList.remove('opening');
                dialog.classList.remove('closing');
                dialog.classList.remove('flying');
                dialog.classList.remove('hiding');
                dialog.classList.add('closed');
                dialog.style.display = 'none';
            }
        }
    }
    //
    // Utils
    //
    runNextAnimationFrame(callback) {
        requestAnimationFrame(() => {
            setTimeout(callback, 0);
        });
    }
    makeFooter() {
        return (h("nv-button", { text: true, "disabled-ripple": true, onClick: () => this.close() }, h("span", null, this.textCancel)));
    }
    render() {
        const footer = this.makeFooter();
        return (h(Host, { class: {
                'nv-dialog': true,
                closed: !this.opened,
            }, style: { display: !this.opened ? 'none' : null } }, h("div", { class: 'nv-dialog-background', onClick: () => this.close() }), h("div", { class: 'nv-dialog-container nv-dialog-action' }, h("section", { class: 'nv-dialog-body' }, h("slot", { name: 'content' })), h("div", { class: 'nv-dialog-action-container' }, h("slot", { name: 'action' })), h("footer", { class: 'nv-dialog-footer' }, footer))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-dialog-action",
        "$members$": {
            "headerTitle": [4, "header-title"],
            "type": [1],
            "opened": [4],
            "textConfirm": [1, "text-confirm"],
            "textCancel": [1, "text-cancel"],
            "color": [1],
            "close": [64],
            "toggle": [64],
            "confirm": [64],
            "open": [64]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvDialogAlert {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Props
        //
        /**
         * The header title for the dialog
         */
        this.headerTitle = false;
        /**
         * If `true`, the dialog is displayed.
         */
        this.opened = false;
        this.show = false;
    }
    //
    // Lifecycles
    //
    componentDidLoad() {
        const dialog = this.el;
        if (dialog) {
            // Add Watch events
            dialog.addEventListener('transitionend', (e) => this.watchEvents(e));
        }
    }
    //
    // Methods
    //
    async close() {
        const dialog = this.el;
        dialog.classList.add('closing');
        this.show = false;
    }
    async toggle() {
        if (this.show) {
            this.close();
            return;
        }
        this.open();
    }
    async confirm() {
        this.close();
    }
    async open() {
        const dialog = this.el;
        dialog.style.display = 'flex';
        // Wait a frame once display is no longer "none", to establish basis for animation
        this.runNextAnimationFrame(() => {
            // no operation during flying and hiding
            if (dialog.classList.contains('flying') ||
                dialog.classList.contains('hiding')) {
                return;
            }
            // If we get focus when we closing the group, we need to cancel the closing
            // state.
            dialog.classList.remove('closed');
            dialog.classList.remove('closing');
            dialog.classList.add('opening');
            this.show = true;
        });
    }
    watchEvents(evt) {
        const dialog = this.el;
        if (evt.type === 'transitionend') {
            // We only process 'opacity' because all states have this
            // change.
            if (evt.propertyName !== 'opacity') {
                return;
            }
            if (dialog.classList.contains('opening')) {
                dialog.classList.add('opened');
                dialog.classList.remove('opening');
            }
            else if (dialog.classList.contains('flying')) {
                dialog.classList.remove('flying');
                this.open();
            }
            // go back to initial state after hiding
            if (dialog.classList.contains('hiding') || dialog.classList.contains('closing')) {
                dialog.classList.remove('opened');
                dialog.classList.remove('opening');
                dialog.classList.remove('closing');
                dialog.classList.remove('flying');
                dialog.classList.remove('hiding');
                dialog.classList.add('closed');
                dialog.style.display = 'none';
            }
        }
    }
    //
    // Utils
    //
    runNextAnimationFrame(callback) {
        requestAnimationFrame(() => {
            setTimeout(callback, 0);
        });
    }
    makeFooter() {
        return (h("nv-button", { text: true, color: this.color, onClick: () => this.confirm() }, h("span", null, this.textConfirm)));
    }
    render() {
        const footer = this.makeFooter();
        return (h(Host, { class: {
                'nv-dialog': true,
                closed: !this.opened,
            }, style: { display: !this.opened ? 'none' : null } }, h("div", { class: 'nv-dialog-background', onClick: () => this.close() }), h("div", { class: 'nv-dialog-container' }, h("section", { class: 'nv-dialog-body' }, h("slot", null)), h("footer", { class: 'nv-dialog-footer' }, footer))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-dialog-alert",
        "$members$": {
            "headerTitle": [4, "header-title"],
            "opened": [4],
            "textConfirm": [1, "text-confirm"],
            "color": [1],
            "close": [64],
            "toggle": [64],
            "confirm": [64],
            "open": [64]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

var Fragment = (props, children) => [ ...children ];

class NvDialogConfirm {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Props
        //
        /**
         * The header title for the dialog.
         */
        this.headerTitle = false;
        /**
         * If `true`, the dialog is displayed.
         */
        this.opened = false;
        this.show = false;
    }
    //
    // Lifecycles
    //
    componentDidLoad() {
        const dialog = this.el;
        if (dialog) {
            // Add Watch events
            dialog.addEventListener('transitionend', (e) => this.watchEvents(e));
        }
    }
    //
    // Methods
    //
    async close() {
        const dialog = this.el;
        dialog.classList.add('closing');
        this.show = false;
    }
    async toggle() {
        if (this.show) {
            this.close();
            return;
        }
        this.open();
    }
    async confirm() {
        this.close();
    }
    async open() {
        const dialog = this.el;
        dialog.style.display = 'flex';
        // Wait a frame once display is no longer "none", to establish basis for animation
        this.runNextAnimationFrame(() => {
            // no operation during flying and hiding
            if (dialog.classList.contains('flying') ||
                dialog.classList.contains('hiding')) {
                return;
            }
            // If we get focus when we closing the group, we need to cancel the closing
            // state.
            dialog.classList.remove('closed');
            dialog.classList.remove('closing');
            dialog.classList.add('opening');
            this.show = true;
        });
    }
    watchEvents(evt) {
        const dialog = this.el;
        if (evt.type === 'transitionend') {
            // We only process 'opacity' because all states have this
            // change.
            if (evt.propertyName !== 'opacity') {
                return;
            }
            if (dialog.classList.contains('opening')) {
                dialog.classList.add('opened');
                dialog.classList.remove('opening');
            }
            else if (dialog.classList.contains('flying')) {
                dialog.classList.remove('flying');
                this.open();
            }
            // go back to initial state after hiding
            if (dialog.classList.contains('hiding') || dialog.classList.contains('closing')) {
                dialog.classList.remove('opened');
                dialog.classList.remove('opening');
                dialog.classList.remove('closing');
                dialog.classList.remove('flying');
                dialog.classList.remove('hiding');
                dialog.classList.add('closed');
                dialog.style.display = 'none';
            }
        }
    }
    //
    // Utils
    //
    runNextAnimationFrame(callback) {
        requestAnimationFrame(() => {
            setTimeout(callback, 0);
        });
    }
    makeFooter() {
        return (h(Fragment, null, h("nv-button", { text: true, "disabled-ripple": true, onClick: () => this.close() }, h("span", null, this.textCancel)), h("nv-button", { text: true, "disabled-ripple": true, color: this.color, onClick: () => this.confirm() }, h("span", null, this.textConfirm)), h("div", { class: 'nv-dialog-footer-divider' })));
    }
    render() {
        const footer = this.makeFooter();
        return (h(Host, { class: {
                'nv-dialog': true,
                closed: !this.opened,
            }, style: { display: !this.opened ? 'none' : null } }, h("div", { class: 'nv-dialog-background', onClick: () => this.close() }), h("div", { class: 'nv-dialog-container' }, h("section", { class: 'nv-dialog-body' }, h("slot", null)), h("footer", { class: 'nv-dialog-footer' }, footer))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-dialog-confirm",
        "$members$": {
            "headerTitle": [4, "header-title"],
            "opened": [4],
            "textConfirm": [1, "text-confirm"],
            "textCancel": [1, "text-cancel"],
            "color": [1],
            "close": [64],
            "toggle": [64],
            "confirm": [64],
            "open": [64]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvDialogPrompt {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Props
        //
        /**
         * The header title for the dialog
         */
        this.headerTitle = false;
        /**
         * If `true`, the dialog is displayed.
         */
        this.opened = false;
        /**
         * If `true`, the text inside on text field is selected after open dialog.
         */
        this.autoSelect = true;
        this.show = false;
    }
    async close() {
        const dialog = this.el;
        dialog.classList.add('closing');
        this.show = false;
    }
    async toggle() {
        if (this.show) {
            this.close();
            return;
        }
        this.open();
    }
    async confirm() {
        this.close();
    }
    async open() {
        const dialog = this.el;
        dialog.style.display = 'flex';
        // Wait a frame once display is no longer "none", to establish basis for animation
        this.runNextAnimationFrame(() => {
            // no operation during flying and hiding
            if (dialog.classList.contains('flying') ||
                dialog.classList.contains('hiding')) {
                return;
            }
            // If we get focus when we closing the group, we need to cancel the closing
            // state.
            dialog.classList.remove('closed');
            dialog.classList.remove('closing');
            dialog.classList.add('opening');
            this.show = true;
        });
    }
    //
    // Lifecycles
    //
    componentDidLoad() {
        const dialog = this.el;
        if (dialog) {
            // Add Watch events
            dialog.addEventListener('transitionend', (e) => this.watchEvents(e));
        }
    }
    watchEvents(evt) {
        const dialog = this.el;
        if (evt.type === 'transitionend') {
            // We only process 'opacity' because all states have this
            // change.
            if (evt.propertyName !== 'opacity') {
                return;
            }
            if (dialog.classList.contains('opening')) {
                dialog.classList.add('opened');
                dialog.classList.remove('opening');
                if (this.textFieldValue || this.promptValue) {
                    this.textField.selectText();
                }
            }
            else if (dialog.classList.contains('flying')) {
                dialog.classList.remove('flying');
                this.open();
            }
            // go back to initial state after hiding
            if (dialog.classList.contains('hiding') || dialog.classList.contains('closing')) {
                dialog.classList.remove('opened');
                dialog.classList.remove('opening');
                dialog.classList.remove('closing');
                dialog.classList.remove('flying');
                dialog.classList.remove('hiding');
                dialog.classList.add('closed');
                dialog.style.display = 'none';
            }
        }
    }
    //
    // Utils
    //
    runNextAnimationFrame(callback) {
        requestAnimationFrame(() => {
            setTimeout(callback, 0);
        });
    }
    makeFooter() {
        return (h(Fragment, null, h("nv-button", { text: true, "disabled-ripple": true, onClick: () => this.close() }, h("span", null, this.textCancel)), h("nv-button", { text: true, "disabled-ripple": true, color: this.color, onClick: () => this.confirm() }, h("span", null, this.textConfirm)), h("div", { class: 'nv-dialog-footer-divider' })));
    }
    render() {
        const footer = this.makeFooter();
        return (h(Host, { class: {
                'nv-dialog': true,
                closed: !this.opened,
            }, style: { display: !this.opened ? 'none' : null } }, h("div", { class: 'nv-dialog-background', onClick: () => this.close() }), h("div", { class: 'nv-dialog-container' }, h("section", { class: 'nv-dialog-body' }, h("nv-text-field", { ref: el => this.textField = el, label: this.label, onInput: node => this.promptValue = node.target.value, value: this.textFieldValue })), h("footer", { class: 'nv-dialog-footer' }, footer))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-dialog-prompt",
        "$members$": {
            "headerTitle": [4, "header-title"],
            "opened": [4],
            "textConfirm": [1, "text-confirm"],
            "textCancel": [1, "text-cancel"],
            "color": [1],
            "label": [1],
            "textFieldValue": [1, "text-field-value"],
            "autoSelect": [4, "auto-select"],
            "close": [64],
            "toggle": [64],
            "confirm": [64],
            "open": [64]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvDialogSelect {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * If `true`, the dialog is displayed.
         */
        this.opened = false;
        /**
         * If `true` the list accept multiple selected elements.
         */
        this.multiple = false;
        this.show = false;
        this.selectedItem = createEvent(this, "selectedItem", 6);
    }
    //
    // Lifecycles
    //
    componentDidLoad() {
        const dialog = this.el;
        const list = this.list;
        const listItems = list.querySelectorAll('li');
        if (dialog) {
            // Add Watch events
            dialog.addEventListener('transitionend', e => this.watchEvents(e));
            list.addEventListener('click', e => this.listClick(e));
        }
        if (listItems) {
            listItems.forEach((item) => {
                inserted(item);
            });
        }
    }
    //
    // Methods
    //
    async close() {
        const dialog = this.el;
        dialog.classList.add('closing');
        this.show = false;
    }
    async toggle() {
        if (this.show) {
            this.close();
            return;
        }
        this.open();
    }
    async confirm() {
        this.close();
    }
    async open() {
        const dialog = this.el;
        dialog.style.display = 'flex';
        // Wait a frame once display is no longer "none", to establish basis for animation
        this.runNextAnimationFrame(() => {
            // no operation during flying and hiding
            if (dialog.classList.contains('flying') ||
                dialog.classList.contains('hiding')) {
                return;
            }
            // If we get focus when we closing the group, we need to cancel the closing
            // state.
            dialog.classList.remove('closed');
            dialog.classList.remove('closing');
            dialog.classList.add('opening');
            this.show = true;
        });
    }
    //
    // Private methods
    //
    listClick(e) {
        const el = e.target;
        const selected = el.getAttribute('aria-selected') === 'true';
        if (this.multiple) {
            this.onChangeMultiple(el, selected);
            return;
        }
        this.onChange(el, selected);
    }
    onChange(el, selected) {
        this.clearSelected();
        if (!selected) {
            el.setAttribute('aria-selected', (!selected).toString());
        }
        this.fireChange();
    }
    onChangeMultiple(el, selected) {
        el.setAttribute('aria-selected', (!selected).toString());
        this.fireChange();
    }
    clearSelected() {
        [].forEach.call(this.getSelectedOptions(), (option) => {
            option.removeAttribute('aria-selected');
        });
    }
    fireChange() {
        this.selectedItem.emit({ value: this.getValueString() });
    }
    makeHeader() {
        if (this.headerTitle) {
            return (h("header", null, h("div", { class: 'nv-dialog-header' }, h("h3", null, this.headerTitle))));
        }
    }
    watchEvents(evt) {
        const dialog = this.el;
        if (evt.type === 'transitionend') {
            // We only process 'opacity' because all states have this
            // change.
            if (evt.propertyName !== 'opacity') {
                return;
            }
            if (dialog.classList.contains('opening')) {
                dialog.classList.add('opened');
                dialog.classList.remove('opening');
            }
            else if (dialog.classList.contains('flying')) {
                dialog.classList.remove('flying');
                this.open();
            }
            // go back to initial state after hiding
            if (dialog.classList.contains('hiding') || dialog.classList.contains('closing')) {
                dialog.classList.remove('opened');
                dialog.classList.remove('opening');
                dialog.classList.remove('closing');
                dialog.classList.remove('flying');
                dialog.classList.remove('hiding');
                dialog.classList.add('closed');
                dialog.style.display = 'none';
            }
        }
    }
    //
    // Getters
    //
    getSelectedOptions() {
        return this.el.querySelectorAll('li[aria-selected="true"]');
    }
    getSelected() {
        return this.getSelectedOptions()[0];
    }
    getValueString() {
        const selected = this.getSelected();
        return selected && selected.textContent;
    }
    //
    // Utils
    //
    runNextAnimationFrame(callback) {
        requestAnimationFrame(() => {
            setTimeout(callback, 0);
        });
    }
    makeFooter() {
        return (h(Fragment, null, h("nv-button", { text: true, "disabled-ripple": true, onClick: () => this.close() }, h("span", null, this.textCancel)), h("nv-button", { text: true, "disabled-ripple": true, color: this.color, onClick: () => this.confirm() }, h("span", null, this.textConfirm)), h("div", { class: 'nv-dialog-footer-divider' })));
    }
    render() {
        const header = this.makeHeader();
        const footer = this.makeFooter();
        return (h(Host, { class: {
                'nv-dialog': true,
                closed: !this.opened,
            }, style: { display: !this.opened ? 'none' : null } }, h("div", { class: 'nv-dialog-background', onClick: () => this.close() }), h("div", { class: 'nv-dialog-container nv-dialog-select' }, header, h("section", { class: 'nv-dialog-body no-padding' }, h("ul", { class: 'nv-list', ref: el => this.list = el }, h("slot", null))), h("footer", { class: 'nv-dialog-footer' }, footer))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-dialog-select",
        "$members$": {
            "headerTitle": [1, "header-title"],
            "textCancel": [1, "text-cancel"],
            "color": [1],
            "opened": [4],
            "multiple": [4],
            "textConfirm": [1, "text-confirm"],
            "close": [64],
            "toggle": [64],
            "confirm": [64],
            "open": [64]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvLabel {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * If `true`, the label will have disabled.
         */
        this.disabled = false;
        /**
         * If `true`, the label will dislay a `*`.
         */
        this.required = false;
        this.disabled = false;
    }
    render() {
        return (h("label", { htmlFor: this.htmlFor, class: {
                'nv-label': true,
                disabled: this.disabled,
                required: this.required,
            } }, h("slot", null)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-label",
        "$members$": {
            "htmlFor": [1, "html-for"],
            "disabled": [4],
            "required": [4]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvList {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * If `true`, the liste will have a **user** style.
         */
        this.users = false;
        /**
         * If `true`, the liste will have a **email** style.
         */
        this.email = false;
        /**
         * If `true`, the header will have a sicky effect.
         */
        this.stickyHeader = false;
        this.nvDidUpdate = createEvent(this, "nvDidUpdate", 7);
    }
    componentDidLoad() {
        if (this.stickyHeader) {
            const headers = this.container.querySelectorAll('.nv-list-header');
            this.headerList = headers;
        }
    }
    componentDidUpdate() {
        this.nvDidUpdate.emit();
    }
    render() {
        return (h("div", { ref: el => this.nvList = el, class: {
                'nv-list': true,
                'nv-list-users-type': this.users,
                'nv-list-email-type': this.email,
                'nv-list-sticky-header': this.stickyHeader,
            } }, h("div", { class: 'nv-list-container', ref: el => this.container = el }, h("slot", null))));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-list",
        "$members$": {
            "users": [4],
            "email": [4],
            "stickyHeader": [4, "sticky-header"]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvListGroup {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    componentDidLoad() {
        const lastChild = this.el.querySelector('.nv-list-group > :last-child > div');
        if (lastChild) {
            lastChild.classList.add('last-child');
        }
    }
    render() {
        return (h("div", { class: 'nv-list-group' }, h("slot", null)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-list-group",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvListHeader {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h("div", { class: 'nv-list-header' }, h("nv-sub-header", null, h("slot", null))));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-list-header",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvListItem {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * If `true`, a small rounded element, will have displayed on left of the list
         * Used by **email** list.
         */
        this.unread = false;
    }
    componentDidLoad() {
        inserted(this.nvList);
    }
    makeTitles() {
        if (this.textTitle || this.textSubtitle) {
            return (h(Fragment, null, h("slot", { name: 'icon' }), h("div", { class: 'nv-list-title-area' }, h("div", { class: 'nv-list-title' }, this.textTitle), h("div", { class: 'nv-list-sub-title' }, this.textSubtitle))));
        }
        if (this.firstName || this.lastName) {
            return (h(Fragment, null, h("span", { class: 'nv-list-first-name' }, this.firstName), h("span", { class: 'nv-list-last-name' }, " ", this.lastName)));
        }
    }
    makeCompany() {
        if (this.company) {
            return h("span", { class: 'nv-list-company' }, this.company);
        }
    }
    makeSubject() {
        if (this.textSubject) {
            return h("small", { class: 'nv-list-subject' }, this.textSubject);
        }
    }
    makeMessage() {
        if (this.textMessage) {
            return h("small", { class: 'nv-list-message' }, this.textMessage);
        }
    }
    makeTime() {
        if (this.textStamp) {
            return h("time", null, this.textStamp);
        }
    }
    makeAvatar() {
        if (!this.textStamp) {
            return h("slot", { name: 'avatar' });
        }
    }
    makeUnread() {
        if (this.unread) {
            return h("div", { class: 'unread-email-badge' });
        }
    }
    makeSlot() {
        if (!this.firstName && !this.lastName && !this.textStamp && !this.unread) {
            return h("slot", null);
        }
    }
    render() {
        const title = this.makeTitles();
        const company = this.makeCompany();
        const subject = this.makeSubject();
        const textMessage = this.makeMessage();
        const time = this.makeTime();
        const avatar = this.makeAvatar();
        const unread = this.makeUnread();
        const slot = this.makeSlot();
        return (h("div", { class: {
                'nv-list-item': true,
                'nv-list-email-unread': this.unread,
            }, ref: el => this.nvList = el }, unread, h("div", { class: 'nv-list-item-container' }, h("header", { class: 'nv-list-title-container' }, title, company, subject, textMessage), time, avatar, slot)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-list-item",
        "$members$": {
            "textTitle": [1, "text-title"],
            "textSubtitle": [1, "text-subtitle"],
            "firstName": [1, "first-name"],
            "lastName": [1, "last-name"],
            "company": [1],
            "textSubject": [1, "text-subject"],
            "textMessage": [1, "text-message"],
            "textStamp": [1, "text-stamp"],
            "unread": [4]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvLoader {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * The color of the loader.
         */
        this.color = 'primary';
        /**
         * The size of the loader.
         */
        this.size = 'medium';
        /**
         * If `true`, the snipper's animarion will be paused.
         */
        this.paused = false;
    }
    render() {
        return (h(Host, { class: {
                'nv-loader': true,
                'nv-loader-primary': this.color === 'primary' ? true : false,
                'nv-loader-secondary': this.color === 'secondary' ? true : false,
                'nv-loader-warning': this.color === 'warning' ? true : false,
                'nv-loader-danger': this.color === 'danger' ? true : false,
                'nv-loader-success': this.color === 'success' ? true : false,
                'nv-loader-anim-paused': this.paused,
                'nv-loader-small': this.size === 'small' ? true : false,
                'nv-loader-medium': this.size === 'medium' ? true : false,
                'nv-loader-large': this.size === 'large' ? true : false,
                'nv-loader-big': this.size === 'big' ? true : false,
            } }));
    }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-loader",
        "$members$": {
            "color": [1],
            "size": [1],
            "paused": [4]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvNavigationItem {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Props
        //
        /**
         * If `true`, the navigation items is selected.
         */
        this.active = false;
        this.onClick = () => {
            this.active = true;
            this.nvNavViewItemDidChange.emit({ item: this.el });
        };
        this.nvNavViewItemDidChange = createEvent(this, "nvNavViewItemDidChange", 7);
    }
    //
    // Methods
    //
    /**
     * Set the NavigationItem active.
     */
    async setActive() {
        this.active = true;
        this.nvNavViewItemDidChange.emit({ item: this.el });
    }
    /**
     * Remove the NavigationItem active.
     */
    async removeActive() {
        this.active = false;
    }
    render() {
        return (h(Host, { class: {
                'nv-navigation-view-item': true,
                'nv-navigation-view-item-active': this.active,
            }, onClick: this.onClick }, h("slot", null)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-navigation-view-item",
        "$members$": {
            "active": [1028],
            "setActive": [64],
            "removeActive": [64]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvNavigationItemHeader {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h(Host, { class: 'nv-nav-item-header' }, h("header", null, h("h6", { class: 'nv-nav-title' }, h("slot", null)))));
    }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-navigation-view-header",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvNavigationItems {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // States
        //
        this.open = false;
        this.contentScrollHeight = 0;
        this.maxHeight = '0px';
        this.items = [];
        this.nvNavViewItemsDidChange = createEvent(this, "nvNavViewItemsDidChange", 7);
    }
    //
    // Lifecycles
    //
    componentDidLoad() {
        this.items = Array.from(this.el.querySelectorAll('nv-navigation-view-item'));
        this.initSelect();
    }
    //
    // Events
    //
    nvNavViewItemDidChangeHandler(event) {
        this.nvNavViewItemsDidChange.emit({
            item: event.detail.item,
            items: this.el,
        });
        this.items.forEach((item) => {
            if (event.detail.item !== item) {
                item.removeActive();
            }
        });
    }
    nvNavViewItemsActiveHandler(event) {
        if (event.detail.items !== this.el) {
            this.items.forEach((item) => {
                item.removeActive();
            });
        }
    }
    //
    // Methods
    //
    async initSelect() {
        // wait for all items to be ready
        await Promise.all(this.items.map(tab => tab.componentOnReady()));
    }
    makeHeader() {
        if (this.textHeader) {
            return (h("nv-navigation-view-header", null, this.textHeader));
        }
    }
    render() {
        const header = this.makeHeader();
        return (h(Host, { class: {
                'nv-nav-items': true,
            } }, header, h("section", null, h("slot", null))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-navigation-view-items",
        "$members$": {
            "textHeader": [1, "text-header"],
            "open": [32],
            "contentScrollHeight": [32],
            "maxHeight": [32],
            "items": [32]
        },
        "$listeners$": [[0, "nvNavViewItemDidChange", "nvNavViewItemDidChangeHandler"], [0, "nvNavViewItemsActive", "nvNavViewItemsActiveHandler"]],
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvNavigationView {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // State
        //
        this.navItems = [];
        this.responsive = false;
        this.mnMbActive = false;
        this.heightMnMb = 0;
        this.hostHeight = 0;
        /**
         * If `true`, the Navigation View will have a style like topbar.
         */
        this.mode = 'left';
        this.resize = () => {
            const winSize = window.innerWidth;
            const menuMb = this.el.querySelector('.nv-navigation-view-mb-menu');
            if (winSize < this.initialWidth && !this.responsive) {
                this.responsive = true;
                this.items.forEach((item) => {
                    if (item.tagName !== 'NV-NAVIGATION-VIEW-HEADER') {
                        menuMb.appendChild(item);
                    }
                    this.getMnMbScrollHeight();
                });
                return;
            }
            if (winSize > this.initialWidth && this.responsive) {
                const leftContent = this.el.querySelector('.nv-nav-view-left-content');
                this.items.forEach((item) => {
                    if (item.tagName !== 'NV-NAVIGATION-VIEW-HEADER') {
                        leftContent.appendChild(item);
                    }
                });
                this.responsive = false;
                return;
            }
            this.getMnMbScrollHeight();
        };
        this.openMnMb = () => {
            if (this.heightMnMb <= 0) {
                this.getMnMbScrollHeight().then(() => {
                    this.openMnMb();
                });
                return;
            }
            if (this.mnMbActive) {
                this.mnMbActive = false;
                return;
            }
            this.mnMbActive = true;
        };
        this.nvNavViewViewWillLoad = createEvent(this, "nvNavViewViewWillLoad", 7);
        this.nvNavViewItemsActive = createEvent(this, "nvNavViewItemsActive", 7);
    }
    //
    // Lifecycles
    //
    componentDidLoad() {
        const nvNames = [
            'nv-navigation-view-header',
            'nv-navigation-view-items',
            'nv-navigation-view-item',
            'nv-navigation-view-separator',
        ].join(', ');
        this.navItems = Array.from(this.el.querySelectorAll(nvNames));
        this.nvNavHeeader = this.el.querySelector('nv-navigation-view-header');
        this.initSelect().then(() => {
            this.nvNavViewViewWillLoad.emit();
            this.getDimensions().then(() => {
                this.resize();
                this.getMnMbScrollHeight();
            });
        });
        this.addEvents();
        if (this.position === 'fixed' && this.mode === 'top') {
            document.querySelector('body').classList.add('nv-navigation-view-fixed-body');
        }
    }
    nvNavViewItemsDidChangeHandler(event) {
        this.nvNavViewItemsActive.emit({
            items: event.detail.items,
            item: event.detail.item,
        });
    }
    async initSelect() {
        // wait for all navItems to be ready.
        await Promise.all(this.navItems.map(item => item.componentOnReady()));
    }
    makeRightContainer() {
        if (this.mode === 'top') {
            return (h("div", { class: 'nv-nav-view-right-content' }, h("slot", { name: 'search' }), h("slot", { name: 'footer' })));
        }
    }
    addEvents() {
        window.addEventListener('resize', this.resize);
    }
    async getDimensions() {
        // Elements
        const leftContent = this.el.querySelector('.nv-nav-view-left-content');
        const rightContent = this.el.querySelector('.nv-nav-view-right-content');
        const menuMb = this.el.querySelector('.nv-navigation-view-mb-menu');
        const styleHost = window.getComputedStyle(this.el);
        // Calc dimensions
        const widthLeftContent = leftContent.offsetWidth || 0;
        const widthRightContent = rightContent.offsetWidth || 0;
        const paddingLeft = parseInt(styleHost.paddingLeft, 10);
        const paddingRight = parseInt(styleHost.paddingRight, 10);
        // Apply dimensions
        this.items = this.el.querySelectorAll('.nv-nav-view-left-content > *');
        this.initialWidth = widthLeftContent + widthRightContent + paddingLeft + paddingRight;
        this.heightMnMb = menuMb.scrollHeight;
        this.hostHeight = this.el.offsetHeight;
    }
    async getMnMbScrollHeight() {
        console.log('resize...');
        if (this.position === 'fixed') {
            const winHeight = window.innerHeight || 0;
            this.heightMnMb = winHeight - this.hostHeight;
            return;
        }
        const menuMb = this.el.querySelector('.nv-navigation-view-mb-menu');
        await Promise.all(this.navItems.map(item => item.componentOnReady()));
        this.heightMnMb = menuMb.scrollHeight;
    }
    render() {
        const rightContainer = this.makeRightContainer();
        return (h(Host, { class: {
                'nv-navigation-view': true,
                'nv-navigation-view-left': this.mode === 'left',
                'nv-navigation-view-top': this.mode === 'top',
                'nv-navigation-view-top-responsive': this.mode === 'top' && this.responsive,
                'nv-navigation-view-top-fixed': this.mode === 'top' && this.position === 'fixed',
            } }, h("div", { class: 'nv-navigation-view-menu', onClick: this.openMnMb }, h("svg", { width: '30', height: '30', viewBox: '0 0 30 30' }, h("g", { transform: 'translate(0 -289.06)' }, h("path", { transform: 'translate(0 289.06)', d: 'm3 6v2h24v-2h-24zm0 8v2h24v-2h-24zm0 8v2h24v-2h-24z' })))), h("div", { class: 'nv-nav-view-left-content' }, h("slot", null)), rightContainer, h("div", { class: {
                'nv-navigation-view-mb-menu': true,
                'nv-nav-view-mnmb-active': this.mnMbActive,
                'nv-nav-view-mnmb-hidden': !this.mnMbActive,
            }, style: { height: `${this.heightMnMb}px` } })));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-navigation-view",
        "$members$": {
            "color": [1],
            "mode": [1],
            "position": [1],
            "navItems": [32],
            "nvNavHeeader": [32],
            "initialWidth": [32],
            "responsive": [32],
            "items": [32],
            "mnMbActive": [32],
            "heightMnMb": [32],
            "hostHeight": [32]
        },
        "$listeners$": [[0, "nvNavViewItemsDidChange", "nvNavViewItemsDidChangeHandler"]],
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvNavigationViewSeparator {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h(Host, { class: 'nv-navigation-view-separator' }));
    }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-navigation-view-separator",
        "$members$": undefined,
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvProgress {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // State
        //
        /**
         * The progress for the circle progressbar
         */
        this.dashoffset = 339.292;
        /**
         * The progress bar color.
         */
        this.color = 'primary';
    }
    handleValue(value) {
        if (value !== undefined) {
            this.calcProgress();
        }
    }
    //
    // Lifecycles
    //
    componentWillLoad() {
        this.calcProgress();
    }
    //
    // Methods
    //
    /**
     * @param newProgress, the new value of the progress bar.
     * Call this method to apply a new value.
     */
    async progress(newProgress) {
        if (newProgress) {
            this.value = newProgress;
        }
    }
    calcProgress() {
        const radius = 54;
        const circumference = 2 * Math.PI * radius;
        const progress = this.value / 100;
        let dashoffset = circumference * (1 - progress);
        if (this.semiCircle) {
            dashoffset = circumference - (169 - (dashoffset / 2));
        }
        if (this.square) {
            dashoffset = 400 * (1 - progress);
        }
        this.dashoffset = dashoffset;
    }
    makeProgressBar() {
        if (this.circle) {
            return (h("div", { class: {
                    'nv-progressbar': true,
                    'nv-progressbar-primary': this.color === 'primary',
                    'nv-progressbar-secondary': this.color === 'secondary',
                    'nv-progressbar-warning': this.color === 'warning',
                    'nv-progressbar-danger': this.color === 'danger',
                    'nv-progressbar-success': this.color === 'success',
                } }, h("svg", { width: '100%', height: '100%', viewBox: '0 0 120 120' }, h("circle", { cx: '60', cy: '60', r: '54', fill: 'none', stroke: '#e6e6e6', "stroke-width": '12' }), h("circle", { class: 'circle-progressbar-svg', cx: '60', cy: '60', r: '54', fill: 'none', "stroke-width": '12', "stroke-dasharray": '339.292', "stroke-dashoffset": this.dashoffset })), this.makeLabel()));
        }
        if (this.semiCircle) {
            return (h("div", { class: {
                    'nv-progressbar': true,
                    'nv-progressbar-primary': this.color === 'primary',
                    'nv-progressbar-secondary': this.color === 'secondary',
                    'nv-progressbar-warning': this.color === 'warning',
                    'nv-progressbar-danger': this.color === 'danger',
                    'nv-progressbar-success': this.color === 'success',
                } }, h("svg", { width: '100%', height: '100%', viewBox: '0 0 120 120' }, h("circle", { cx: '60', cy: '60', r: '54', fill: 'none', stroke: '#e6e6e6', "stroke-width": '12', "stroke-dasharray": '339.292', "stroke-dashoffset": '169' }), h("circle", { class: 'circle-progressbar-svg', cx: '60', cy: '60', r: '54', fill: 'none', "stroke-width": '12', "stroke-dasharray": '339.292', "stroke-dashoffset": this.dashoffset })), this.makeLabel()));
        }
        if (this.square) {
            return (h("div", { class: {
                    'nv-progressbar': true,
                    'nv-progressbar-primary': this.color === 'primary',
                    'nv-progressbar-secondary': this.color === 'secondary',
                    'nv-progressbar-warning': this.color === 'warning',
                    'nv-progressbar-danger': this.color === 'danger',
                    'nv-progressbar-success': this.color === 'success',
                } }, h("svg", { width: '100%', height: '100%' }, h("g", { transform: 'translate(0,-270.54164)' }, h("path", { class: 'square-progressbar-svg', fill: 'none', "stroke-width": '12', "stroke-dasharray": '400', "stroke-dashoffset": this.dashoffset, d: 'm 49.999967,270.6664 49.875345,-7e-5 v 99.75063 H 0.12468834 V 270.6664 Z' }))), this.makeLabel()));
        }
        return (h("div", { role: 'progressbar', "aria-valuenow": this.value, "aria-valuemin": '0', "aria-valuemax": '100', class: {
                'nv-progressbar': true,
                'nv-progressbar-primary': this.color === 'primary',
                'nv-progressbar-secondary': this.color === 'secondary',
                'nv-progressbar-warning': this.color === 'warning',
                'nv-progressbar-danger': this.color === 'danger',
                'nv-progressbar-success': this.color === 'success',
                'nv-progressbar-indeterminate': this.indeterminate,
            }, style: { width: !this.circle || !this.semiCircle ? `${this.value}%` : null } }));
    }
    makeLabel() {
        if (this.label) {
            return h("span", { class: 'nv-progressbar-label' }, this.value, "%");
        }
    }
    render() {
        const progressBar = this.makeProgressBar();
        return (h(Host, { class: {
                'nv-progress': true,
                'nv-progress-rounded': this.rounded,
                'nv-progress-circle': this.circle,
                'nv-progress-semi-circle': this.semiCircle,
                'nv-progress-square': this.square,
                'nv-progressbar-completed': this.value >= 100,
            } }, progressBar));
    }
    static get watchers() { return {
        "value": ["handleValue"]
    }; }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-progress",
        "$members$": {
            "value": [514],
            "color": [1],
            "indeterminate": [4],
            "rounded": [4],
            "circle": [4],
            "semiCircle": [4, "semi-circle"],
            "square": [4],
            "label": [4],
            "dashoffset": [32],
            "progress": [64]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvRadio {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Props
        //
        /**
         * If `true`, the user cannot interecat with the radio.
         */
        this.disabled = true;
        /**
         * The color to use from your application color palette.
         */
        this.color = 'primary';
        //
        // Methods
        //
        this.updateCheck = () => {
            if (this.checked) {
                this.nvDeselected.emit();
            }
            else {
                this.checked = true;
            }
        };
        this.nvSelect = createEvent(this, "nvSelect", 7);
        this.nvDeselected = createEvent(this, "nvDeselected", 7);
    }
    //
    // Watch
    //
    handleChecked(isChecked) {
        if (isChecked) {
            this.nvSelect.emit({
                checked: true,
                value: this.value,
            });
        }
    }
    render() {
        return (h("div", { role: 'radiogroup', class: {
                'nv-radio': true,
                'nv-radio-checked': this.checked,
                'nv-radio-disabled': this.disabled,
                'nv-radio-primary': this.color === 'primary' ? true : false,
                'nv-radio-secondary': this.color === 'secondary' ? true : false,
                'nv-radio-warning': this.color === 'warning' ? true : false,
                'nv-radio-danger': this.color === 'danger' ? true : false,
                'nv-radio-success': this.color === 'success' ? true : false,
            }, onClick: this.updateCheck }, h("input", { type: 'radio', name: this.name, value: this.value, checked: this.checked }), h("div", { class: 'checkmark-container' }, h("span", { class: 'checkmark' }))));
    }
    static get watchers() { return {
        "checked": ["handleChecked"]
    }; }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-radio",
        "$members$": {
            "disabled": [4],
            "name": [1],
            "checked": [1028],
            "value": [8],
            "color": [1]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvRadioGroup {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        this.onSelect = (ev) => {
            this.updateRadios(ev);
        };
    }
    //
    // Lifecyles
    //
    componentDidLoad() {
        this.addName();
    }
    //
    // Methods
    //
    async updateRadios(ev) {
        const radios = this.el.querySelectorAll('nv-radio');
        radios.forEach((r) => {
            console.log(r);
            if (r !== ev.target) {
                r.checked = false;
            }
        });
    }
    async addName() {
        const radios = this.el.querySelectorAll('input[type="radio"]');
        if (this.name) {
            radios.forEach((r) => {
                r.name = this.name;
            });
        }
    }
    render() {
        return (h(Host, { role: 'radiogroup', onNvSelect: this.onSelect, class: {
                'nv-radio-group': true,
            } }, h("slot", null)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-radio-group",
        "$members$": {
            "name": [1]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvRating {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * The number of the stars.
         */
        this.stars = 5;
        /**
         * If `true`, the user cannot interact with the rating.
         */
        this.readonly = false;
        /**
         * If `true`, the user cannot interact with the rating and
         * the rating accept float numbers for the display a general rating.
         */
        this.generalRate = false;
        this.nvChange = createEvent(this, "nvChange", 7);
    }
    handdleValue(v) {
        if (v && !this.readonly) {
            this.starSelected = v;
            this.nvChange.emit({ value: v });
        }
    }
    //
    // Lifecycles
    //
    componentDidLoad() {
        this.starSelected = this.rate;
        if (this.generalRate) {
            this.readonly = true;
            this.starSelected = this.stars;
        }
    }
    onOver(i) {
        this.starSelected = i;
    }
    onLeave() {
        this.starSelected = this.rate - 1;
    }
    onClick(i) {
        this.rate = i + 1;
        this.starSelected = i;
    }
    makeStar() {
        const stars = [];
        for (let i = 0; i < this.stars; i += 1) {
            const events = !this.readonly ? {
                onMouseOver: () => this.onOver(i),
                onMouseLeave: () => this.onLeave(),
                onClick: () => this.onClick(i),
            } : null;
            const starContent = (h("div", Object.assign({ class: {
                    'nv-star-content': true,
                    'nv-star-show': this.starSelected >= i,
                } }, events), h("svg", { viewBox: '0 0 30 30' }, h("path", { d: 'm22.631 26.775-7.4756-3.7864-7.3572\n            4.0116 1.291-8.2798-6.0888-5.7575 8.2735-1.3308 3.5941-7.57 3.8223\n            7.4573 8.3101 1.079-5.9112 5.9397z' }))));
            stars.push(starContent);
        }
        return stars;
    }
    makeStarBorder() {
        const stars = [];
        for (let i = 0; i < this.stars; i += 1) {
            stars.push(h("svg", { viewBox: '0 0 30 30' }, h("g", { transform: 'translate(0 -289.06)' }, h("path", { d: 'm14.867 293.12-3.5938 7.5703-8.2734 1.3301 6.0898\n            5.7578-1.291 8.2793 7.3555-4.0117 7.4766 3.7871-0.37305-1.9922-1.168-6.2441\n            5.9102-5.9394-8.3105-1.0801zm0.06836 4.5176 2.4551 4.7871\n            5.3359 0.69336-3.7969 3.8125 0.99023 5.2871-4.7988-2.4316-4.7227 2.5762\n            0.82812-5.3145-3.9102-3.6973 5.3125-0.85352z' }))));
        }
        return stars;
    }
    render() {
        const starBoder = this.makeStarBorder();
        const star = this.makeStar();
        const getStyle = () => {
            const style = {};
            if (this.generalRate) {
                style.width = `${(this.rate * 100) / this.stars}%`;
            }
            return style;
        };
        return (h(Host, { class: {
                'nv-rating': true,
                'nv-rating-primary': this.color === 'primary' ? true : false,
                'nv-rating-secondary': this.color === 'secondary' ? true : false,
                'nv-rating-warning': this.color === 'warning' ? true : false,
                'nv-rating-danger': this.color === 'danger' ? true : false,
                'nv-rating-success': this.color === 'success' ? true : false,
                'nv-rating-no-interact': this.readonly,
            } }, h("div", { class: 'nv-stars-bkg' }, starBoder), h("div", { class: 'nv-stars-interact' }, h("div", { class: 'nv-stars-interact-container', style: getStyle() }, star))));
    }
    static get watchers() { return {
        "rate": ["handdleValue"]
    }; }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-rating",
        "$members$": {
            "color": [1],
            "stars": [1026],
            "readonly": [4],
            "generalRate": [4, "general-rate"],
            "rate": [1026],
            "starSelected": [32]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

/**
 * A collection of shims that provide minimal functionality of the ES6 collections.
 *
 * These implementations are not meant to be used outside of the ResizeObserver
 * modules as they cover only a limited range of use cases.
 */
/* eslint-disable require-jsdoc, valid-jsdoc */
var MapShim = (function () {
    if (typeof Map !== 'undefined') {
        return Map;
    }
    /**
     * Returns index in provided array that matches the specified key.
     *
     * @param {Array<Array>} arr
     * @param {*} key
     * @returns {number}
     */
    function getIndex(arr, key) {
        var result = -1;
        arr.some(function (entry, index) {
            if (entry[0] === key) {
                result = index;
                return true;
            }
            return false;
        });
        return result;
    }
    return /** @class */ (function () {
        function class_1() {
            this.__entries__ = [];
        }
        Object.defineProperty(class_1.prototype, "size", {
            /**
             * @returns {boolean}
             */
            get: function () {
                return this.__entries__.length;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {*} key
         * @returns {*}
         */
        class_1.prototype.get = function (key) {
            var index = getIndex(this.__entries__, key);
            var entry = this.__entries__[index];
            return entry && entry[1];
        };
        /**
         * @param {*} key
         * @param {*} value
         * @returns {void}
         */
        class_1.prototype.set = function (key, value) {
            var index = getIndex(this.__entries__, key);
            if (~index) {
                this.__entries__[index][1] = value;
            }
            else {
                this.__entries__.push([key, value]);
            }
        };
        /**
         * @param {*} key
         * @returns {void}
         */
        class_1.prototype.delete = function (key) {
            var entries = this.__entries__;
            var index = getIndex(entries, key);
            if (~index) {
                entries.splice(index, 1);
            }
        };
        /**
         * @param {*} key
         * @returns {void}
         */
        class_1.prototype.has = function (key) {
            return !!~getIndex(this.__entries__, key);
        };
        /**
         * @returns {void}
         */
        class_1.prototype.clear = function () {
            this.__entries__.splice(0);
        };
        /**
         * @param {Function} callback
         * @param {*} [ctx=null]
         * @returns {void}
         */
        class_1.prototype.forEach = function (callback, ctx) {
            if (ctx === void 0) { ctx = null; }
            for (var _i = 0, _a = this.__entries__; _i < _a.length; _i++) {
                var entry = _a[_i];
                callback.call(ctx, entry[1], entry[0]);
            }
        };
        return class_1;
    }());
})();

/**
 * Detects whether window and document objects are available in current environment.
 */
var isBrowser = typeof window !== 'undefined' && typeof document !== 'undefined' && window.document === document;

// Returns global object of a current environment.
var global$1 = (function () {
    if (typeof global !== 'undefined' && global.Math === Math) {
        return global;
    }
    if (typeof self !== 'undefined' && self.Math === Math) {
        return self;
    }
    if (typeof window !== 'undefined' && window.Math === Math) {
        return window;
    }
    // eslint-disable-next-line no-new-func
    return Function('return this')();
})();

/**
 * A shim for the requestAnimationFrame which falls back to the setTimeout if
 * first one is not supported.
 *
 * @returns {number} Requests' identifier.
 */
var requestAnimationFrame$1 = (function () {
    if (typeof requestAnimationFrame === 'function') {
        // It's required to use a bounded function because IE sometimes throws
        // an "Invalid calling object" error if rAF is invoked without the global
        // object on the left hand side.
        return requestAnimationFrame.bind(global$1);
    }
    return function (callback) { return setTimeout(function () { return callback(Date.now()); }, 1000 / 60); };
})();

// Defines minimum timeout before adding a trailing call.
var trailingTimeout = 2;
/**
 * Creates a wrapper function which ensures that provided callback will be
 * invoked only once during the specified delay period.
 *
 * @param {Function} callback - Function to be invoked after the delay period.
 * @param {number} delay - Delay after which to invoke callback.
 * @returns {Function}
 */
function throttle (callback, delay) {
    var leadingCall = false, trailingCall = false, lastCallTime = 0;
    /**
     * Invokes the original callback function and schedules new invocation if
     * the "proxy" was called during current request.
     *
     * @returns {void}
     */
    function resolvePending() {
        if (leadingCall) {
            leadingCall = false;
            callback();
        }
        if (trailingCall) {
            proxy();
        }
    }
    /**
     * Callback invoked after the specified delay. It will further postpone
     * invocation of the original function delegating it to the
     * requestAnimationFrame.
     *
     * @returns {void}
     */
    function timeoutCallback() {
        requestAnimationFrame$1(resolvePending);
    }
    /**
     * Schedules invocation of the original function.
     *
     * @returns {void}
     */
    function proxy() {
        var timeStamp = Date.now();
        if (leadingCall) {
            // Reject immediately following calls.
            if (timeStamp - lastCallTime < trailingTimeout) {
                return;
            }
            // Schedule new call to be in invoked when the pending one is resolved.
            // This is important for "transitions" which never actually start
            // immediately so there is a chance that we might miss one if change
            // happens amids the pending invocation.
            trailingCall = true;
        }
        else {
            leadingCall = true;
            trailingCall = false;
            setTimeout(timeoutCallback, delay);
        }
        lastCallTime = timeStamp;
    }
    return proxy;
}

// Minimum delay before invoking the update of observers.
var REFRESH_DELAY = 20;
// A list of substrings of CSS properties used to find transition events that
// might affect dimensions of observed elements.
var transitionKeys = ['top', 'right', 'bottom', 'left', 'width', 'height', 'size', 'weight'];
// Check if MutationObserver is available.
var mutationObserverSupported = typeof MutationObserver !== 'undefined';
/**
 * Singleton controller class which handles updates of ResizeObserver instances.
 */
var ResizeObserverController = /** @class */ (function () {
    /**
     * Creates a new instance of ResizeObserverController.
     *
     * @private
     */
    function ResizeObserverController() {
        /**
         * Indicates whether DOM listeners have been added.
         *
         * @private {boolean}
         */
        this.connected_ = false;
        /**
         * Tells that controller has subscribed for Mutation Events.
         *
         * @private {boolean}
         */
        this.mutationEventsAdded_ = false;
        /**
         * Keeps reference to the instance of MutationObserver.
         *
         * @private {MutationObserver}
         */
        this.mutationsObserver_ = null;
        /**
         * A list of connected observers.
         *
         * @private {Array<ResizeObserverSPI>}
         */
        this.observers_ = [];
        this.onTransitionEnd_ = this.onTransitionEnd_.bind(this);
        this.refresh = throttle(this.refresh.bind(this), REFRESH_DELAY);
    }
    /**
     * Adds observer to observers list.
     *
     * @param {ResizeObserverSPI} observer - Observer to be added.
     * @returns {void}
     */
    ResizeObserverController.prototype.addObserver = function (observer) {
        if (!~this.observers_.indexOf(observer)) {
            this.observers_.push(observer);
        }
        // Add listeners if they haven't been added yet.
        if (!this.connected_) {
            this.connect_();
        }
    };
    /**
     * Removes observer from observers list.
     *
     * @param {ResizeObserverSPI} observer - Observer to be removed.
     * @returns {void}
     */
    ResizeObserverController.prototype.removeObserver = function (observer) {
        var observers = this.observers_;
        var index = observers.indexOf(observer);
        // Remove observer if it's present in registry.
        if (~index) {
            observers.splice(index, 1);
        }
        // Remove listeners if controller has no connected observers.
        if (!observers.length && this.connected_) {
            this.disconnect_();
        }
    };
    /**
     * Invokes the update of observers. It will continue running updates insofar
     * it detects changes.
     *
     * @returns {void}
     */
    ResizeObserverController.prototype.refresh = function () {
        var changesDetected = this.updateObservers_();
        // Continue running updates if changes have been detected as there might
        // be future ones caused by CSS transitions.
        if (changesDetected) {
            this.refresh();
        }
    };
    /**
     * Updates every observer from observers list and notifies them of queued
     * entries.
     *
     * @private
     * @returns {boolean} Returns "true" if any observer has detected changes in
     *      dimensions of it's elements.
     */
    ResizeObserverController.prototype.updateObservers_ = function () {
        // Collect observers that have active observations.
        var activeObservers = this.observers_.filter(function (observer) {
            return observer.gatherActive(), observer.hasActive();
        });
        // Deliver notifications in a separate cycle in order to avoid any
        // collisions between observers, e.g. when multiple instances of
        // ResizeObserver are tracking the same element and the callback of one
        // of them changes content dimensions of the observed target. Sometimes
        // this may result in notifications being blocked for the rest of observers.
        activeObservers.forEach(function (observer) { return observer.broadcastActive(); });
        return activeObservers.length > 0;
    };
    /**
     * Initializes DOM listeners.
     *
     * @private
     * @returns {void}
     */
    ResizeObserverController.prototype.connect_ = function () {
        // Do nothing if running in a non-browser environment or if listeners
        // have been already added.
        if (!isBrowser || this.connected_) {
            return;
        }
        // Subscription to the "Transitionend" event is used as a workaround for
        // delayed transitions. This way it's possible to capture at least the
        // final state of an element.
        document.addEventListener('transitionend', this.onTransitionEnd_);
        window.addEventListener('resize', this.refresh);
        if (mutationObserverSupported) {
            this.mutationsObserver_ = new MutationObserver(this.refresh);
            this.mutationsObserver_.observe(document, {
                attributes: true,
                childList: true,
                characterData: true,
                subtree: true
            });
        }
        else {
            document.addEventListener('DOMSubtreeModified', this.refresh);
            this.mutationEventsAdded_ = true;
        }
        this.connected_ = true;
    };
    /**
     * Removes DOM listeners.
     *
     * @private
     * @returns {void}
     */
    ResizeObserverController.prototype.disconnect_ = function () {
        // Do nothing if running in a non-browser environment or if listeners
        // have been already removed.
        if (!isBrowser || !this.connected_) {
            return;
        }
        document.removeEventListener('transitionend', this.onTransitionEnd_);
        window.removeEventListener('resize', this.refresh);
        if (this.mutationsObserver_) {
            this.mutationsObserver_.disconnect();
        }
        if (this.mutationEventsAdded_) {
            document.removeEventListener('DOMSubtreeModified', this.refresh);
        }
        this.mutationsObserver_ = null;
        this.mutationEventsAdded_ = false;
        this.connected_ = false;
    };
    /**
     * "Transitionend" event handler.
     *
     * @private
     * @param {TransitionEvent} event
     * @returns {void}
     */
    ResizeObserverController.prototype.onTransitionEnd_ = function (_a) {
        var _b = _a.propertyName, propertyName = _b === void 0 ? '' : _b;
        // Detect whether transition may affect dimensions of an element.
        var isReflowProperty = transitionKeys.some(function (key) {
            return !!~propertyName.indexOf(key);
        });
        if (isReflowProperty) {
            this.refresh();
        }
    };
    /**
     * Returns instance of the ResizeObserverController.
     *
     * @returns {ResizeObserverController}
     */
    ResizeObserverController.getInstance = function () {
        if (!this.instance_) {
            this.instance_ = new ResizeObserverController();
        }
        return this.instance_;
    };
    /**
     * Holds reference to the controller's instance.
     *
     * @private {ResizeObserverController}
     */
    ResizeObserverController.instance_ = null;
    return ResizeObserverController;
}());

/**
 * Defines non-writable/enumerable properties of the provided target object.
 *
 * @param {Object} target - Object for which to define properties.
 * @param {Object} props - Properties to be defined.
 * @returns {Object} Target object.
 */
var defineConfigurable = (function (target, props) {
    for (var _i = 0, _a = Object.keys(props); _i < _a.length; _i++) {
        var key = _a[_i];
        Object.defineProperty(target, key, {
            value: props[key],
            enumerable: false,
            writable: false,
            configurable: true
        });
    }
    return target;
});

/**
 * Returns the global object associated with provided element.
 *
 * @param {Object} target
 * @returns {Object}
 */
var getWindowOf = (function (target) {
    // Assume that the element is an instance of Node, which means that it
    // has the "ownerDocument" property from which we can retrieve a
    // corresponding global object.
    var ownerGlobal = target && target.ownerDocument && target.ownerDocument.defaultView;
    // Return the local global object if it's not possible extract one from
    // provided element.
    return ownerGlobal || global$1;
});

// Placeholder of an empty content rectangle.
var emptyRect = createRectInit(0, 0, 0, 0);
/**
 * Converts provided string to a number.
 *
 * @param {number|string} value
 * @returns {number}
 */
function toFloat(value) {
    return parseFloat(value) || 0;
}
/**
 * Extracts borders size from provided styles.
 *
 * @param {CSSStyleDeclaration} styles
 * @param {...string} positions - Borders positions (top, right, ...)
 * @returns {number}
 */
function getBordersSize(styles) {
    var positions = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        positions[_i - 1] = arguments[_i];
    }
    return positions.reduce(function (size, position) {
        var value = styles['border-' + position + '-width'];
        return size + toFloat(value);
    }, 0);
}
/**
 * Extracts paddings sizes from provided styles.
 *
 * @param {CSSStyleDeclaration} styles
 * @returns {Object} Paddings box.
 */
function getPaddings(styles) {
    var positions = ['top', 'right', 'bottom', 'left'];
    var paddings = {};
    for (var _i = 0, positions_1 = positions; _i < positions_1.length; _i++) {
        var position = positions_1[_i];
        var value = styles['padding-' + position];
        paddings[position] = toFloat(value);
    }
    return paddings;
}
/**
 * Calculates content rectangle of provided SVG element.
 *
 * @param {SVGGraphicsElement} target - Element content rectangle of which needs
 *      to be calculated.
 * @returns {DOMRectInit}
 */
function getSVGContentRect(target) {
    var bbox = target.getBBox();
    return createRectInit(0, 0, bbox.width, bbox.height);
}
/**
 * Calculates content rectangle of provided HTMLElement.
 *
 * @param {HTMLElement} target - Element for which to calculate the content rectangle.
 * @returns {DOMRectInit}
 */
function getHTMLElementContentRect(target) {
    // Client width & height properties can't be
    // used exclusively as they provide rounded values.
    var clientWidth = target.clientWidth, clientHeight = target.clientHeight;
    // By this condition we can catch all non-replaced inline, hidden and
    // detached elements. Though elements with width & height properties less
    // than 0.5 will be discarded as well.
    //
    // Without it we would need to implement separate methods for each of
    // those cases and it's not possible to perform a precise and performance
    // effective test for hidden elements. E.g. even jQuery's ':visible' filter
    // gives wrong results for elements with width & height less than 0.5.
    if (!clientWidth && !clientHeight) {
        return emptyRect;
    }
    var styles = getWindowOf(target).getComputedStyle(target);
    var paddings = getPaddings(styles);
    var horizPad = paddings.left + paddings.right;
    var vertPad = paddings.top + paddings.bottom;
    // Computed styles of width & height are being used because they are the
    // only dimensions available to JS that contain non-rounded values. It could
    // be possible to utilize the getBoundingClientRect if only it's data wasn't
    // affected by CSS transformations let alone paddings, borders and scroll bars.
    var width = toFloat(styles.width), height = toFloat(styles.height);
    // Width & height include paddings and borders when the 'border-box' box
    // model is applied (except for IE).
    if (styles.boxSizing === 'border-box') {
        // Following conditions are required to handle Internet Explorer which
        // doesn't include paddings and borders to computed CSS dimensions.
        //
        // We can say that if CSS dimensions + paddings are equal to the "client"
        // properties then it's either IE, and thus we don't need to subtract
        // anything, or an element merely doesn't have paddings/borders styles.
        if (Math.round(width + horizPad) !== clientWidth) {
            width -= getBordersSize(styles, 'left', 'right') + horizPad;
        }
        if (Math.round(height + vertPad) !== clientHeight) {
            height -= getBordersSize(styles, 'top', 'bottom') + vertPad;
        }
    }
    // Following steps can't be applied to the document's root element as its
    // client[Width/Height] properties represent viewport area of the window.
    // Besides, it's as well not necessary as the <html> itself neither has
    // rendered scroll bars nor it can be clipped.
    if (!isDocumentElement(target)) {
        // In some browsers (only in Firefox, actually) CSS width & height
        // include scroll bars size which can be removed at this step as scroll
        // bars are the only difference between rounded dimensions + paddings
        // and "client" properties, though that is not always true in Chrome.
        var vertScrollbar = Math.round(width + horizPad) - clientWidth;
        var horizScrollbar = Math.round(height + vertPad) - clientHeight;
        // Chrome has a rather weird rounding of "client" properties.
        // E.g. for an element with content width of 314.2px it sometimes gives
        // the client width of 315px and for the width of 314.7px it may give
        // 314px. And it doesn't happen all the time. So just ignore this delta
        // as a non-relevant.
        if (Math.abs(vertScrollbar) !== 1) {
            width -= vertScrollbar;
        }
        if (Math.abs(horizScrollbar) !== 1) {
            height -= horizScrollbar;
        }
    }
    return createRectInit(paddings.left, paddings.top, width, height);
}
/**
 * Checks whether provided element is an instance of the SVGGraphicsElement.
 *
 * @param {Element} target - Element to be checked.
 * @returns {boolean}
 */
var isSVGGraphicsElement = (function () {
    // Some browsers, namely IE and Edge, don't have the SVGGraphicsElement
    // interface.
    if (typeof SVGGraphicsElement !== 'undefined') {
        return function (target) { return target instanceof getWindowOf(target).SVGGraphicsElement; };
    }
    // If it's so, then check that element is at least an instance of the
    // SVGElement and that it has the "getBBox" method.
    // eslint-disable-next-line no-extra-parens
    return function (target) { return (target instanceof getWindowOf(target).SVGElement &&
        typeof target.getBBox === 'function'); };
})();
/**
 * Checks whether provided element is a document element (<html>).
 *
 * @param {Element} target - Element to be checked.
 * @returns {boolean}
 */
function isDocumentElement(target) {
    return target === getWindowOf(target).document.documentElement;
}
/**
 * Calculates an appropriate content rectangle for provided html or svg element.
 *
 * @param {Element} target - Element content rectangle of which needs to be calculated.
 * @returns {DOMRectInit}
 */
function getContentRect(target) {
    if (!isBrowser) {
        return emptyRect;
    }
    if (isSVGGraphicsElement(target)) {
        return getSVGContentRect(target);
    }
    return getHTMLElementContentRect(target);
}
/**
 * Creates rectangle with an interface of the DOMRectReadOnly.
 * Spec: https://drafts.fxtf.org/geometry/#domrectreadonly
 *
 * @param {DOMRectInit} rectInit - Object with rectangle's x/y coordinates and dimensions.
 * @returns {DOMRectReadOnly}
 */
function createReadOnlyRect(_a) {
    var x = _a.x, y = _a.y, width = _a.width, height = _a.height;
    // If DOMRectReadOnly is available use it as a prototype for the rectangle.
    var Constr = typeof DOMRectReadOnly !== 'undefined' ? DOMRectReadOnly : Object;
    var rect = Object.create(Constr.prototype);
    // Rectangle's properties are not writable and non-enumerable.
    defineConfigurable(rect, {
        x: x, y: y, width: width, height: height,
        top: y,
        right: x + width,
        bottom: height + y,
        left: x
    });
    return rect;
}
/**
 * Creates DOMRectInit object based on the provided dimensions and the x/y coordinates.
 * Spec: https://drafts.fxtf.org/geometry/#dictdef-domrectinit
 *
 * @param {number} x - X coordinate.
 * @param {number} y - Y coordinate.
 * @param {number} width - Rectangle's width.
 * @param {number} height - Rectangle's height.
 * @returns {DOMRectInit}
 */
function createRectInit(x, y, width, height) {
    return { x: x, y: y, width: width, height: height };
}

/**
 * Class that is responsible for computations of the content rectangle of
 * provided DOM element and for keeping track of it's changes.
 */
var ResizeObservation = /** @class */ (function () {
    /**
     * Creates an instance of ResizeObservation.
     *
     * @param {Element} target - Element to be observed.
     */
    function ResizeObservation(target) {
        /**
         * Broadcasted width of content rectangle.
         *
         * @type {number}
         */
        this.broadcastWidth = 0;
        /**
         * Broadcasted height of content rectangle.
         *
         * @type {number}
         */
        this.broadcastHeight = 0;
        /**
         * Reference to the last observed content rectangle.
         *
         * @private {DOMRectInit}
         */
        this.contentRect_ = createRectInit(0, 0, 0, 0);
        this.target = target;
    }
    /**
     * Updates content rectangle and tells whether it's width or height properties
     * have changed since the last broadcast.
     *
     * @returns {boolean}
     */
    ResizeObservation.prototype.isActive = function () {
        var rect = getContentRect(this.target);
        this.contentRect_ = rect;
        return (rect.width !== this.broadcastWidth ||
            rect.height !== this.broadcastHeight);
    };
    /**
     * Updates 'broadcastWidth' and 'broadcastHeight' properties with a data
     * from the corresponding properties of the last observed content rectangle.
     *
     * @returns {DOMRectInit} Last observed content rectangle.
     */
    ResizeObservation.prototype.broadcastRect = function () {
        var rect = this.contentRect_;
        this.broadcastWidth = rect.width;
        this.broadcastHeight = rect.height;
        return rect;
    };
    return ResizeObservation;
}());

var ResizeObserverEntry = /** @class */ (function () {
    /**
     * Creates an instance of ResizeObserverEntry.
     *
     * @param {Element} target - Element that is being observed.
     * @param {DOMRectInit} rectInit - Data of the element's content rectangle.
     */
    function ResizeObserverEntry(target, rectInit) {
        var contentRect = createReadOnlyRect(rectInit);
        // According to the specification following properties are not writable
        // and are also not enumerable in the native implementation.
        //
        // Property accessors are not being used as they'd require to define a
        // private WeakMap storage which may cause memory leaks in browsers that
        // don't support this type of collections.
        defineConfigurable(this, { target: target, contentRect: contentRect });
    }
    return ResizeObserverEntry;
}());

var ResizeObserverSPI = /** @class */ (function () {
    /**
     * Creates a new instance of ResizeObserver.
     *
     * @param {ResizeObserverCallback} callback - Callback function that is invoked
     *      when one of the observed elements changes it's content dimensions.
     * @param {ResizeObserverController} controller - Controller instance which
     *      is responsible for the updates of observer.
     * @param {ResizeObserver} callbackCtx - Reference to the public
     *      ResizeObserver instance which will be passed to callback function.
     */
    function ResizeObserverSPI(callback, controller, callbackCtx) {
        /**
         * Collection of resize observations that have detected changes in dimensions
         * of elements.
         *
         * @private {Array<ResizeObservation>}
         */
        this.activeObservations_ = [];
        /**
         * Registry of the ResizeObservation instances.
         *
         * @private {Map<Element, ResizeObservation>}
         */
        this.observations_ = new MapShim();
        if (typeof callback !== 'function') {
            throw new TypeError('The callback provided as parameter 1 is not a function.');
        }
        this.callback_ = callback;
        this.controller_ = controller;
        this.callbackCtx_ = callbackCtx;
    }
    /**
     * Starts observing provided element.
     *
     * @param {Element} target - Element to be observed.
     * @returns {void}
     */
    ResizeObserverSPI.prototype.observe = function (target) {
        if (!arguments.length) {
            throw new TypeError('1 argument required, but only 0 present.');
        }
        // Do nothing if current environment doesn't have the Element interface.
        if (typeof Element === 'undefined' || !(Element instanceof Object)) {
            return;
        }
        if (!(target instanceof getWindowOf(target).Element)) {
            throw new TypeError('parameter 1 is not of type "Element".');
        }
        var observations = this.observations_;
        // Do nothing if element is already being observed.
        if (observations.has(target)) {
            return;
        }
        observations.set(target, new ResizeObservation(target));
        this.controller_.addObserver(this);
        // Force the update of observations.
        this.controller_.refresh();
    };
    /**
     * Stops observing provided element.
     *
     * @param {Element} target - Element to stop observing.
     * @returns {void}
     */
    ResizeObserverSPI.prototype.unobserve = function (target) {
        if (!arguments.length) {
            throw new TypeError('1 argument required, but only 0 present.');
        }
        // Do nothing if current environment doesn't have the Element interface.
        if (typeof Element === 'undefined' || !(Element instanceof Object)) {
            return;
        }
        if (!(target instanceof getWindowOf(target).Element)) {
            throw new TypeError('parameter 1 is not of type "Element".');
        }
        var observations = this.observations_;
        // Do nothing if element is not being observed.
        if (!observations.has(target)) {
            return;
        }
        observations.delete(target);
        if (!observations.size) {
            this.controller_.removeObserver(this);
        }
    };
    /**
     * Stops observing all elements.
     *
     * @returns {void}
     */
    ResizeObserverSPI.prototype.disconnect = function () {
        this.clearActive();
        this.observations_.clear();
        this.controller_.removeObserver(this);
    };
    /**
     * Collects observation instances the associated element of which has changed
     * it's content rectangle.
     *
     * @returns {void}
     */
    ResizeObserverSPI.prototype.gatherActive = function () {
        var _this = this;
        this.clearActive();
        this.observations_.forEach(function (observation) {
            if (observation.isActive()) {
                _this.activeObservations_.push(observation);
            }
        });
    };
    /**
     * Invokes initial callback function with a list of ResizeObserverEntry
     * instances collected from active resize observations.
     *
     * @returns {void}
     */
    ResizeObserverSPI.prototype.broadcastActive = function () {
        // Do nothing if observer doesn't have active observations.
        if (!this.hasActive()) {
            return;
        }
        var ctx = this.callbackCtx_;
        // Create ResizeObserverEntry instance for every active observation.
        var entries = this.activeObservations_.map(function (observation) {
            return new ResizeObserverEntry(observation.target, observation.broadcastRect());
        });
        this.callback_.call(ctx, entries, ctx);
        this.clearActive();
    };
    /**
     * Clears the collection of active observations.
     *
     * @returns {void}
     */
    ResizeObserverSPI.prototype.clearActive = function () {
        this.activeObservations_.splice(0);
    };
    /**
     * Tells whether observer has active observations.
     *
     * @returns {boolean}
     */
    ResizeObserverSPI.prototype.hasActive = function () {
        return this.activeObservations_.length > 0;
    };
    return ResizeObserverSPI;
}());

// Registry of internal observers. If WeakMap is not available use current shim
// for the Map collection as it has all required methods and because WeakMap
// can't be fully polyfilled anyway.
var observers = typeof WeakMap !== 'undefined' ? new WeakMap() : new MapShim();
/**
 * ResizeObserver API. Encapsulates the ResizeObserver SPI implementation
 * exposing only those methods and properties that are defined in the spec.
 */
var ResizeObserver = /** @class */ (function () {
    /**
     * Creates a new instance of ResizeObserver.
     *
     * @param {ResizeObserverCallback} callback - Callback that is invoked when
     *      dimensions of the observed elements change.
     */
    function ResizeObserver(callback) {
        if (!(this instanceof ResizeObserver)) {
            throw new TypeError('Cannot call a class as a function.');
        }
        if (!arguments.length) {
            throw new TypeError('1 argument required, but only 0 present.');
        }
        var controller = ResizeObserverController.getInstance();
        var observer = new ResizeObserverSPI(callback, controller, this);
        observers.set(this, observer);
    }
    return ResizeObserver;
}());
// Expose public methods of ResizeObserver.
[
    'observe',
    'unobserve',
    'disconnect'
].forEach(function (method) {
    ResizeObserver.prototype[method] = function () {
        var _a;
        return (_a = observers.get(this))[method].apply(_a, arguments);
    };
});

var index = (function () {
    // Export existing implementation if available.
    if (typeof global$1.ResizeObserver !== 'undefined') {
        return global$1.ResizeObserver;
    }
    return ResizeObserver;
})();

class NvScroll {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // State
        //
        this.scrollTopMax = 0;
        this.scrollLeftMax = 0;
        this.hover = false;
        this.scrollTop = 0;
        this.scrollVTop = 0;
        this.scrollYHeight = 0;
        this.scrollLeft = 0;
        this.scrollVLeft = 0;
        this.scrollXWidth = 0;
        this.startup = false;
        //
        // Props
        //
        /**
         * If `true`, the scrollY will be displayed.
         */
        this.scrollY = true;
        /**
         * If `true`, the scrollX will be displayed.
         */
        this.scrollX = false;
        /**
         * The avatar size
         */
        this.size = 'medium';
        this.onOver = () => {
            this.hover = true;
        };
        this.onLeave = () => {
            this.hover = false;
        };
        this.onScroll = (e = undefined) => {
            const el = e ? e.target : this.content;
            if (el && !this.scrollActive) {
                const scrollTop = el.scrollTop * 100 / this.scrollTopMax;
                this.scrollVTop = scrollTop * (this.el.offsetHeight - this.scrollYHeight) / 100;
                const scrollLeft = el.scrollLeft * 100 / this.scrollLeftMax;
                this.scrollVLeft = scrollLeft * (this.el.offsetWidth - this.scrollXWidth) / 100;
            }
        };
        //
        // Mouse/Touch events
        //
        this.onStart = (e) => {
            const el = e.target;
            this.scrollActive = el.classList.contains('nv-v-scroll-y') ? 'y' : 'x';
            if (this.scrollActive === 'y') {
                const sliderBar = this.el.querySelector('.nv-v-scroll-y');
                const rect = sliderBar.getBoundingClientRect();
                const mouseEvt = e;
                const touchEvt = e;
                let currentPosY;
                if (e.type === 'touchstart') {
                    currentPosY =
                        touchEvt.targetTouches[0].clientY - rect.top;
                }
                else {
                    currentPosY = mouseEvt.clientY - rect.top;
                }
                // Prevent error on the position
                if (Math.sign(currentPosY) === -1) {
                    currentPosY = 0;
                }
                else if (currentPosY > sliderBar.clientHeight) {
                    currentPosY = sliderBar.clientHeight;
                }
                // Get the percent position.
                const percentPosY = (currentPosY / sliderBar.clientHeight) * 100;
                this.updatePosition(percentPosY);
            }
            else {
                const sliderBar = this.el.querySelector('.nv-v-scroll-x');
                const rect = sliderBar.getBoundingClientRect();
                const mouseEvt = e;
                const touchEvt = e;
                let currentPosX;
                if (e.type === 'touchstart') {
                    currentPosX =
                        touchEvt.targetTouches[0].clientX - rect.left;
                }
                else {
                    currentPosX = mouseEvt.clientX - rect.left;
                }
                // Prevent error on the position
                if (Math.sign(currentPosX) === -1) {
                    currentPosX = 0;
                }
                else if (currentPosX > sliderBar.clientWidth) {
                    currentPosX = sliderBar.clientWidth;
                }
                // Get the percent position.
                const percentPosX = (currentPosX / sliderBar.clientWidth) * 100;
                this.updatePosition(percentPosX);
            }
            this.addEvent();
        };
        this.onEnd = () => {
            this.scrollActive = undefined;
            this.removeEvent();
        };
        this.onMove = (e) => {
            if (this.scrollActive === 'y') {
                const sliderBar = this.el.querySelector('.nv-v-scroll-y');
                const rect = sliderBar.getBoundingClientRect();
                const mouseEvt = e;
                const touchEvt = e;
                let currentPosY;
                if (e.type === 'touchstart') {
                    currentPosY =
                        touchEvt.targetTouches[0].clientY - rect.top;
                }
                else {
                    currentPosY = mouseEvt.clientY - rect.top;
                }
                // Prevent error on the position
                if (Math.sign(currentPosY) === -1) {
                    currentPosY = 0;
                }
                else if (currentPosY > sliderBar.clientHeight) {
                    currentPosY = sliderBar.clientHeight;
                }
                // Get the percent position.
                const percentPosY = (currentPosY / sliderBar.clientHeight) * 100;
                this.updatePosition(percentPosY);
            }
            else {
                const sliderBar = this.el.querySelector('.nv-v-scroll-x');
                const rect = sliderBar.getBoundingClientRect();
                const mouseEvt = e;
                const touchEvt = e;
                let currentPosX;
                if (e.type === 'touchstart') {
                    currentPosX =
                        touchEvt.targetTouches[0].clientX - rect.left;
                }
                else {
                    currentPosX = mouseEvt.clientX - rect.left;
                }
                // Prevent error on the position
                if (Math.sign(currentPosX) === -1) {
                    currentPosX = 0;
                }
                else if (currentPosX > sliderBar.clientWidth) {
                    currentPosX = sliderBar.clientWidth;
                }
                // Get the percent position.
                const percentPosX = (currentPosX / sliderBar.clientWidth) * 100;
                this.updatePosition(percentPosX);
            }
        };
    }
    //
    // Lificycles
    //
    componentDidLoad() {
        this.getDimensions();
        const resizeObserver = new index(() => {
            this.getDimensions();
        });
        resizeObserver.observe(this.el);
        resizeObserver.observe(this.content);
        this.startup = true;
    }
    getDimensions() {
        const content = this.el.querySelector('.nv-scroll-content-container');
        const elHeight = this.el.offsetHeight;
        const elWidth = this.el.offsetWidth;
        if (content) {
            this.scrollTopMax = content.offsetHeight - elHeight;
            this.scrollYHeight = elHeight * (elHeight / content.offsetHeight);
            this.scrollLeftMax = content.offsetWidth - elWidth;
            this.scrollXWidth = elWidth * (elWidth / content.offsetWidth);
            this.onScroll();
        }
    }
    addEvent() {
        window.addEventListener('mouseup', this.onEnd);
        window.addEventListener('touchend', this.onEnd);
        window.addEventListener('mousemove', this.onMove);
        window.addEventListener('touchmove', this.onMove);
    }
    removeEvent() {
        window.removeEventListener('mousemove', this.onMove);
        window.removeEventListener('touchmove', this.onMove);
        window.removeEventListener('mouseup', this.onEnd);
        window.removeEventListener('touchend', this.onEnd);
    }
    updatePosition(currentPos) {
        if (this.scrollActive === 'y') {
            this.scrollTop = this.scrollTopMax * currentPos / 100;
            this.content.scrollTop = this.scrollTop;
            this.scrollVTop = currentPos * (this.el.offsetHeight - this.scrollYHeight) / 100;
        }
        else {
            this.scrollLeft = this.scrollLeftMax * currentPos / 100;
            this.content.scrollLeft = this.scrollLeft;
            this.scrollVLeft = currentPos * (this.el.offsetWidth - this.scrollXWidth) / 100;
        }
    }
    makeScrollY() {
        if ((this.el.offsetHeight !== this.scrollYHeight) && this.scrollY) {
            const style = () => {
                const style = {};
                style.top = `${this.scrollVTop}px`;
                style.height = `${this.scrollYHeight}px`;
                return style;
            };
            return (h("div", { class: 'nv-v-scroll-y', onMouseDown: this.onStart, onTouchStart: this.onStart }, h("div", { class: 'nv-scroll-y-slider', style: style() })));
        }
    }
    makeScrollX() {
        if ((this.el.offsetWidth !== this.scrollXWidth) && this.scrollX && this.startup) {
            const style = () => {
                const style = {};
                style.left = `${this.scrollVLeft}px`;
                style.width = `${this.scrollXWidth}px`;
                return style;
            };
            return (h("div", { class: 'nv-v-scroll-x', onMouseDown: this.onStart, onTouchStart: this.onStart }, h("div", { class: 'nv-scroll-x-slider', style: style() })));
        }
    }
    render() {
        const vScrollY = this.makeScrollY();
        const vScrollX = this.makeScrollX();
        return (h(Host, { role: 'presentation', class: {
                'nv-scroll': true,
                'nv-scroll-y': this.scrollY,
                'nv-scroll-x': this.scrollX,
                'nv-scroll-display': this.hover,
                'nv-scroll-active': this.scrollActive !== undefined,
                'nv-scroll-y-active': this.scrollActive === 'y',
                'nv-scroll-x-active': this.scrollActive === 'x',
            }, onMouseOver: this.onOver, onMouseLeave: this.onLeave }, h("div", { ref: el => this.content = el, class: 'nv-scroll-content', onScroll: this.onScroll }, h("div", { class: 'nv-scroll-content-container' }, h("slot", null))), vScrollY, vScrollX));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-scroll",
        "$members$": {
            "scrollY": [4, "scroll-y"],
            "scrollX": [4, "scroll-x"],
            "size": [1],
            "scrollTopMax": [32],
            "scrollLeftMax": [32],
            "hover": [32],
            "scrollTop": [32],
            "scrollVTop": [32],
            "scrollYHeight": [32],
            "scrollLeft": [32],
            "scrollVLeft": [32],
            "scrollXWidth": [32],
            "scrollActive": [32],
            "startup": [32]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvSelect {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // State
        //
        this.open = false;
        this.selectedValues = [];
        this.options = [];
        /**
         * If `true`, the user cannot interact with the select.
         */
        this.disabled = false;
        /**
         * If `true`, the select can accept multiple values.
         */
        this.multiple = false;
        this.onClick = () => {
            this.toggle();
        };
        this.onKeyDown = (e) => {
            if (e.key === 'Escape') {
                this.toggle();
            }
        };
        this.onStart = () => {
            this.toggle();
        };
        this.nvChange = createEvent(this, "nvChange", 7);
    }
    //
    // Lifecyles
    //
    componentDidLoad() {
        this.options = Array.from(this.el.querySelectorAll('nv-select-option'));
        if (this.value && typeof this.value === 'string') {
            this.initSelect().then(() => {
                this.options.forEach((option) => {
                    if (option.value === this.value) {
                        option.selected = true;
                        this.getValues();
                    }
                });
            });
        }
    }
    //
    // Events
    //
    nvSelectOptionSelectHandler(event) {
        if (this.multiple) {
            this.nvChange.emit({
                option: event.detail.option,
                multiple: true,
            });
            this.getValues();
        }
        else {
            this.nvChange.emit({
                option: event.detail.option,
                multiple: false,
            });
            this.getValues();
            this.toggle();
        }
    }
    //
    // Methods
    //
    /**
     * Close or open the select.
     */
    async toggle() {
        if (!this.disabled) {
            if (this.open) {
                this.open = false;
                this.removeEvent();
                return;
            }
            this.open = true;
            this.addEvent();
        }
    }
    addEvent() {
        if (!this.disabled) {
            window.addEventListener('keydown', this.onKeyDown);
            window.addEventListener('mousedown', this.onStart);
        }
    }
    removeEvent() {
        if (!this.disabled) {
            window.removeEventListener('keydown', this.onKeyDown);
            window.removeEventListener('mousedown', this.onStart);
        }
    }
    getValues() {
        const selectedValues = [];
        const value = [];
        this.options.forEach((option) => {
            if (option.selected) {
                selectedValues.push({
                    label: option.textLabel ? option.textLabel : option.innerText,
                    value: option.value || undefined,
                });
                value.push(option.value || undefined);
            }
        });
        this.selectedValues = selectedValues;
        this.value = value;
    }
    makeNativeSelect() {
        const options = [];
        if (this.name) {
            this.selectedValues.forEach((values) => {
                options.push(h("option", { value: values.value, selected: true }));
            });
            return (h("select", { name: this.name, hidden: true, disabled: this.disabled }, options));
        }
    }
    async initSelect() {
        // wait for all navItems to be ready.
        await Promise.all(this.options.map(item => item.componentOnReady()));
    }
    getLabel() {
        const selectedValues = this.selectedValues;
        let label = '';
        if (!this.value || this.value.length <= 0) {
            label = this.textLabel;
        }
        else {
            label = [].map.call(selectedValues, (content) => {
                return content.label;
            }).join(', ');
        }
        return label;
    }
    render() {
        const label = this.getLabel();
        const nativeSelect = this.makeNativeSelect();
        return (h(Host, { class: {
                'nv-select': true,
                'nv-select-opened': this.open,
                'nv-select-closed': !this.open,
                'nv-select-disabled': this.disabled,
            } }, h("div", { class: 'nv-select-el', onClick: this.onClick }, h("span", { class: 'nv-select-label' }, label)), h("div", { class: 'nv-select-items' }, h("slot", null)), nativeSelect));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-select",
        "$members$": {
            "textLabel": [1, "text-label"],
            "name": [1],
            "value": [1],
            "disabled": [4],
            "multiple": [4],
            "open": [32],
            "selectedValues": [32],
            "options": [32],
            "toggle": [64]
        },
        "$listeners$": [[0, "nvSelectOptionSelect", "nvSelectOptionSelectHandler"]],
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvSelectOption {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * If `true`, the select option is selected.
         */
        this.selected = false;
        /**
         * If `true`, the user cannot interact with the select option.
         */
        this.disabled = false;
        //
        // Methods
        //
        this.onClick = (e) => {
            e.preventDefault();
            if (this.selected) {
                this.selected = false;
                this.nvSelectOptionSelect.emit({
                    option: this.el,
                });
                return;
            }
            this.selected = true;
            this.nvSelectOptionSelect.emit({
                option: this.el,
            });
        };
        this.nvSelectOptionSelect = createEvent(this, "nvSelectOptionSelect", 7);
    }
    customEventHandler(event) {
        if (this.el !== event.detail.option && !event.detail.multiple) {
            this.selected = false;
        }
    }
    render() {
        return (h(Host, { class: {
                'nv-select-item': true,
                'nv-select-item-selected': this.selected,
                'nv-select-item-disabled': this.disabled,
            }, onMouseDown: !this.disabled ? this.onClick : null }, h("slot", null)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-select-option",
        "$members$": {
            "value": [1032],
            "selected": [1028],
            "disabled": [1028],
            "textLabel": [1025, "text-label"]
        },
        "$listeners$": [[16, "nvChange", "customEventHandler"]],
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvSlider {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        this.noUpdate = false;
        this.knobA = { value: 0, posX: 0 };
        this.knobB = { value: 0, posX: 0 };
        /**
         * Used to animate knobs and bars on the click event
         */
        this.fixPosition = false;
        //
        // Props
        //
        /**
         * The name of the control, which is submitted with the form data.
         */
        this.name = '';
        /**
         * If `true`, show two knobs.
         */
        this.dualKnobs = false;
        /**
         * Minimum integer value of the range.
         */
        this.min = 0;
        /**
         * Maximum integer value of the range.
         */
        this.max = 100;
        /**
         * If true, the knob snaps to tick marks evenly spaced based on the step property value.
         */
        this.snaps = false;
        /**
         * the value of the range.
         */
        this.value = 0;
        /**
         * Specifies the value granularity.
         */
        this.step = 1;
        /**
         * If `true`, the user cannot interact with the range.
         */
        this.disabled = false;
        /**
         * The color to use from your application color palette.
         */
        this.color = 'primary';
        //
        // Mouse and Touch events
        //
        this.onStart = (evt) => {
            const slider = this.el;
            this.rect = slider.getBoundingClientRect();
            const mouseEvt = evt;
            const touchEvt = evt;
            this.fixPosition = true;
            if (!this.dualKnobs) {
                this.pressedKnob = 'A';
            }
            let currentX;
            /**
             * change position left knob and the bar
             */
            if (evt.type === 'touchstart') {
                currentX =
                    touchEvt.targetTouches[0].clientX - this.rect.left;
            }
            else {
                currentX = mouseEvt.clientX - this.rect.left;
            }
            // Prevent error on the position
            if (Math.sign(currentX) === -1) {
                currentX = 0;
            }
            else if (currentX > slider.clientWidth) {
                currentX = slider.clientWidth;
            }
            // Get the nearest knob
            const percentX = (currentX / slider.clientWidth) * 100;
            this.pressedKnob =
                !this.dualKnobs ||
                    Math.abs(this.knobA.posX - percentX) < Math.abs(this.knobB.posX - percentX)
                    ? 'A'
                    : 'B';
            this.updatePosition(currentX);
            this.addEvent();
        };
        this.onEnd = () => {
            if (!this.pressedKnob) {
                return;
            }
            this.pressedKnob = undefined;
            this.removeEvent();
        };
        this.onMove = (evt) => {
            if (this.fixPosition) {
                this.fixPosition = false;
            }
            const slider = this.el;
            const mouseEvt = evt;
            const touchEvt = evt;
            let currentX;
            /**
             * change position left knob and the bar
             */
            if (evt.type === 'touchmove') {
                currentX =
                    touchEvt.targetTouches[0].clientX - slider.getBoundingClientRect().left;
            }
            else {
                currentX = mouseEvt.clientX - slider.getBoundingClientRect().left;
            }
            // Prevent error on the position
            if (Math.sign(currentX) === -1) {
                currentX = 0;
            }
            else if (currentX > slider.clientWidth) {
                currentX = slider.clientWidth;
            }
            this.updatePosition(currentX);
        };
        this.nvChange = createEvent(this, "nvChange", 7);
    }
    handleValue() {
        if (!this.disabled) {
            if (!this.noUpdate) {
                this.updateRatio();
            }
        }
    }
    //
    // Lifecycles
    //
    componentDidLoad() {
        this.updateRatio();
    }
    addEvent() {
        window.addEventListener('mouseup', this.onEnd);
        window.addEventListener('touchend', this.onEnd);
        window.addEventListener('mousemove', this.onMove);
        window.addEventListener('touchmove', this.onMove);
    }
    removeEvent() {
        window.removeEventListener('mousemove', this.onMove);
        window.removeEventListener('touchmove', this.onMove);
        window.removeEventListener('mouseup', this.onEnd);
        window.removeEventListener('touchend', this.onEnd);
    }
    updatePosition(currentX) {
        const slider = this.el;
        let posX = (currentX / slider.clientWidth) * 100;
        const lengthPerStep = 100 / ((this.max - this.min) / this.step);
        const steps = Math.round(posX / lengthPerStep);
        let value = steps * lengthPerStep * (this.max - this.min) * 0.01 + this.min;
        if (value > this.max) {
            value = this.max;
        }
        if (this.step > 1) {
            posX = steps * lengthPerStep;
            if (posX > 100) {
                posX = 100;
            }
        }
        // Store old positions
        const old = { A: this.knobA, B: this.knobB };
        // Update position and value from the pressed knob
        this.pressedKnob === 'A' ? this.knobA = { value, posX } : this.knobB = { value, posX };
        const emmitValue = this.dualKnobs ? {
            lower: Math.min(this.knobA.value, this.knobB.value),
            upper: Math.max(this.knobA.value, this.knobB.value),
        } : this.knobA.value;
        if ((old.A.posX !== this.knobA.posX || old.B.posX !== this.knobB.posX) ||
            (old.A.value !== this.knobA.value || old.B.value !== this.knobB.value)) {
            this.updateValue();
            this.nvChange.emit({ value: emmitValue });
        }
    }
    updateValue() {
        this.noUpdate = true;
        this.value = this.dualKnobs ? {
            lower: Math.min(this.knobA.value, this.knobB.value),
            upper: Math.max(this.knobA.value, this.knobB.value),
        } : this.knobA.value;
        this.noUpdate = false;
    }
    updateRatio() {
        const value = this.getValue();
        const lengthPerStep = 100 / ((this.max - this.min) / this.step);
        this.fixPosition = true;
        if (this.dualKnobs && typeof value === 'object') {
            let posKnobA = (value.lower - this.min) / (this.max - this.min) * 100;
            let posKnobB = (value.upper - this.min) / (this.max - this.min) * 100;
            const stepsA = Math.round(posKnobA / lengthPerStep);
            const stepsB = Math.round(posKnobB / lengthPerStep);
            let valueA = stepsA * lengthPerStep * (this.max - this.min) * 0.01 + this.min;
            let valueB = stepsB * lengthPerStep * (this.max - this.min) * 0.01 + this.min;
            if (valueA > this.max) {
                valueA = this.max;
            }
            if (valueB > this.max) {
                valueB = this.max;
            }
            if (this.step > 1) {
                posKnobA = stepsA * lengthPerStep;
                posKnobB = stepsB * lengthPerStep;
                if (posKnobA > 100) {
                    posKnobA = 100;
                }
                if (posKnobB > 100) {
                    posKnobB = 100;
                }
            }
            this.knobA = { value: valueA, posX: posKnobA };
            this.knobB = { value: valueB, posX: posKnobB };
            this.updateValue();
        }
        if (value && typeof value === 'number') {
            let posKnobA = (value - this.min) / (this.max - this.min) * 100;
            const stepsA = Math.round(posKnobA / lengthPerStep);
            let valueA = stepsA * lengthPerStep * (this.max - this.min) * 0.01 + this.min;
            if (valueA > this.max) {
                valueA = this.max;
            }
            if (this.step > 1) {
                posKnobA = stepsA * lengthPerStep;
                if (posKnobA > 100) {
                    posKnobA = 100;
                }
            }
            this.knobA = { value: valueA, posX: posKnobA };
            this.updateValue();
        }
    }
    getValue() {
        const value = this.value || 0;
        if (this.dualKnobs) {
            if (this.dualKnobs && typeof this.value === 'object') {
                return value;
            }
            return {
                lower: 0,
                upper: value,
            };
        }
        return value;
    }
    makeKnob({ disabled, min, max, knob, value, pressed, }) {
        let snap;
        if (this.snaps) {
            snap = (h("div", { class: {
                    'nv-slider-snap': true,
                } }, knob === 'A' ? this.knobA.value : this.knobB.value));
        }
        return (h("div", { class: {
                'nv-slider-knob-handle': true,
                'nv-slider-knob-a': knob === 'A',
                'nv-slider-knob-b': knob === 'B',
                'nv-slider-knob-pressed': pressed,
                'nv-slider-knob-min': value === min,
                'nv-slider-knob-max': value === max,
            }, style: { left: `${value}%` }, role: 'slider', tabindex: disabled ? -1 : 0, "aria-valuemin": min, "aria-valuemax": max, "aria-disabled": disabled ? 'true' : null, "aria-valuenow": value }, snap, h("div", { class: 'nv-slider-knob' })));
    }
    makeActivebar() {
        const knobA = this.knobA.posX;
        const knobB = this.knobB.posX;
        if (this.dualKnobs) {
            const diffWidth = this.pressedKnob ? '60px' : '30px';
            const diffLeft = this.pressedKnob ? '30px' : '15px';
            const width = `calc(${Math.abs(knobA) < Math.abs(knobB) ?
                `${knobB}% - ${knobA}%` : `${knobA}% - ${knobB}%`} - ${diffWidth})`;
            return (h("div", { class: 'nv-slider-bar-active', style: {
                    width,
                    left: Math.abs(knobA) < Math.abs(knobB) ?
                        `calc(${knobA}% + ${diffLeft})` : `calc(${knobB}% + ${diffLeft})`,
                } }));
        }
        return (h("div", { class: 'nv-slider-bar-active', style: { width: `calc(${knobA}% - 15px)` } }));
    }
    makeSlideBar() {
        if (this.dualKnobs) {
            const knobA = this.knobA.posX;
            const knobB = this.knobB.posX;
            return (h(Fragment, null, h("div", { class: 'nv-slider-bar', style: {
                    width: `calc(${knobA}% - 15px)`,
                    left: this.pressedKnob === 'A' ? '-15px' : '0px',
                }, role: 'presentation' }), h("div", { class: 'nv-slider-bar', style: {
                    width: `calc(${100 - knobB}% - 15px)`,
                    left: 'unset',
                    right: '0px',
                }, role: 'presentation' })));
        }
        return (h("div", { class: 'nv-slider-bar', style: { width: `calc(${100 - this.knobA.posX}% - 15px)` }, role: 'presentation' }));
    }
    makeTicks() {
        if (this.step <= 1) {
            return;
        }
        const step = this.step;
        const ticks = [];
        for (let value = this.min; value <= this.max; value += step) {
            const percentX = ((value - this.min) / (this.max - this.min) * 100);
            const tick = h("div", { class: {
                    'nv-slider-tick': true,
                    'nv-slider-tick-active': this.knobA.posX >= value,
                }, style: { left: `${percentX}%` } });
            ticks.push(tick);
        }
        if (!(this.step / 100 % 1 === 0)) {
            // Add the last tick
            ticks.push(h("div", { class: {
                    'nv-slider-tick': true,
                    'nv-slider-tick-active': this.knobA.posX === 100,
                }, style: { left: `100%` } }));
        }
        return (h("div", { class: 'nv-slider-ticks' }, ticks));
    }
    render() {
        const disabled = this.disabled;
        const min = this.min;
        const max = this.max;
        const knobA = this.makeKnob({
            disabled,
            min,
            max,
            knob: 'A',
            pressed: this.pressedKnob === 'A',
            value: this.knobA.posX,
        });
        const activeBar = this.makeActivebar();
        const slideBar = this.makeSlideBar();
        const ticks = this.makeTicks();
        let knobB;
        if (this.dualKnobs) {
            knobB = this.makeKnob({
                disabled,
                min,
                max,
                knob: 'B',
                pressed: this.pressedKnob === 'B',
                value: this.knobB.posX,
            });
        }
        return (h(Host, { class: {
                'nv-slider': true,
                'nv-slider-active': this.pressedKnob !== undefined,
                'nv-slider-salt-animation': this.fixPosition,
                'nv-slider-primary': this.color === 'primary',
                'nv-slider-secondary': this.color === 'secondary',
                'nv-slider-warning': this.color === 'warning',
                'nv-slider-danger': this.color === 'danger',
                'nv-slider-success': this.color === 'success',
                'nv-slider-disabled': this.disabled,
                'nv-slider-with-snaps': this.snaps,
                'nv-slider-dual-knobs': this.dualKnobs,
            }, onMouseDown: !this.disabled ? this.onStart : null, onTouchStart: !this.disabled ? this.onStart : null }, h("slot", { name: 'start' }), h("div", { class: 'nv-slider-bars' }, slideBar, activeBar, ticks), knobA, knobB, h("slot", { name: 'end' })));
    }
    get el() { return getElement(this); }
    static get watchers() { return {
        "value": ["handleValue"]
    }; }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-slider",
        "$members$": {
            "name": [1],
            "dualKnobs": [4, "dual-knobs"],
            "min": [2],
            "max": [2],
            "snaps": [4],
            "value": [1026],
            "step": [2],
            "disabled": [4],
            "color": [1],
            "pressedKnob": [32],
            "knobA": [32],
            "knobB": [32],
            "fixPosition": [32]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvSubHeader {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * The aria-level.
         */
        this.level = 1;
    }
    render() {
        return (h("div", { class: 'nv-sub-header', "aria-level": this.level }, h("div", { class: 'nv-sub-header-line' }), h("div", { class: 'nv-sub-header-text' }, h("slot", null)), h("div", { class: 'nv-sub-header-line' })));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-sub-header",
        "$members$": {
            "level": [2]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvSwitch {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * If `true`, the user cannot interact with the Swicth.
         */
        this.disabled = false;
        /**
         * The color of the Switch.
         */
        this.color = 'primary';
        /**
         * If `true`, the switch is checked.
         */
        this.checked = false;
    }
    render() {
        return (h("div", { class: {
                'nv-switch': true,
                'nv-switch-primary': this.color === 'primary' ? true : false,
                'nv-switch-secondary': this.color === 'secondary' ? true : false,
                'nv-switch-warning': this.color === 'warning' ? true : false,
                'nv-switch-danger': this.color === 'danger' ? true : false,
                'nv-switch-success': this.color === 'success' ? true : false,
                'nv-switch-disabled': this.disabled,
            } }, h("label", null, h("input", { type: 'checkbox', checked: this.checked, disabled: this.disabled }), h("span", { class: 'nv-slider round' }))));
    }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-switch",
        "$members$": {
            "disabled": [4],
            "color": [1],
            "checked": [4]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
function runNextAnimationFrame(callback) {
    requestAnimationFrame(() => {
        setTimeout(callback, 0);
    });
}

class NvTab {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /** @internal */
        this.active = false;
        this.nvPrevTabDidChange = createEvent(this, "nvPrevTabDidChange", 7);
    }
    handleScroll(evt) {
        if (evt.type === 'animationend') {
            if (this.el.classList.contains('nv-displaying-tab')) {
                this.el.classList.add('nv-tab-displayed');
                this.el.classList.remove('nv-displaying-tab');
            }
            // go back to initial state after hiding
            if (this.el.classList.contains('nv-hiding-tab')) {
                this.el.classList.remove('nv-tab-displayed');
                this.el.classList.remove('nv-displaying-tab');
                this.el.classList.remove('nv-hiding-tab');
                this.nvPrevTabDidChange.emit({
                    tab: this.tab,
                });
                this.active = false;
            }
        }
    }
    //
    // Lificycles
    //
    componentWillLoad() {
        this.rootTab = this.el.parentElement;
    }
    //
    // Methods
    //
    /**
     * Set the active component for the tab
     */
    async setActive() {
        const tab = this.el;
        this.active = true;
        // Wait a frame once display is no longer "none", to establish basis for animation
        runNextAnimationFrame(() => {
            // no operation during displaying and hiding
            if (tab.classList.contains('nv-displaying-tab') ||
                tab.classList.contains('nv-hiding-tab')) {
                return;
            }
            tab.classList.add('nv-displaying-tab');
        });
    }
    /**
     * Remote the active componente for the tab.
     */
    async removeActive() {
        this.el.classList.add('nv-hiding-tab');
    }
    render() {
        return (h(Host, { role: 'tabpanel', "aria-hidden": !this.active ? 'true' : null, "aria-labelledby": `tab-button-${this.tab}`, class: {
                'nv-tab': true,
                'nv-tab-hidden': !this.active,
            } }, h("div", { class: 'nv-tab-content' }, h("slot", null))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-tab",
        "$members$": {
            "active": [1028],
            "tab": [1],
            "setActive": [64],
            "removeActive": [64]
        },
        "$listeners$": [[0, "animationend", "handleScroll"]],
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvTabBar {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        this.nvTabBarChanged = createEvent(this, "nvTabBarChanged", 7);
    }
    //
    // Lifecycles
    //
    componentWillLoad() {
        this.selectedTabChanged();
    }
    //
    // Watchs
    //
    selectedTabChanged() {
        if (this.selectedTab !== undefined) {
            this.nvTabBarChanged.emit({
                tab: this.selectedTab,
            });
        }
    }
    render() {
        return (h(Host, { class: 'nv-tab-bar' }, h("slot", null)));
    }
    static get watchers() { return {
        "selectedTab": ["selectedTabChanged"]
    }; }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-tab-bar",
        "$members$": {
            "selectedTab": [1, "selected-tab"]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvTabButton {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * If `true`, the user cannot interact with the tab button.
         */
        this.disabled = false;
        this.onClick = (ev) => {
            if (!this.selected) {
                this.selectTab(ev);
            }
        };
        this.nvTabButtonClick = createEvent(this, "nvTabButtonClick", 7);
    }
    onTabBarChanged(ev) {
        this.selected = this.tab === ev.detail.tab;
    }
    selectTab(ev) {
        if (this.tab !== undefined) {
            if (!this.disabled) {
                this.nvTabButtonClick.emit({
                    tab: this.tab,
                    selected: this.selected,
                });
            }
            ev.preventDefault();
        }
    }
    get tabIndex() {
        if (this.disabled) {
            return -1;
        }
        const hasTabIndex = this.el.hasAttribute('tabindex');
        if (hasTabIndex) {
            return this.el.getAttribute('tabindex');
        }
        return 0;
    }
    render() {
        return (h(Host, { role: 'tab', tabindex: this.tabIndex, "aria-selected": this.selected ? 'true' : null, id: this.tab !== undefined ? `tab-button-${this.tab}` : null, onClick: this.onClick, class: {
                'nv-tab-button': true,
                'nv-tab-button-active': this.selected,
            } }, h("nv-button", { text: true }, h("slot", null))));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-tab-button",
        "$members$": {
            "disabled": [4],
            "tab": [1],
            "selected": [4]
        },
        "$listeners$": [[16, "nvTabBarChanged", "onTabBarChanged"]],
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvTabs {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * If true, the tab component is complete loaded and can initial select tab
         */
        this.start = false;
        //
        // State
        //
        this.tabs = [];
        //
        // Methods
        //
        this.onTabClicked = (ev) => {
            const { tab } = ev.detail;
            const selectedTab = this.tabs.find(t => t.tab === tab);
            if (selectedTab) {
                this.select(selectedTab);
            }
        };
        this.nvNavWillLoad = createEvent(this, "nvNavWillLoad", 7);
        this.nvTabsWillChange = createEvent(this, "nvTabsWillChange", 3);
        this.nvTabsDidChange = createEvent(this, "nvTabsDidChange", 3);
    }
    handleNvPrevTabDidChange() {
        this.selectedTab.setActive();
    }
    //
    // Lifecycles
    //
    componentWillLoad() {
        this.tabs = Array.from(this.el.querySelectorAll('nv-tab'));
        this.initSelect().then(() => {
            this.nvNavWillLoad.emit();
            this.componentWillUpdate();
        });
    }
    componentDidUnload() {
        this.tabs.length = 0;
        this.selectedTab = this.leavingTab = undefined;
    }
    componentWillUpdate() {
        const tabBar = this.el.querySelector('nv-tab-bar');
        if (tabBar) {
            const tab = this.selectedTab ? this.selectedTab.tab : undefined;
            tabBar.selectedTab = tab;
        }
    }
    //
    // Watchs
    //
    handleNvTabsWillChange() {
        if (!this.previousSelectedTab || this.previousSelectedTab.tab === this.selectedTab.tab) {
            return;
        }
        const topBar = this.el.querySelector('nv-tab-bar');
        topBar.setAttribute('selected-tab', this.selectedTab.tab);
        this.previousSelectedTab.removeActive().then(() => {
            this.previousSelectedTab = undefined;
        });
    }
    /**
     * Select a tab by the value of its `tab` property or an element reference.
     *
     * @param tab The tab instance to select.
     * If passed a string, it should be the value of the tab's `tab` property.
     */
    async select(tab) {
        const selectedTab = await this.getTab(tab);
        if (this.previousSelectedTab && (this.previousSelectedTab.tab === selectedTab.tab)) {
            return;
        }
        // Set current tab for the hidding.
        this.previousSelectedTab = this.selectedTab;
        await this.setActive(selectedTab);
        if (!this.start) {
            selectedTab.setActive();
            this.start = true;
        }
        return true;
    }
    /**
     * Get a specific tab by the value of its `tab` property or an element reference.
     *
     * @param tab The tab instance to select.
     * If passed a string, it should be the value of the tab's `tab` property.
     */
    async getTab(tab) {
        const tabEl = (typeof tab === 'string')
            ? this.tabs.find(t => t.tab === tab)
            : tab;
        if (!tabEl) {
            console.error(`tab with id: "${tabEl}" does not exist`);
        }
        return tabEl;
    }
    setActive(selectedTab) {
        this.leavingTab = this.selectedTab;
        this.selectedTab = selectedTab;
        this.nvTabsWillChange.emit({ tab: selectedTab.tab });
        return;
    }
    async initSelect() {
        // wait for all tabs to be ready
        await Promise.all(this.tabs.map(tab => tab.componentOnReady()));
        await this.select(this.tabs[0]);
    }
    render() {
        return (h(Host, { onNvTabButtonClick: this.onTabClicked, class: {
                'nv-tabs': true,
            } }, h("slot", { name: 'top' }), h("slot", null), h("slot", { name: 'bottom' })));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-tabs",
        "$members$": {
            "tabs": [32],
            "selectedTab": [32],
            "previousSelectedTab": [32],
            "select": [64],
            "getTab": [64]
        },
        "$listeners$": [[0, "nvPrevTabDidChange", "handleNvPrevTabDidChange"], [0, "nvTabsWillChange", "handleNvTabsWillChange"]],
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvTextField {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Props
        //
        /**
         * If `true`, the value of the input is readonly
         */
        this.readonly = false;
        /**
         * The min lenght for the input.
         */
        this.min = 0;
        /**
         * If `true`, the counter is displayed.
         */
        this.counter = false;
        /**
         * The type of the input.
         */
        this.type = 'text';
        /**
         * If `true`, the text field will be a textarea.
         */
        this.multiline = false;
        /**
         * If `true` the height element will increase based on the element.
         * Avalible only for **multiline(textarea)**.
         */
        this.autoGrow = false;
        /**
         * If `true`, the textarea cannot will resized.
         */
        this.noResize = false;
        /**
         * If `true`, the content of the password input will be displayed.
         */
        this.passView = false;
        this.showPassword = () => {
            if (this.type === 'password') {
                this.type = 'text';
                this.passView = true;
            }
            else {
                this.type = 'password';
                this.passView = false;
            }
        };
        this.inputFocus = false;
        this.valueLength = 0;
        this.onInput = createEvent(this, "onInput", 7);
    }
    //
    // Lifecyles
    //
    componentWillLoad() {
        if (this.value) {
            this.newValue = this.value;
        }
    }
    componentDidLoad() {
        if (this.prefixText) {
            const prefix = this.el.querySelector('.nv-text-field-prefix');
            this.prefixWidth = prefix.offsetWidth;
        }
        this.inputType = this.type;
    }
    //
    // Methods
    //
    async selectText() {
        const valueLength = this.input.value.length;
        this.input.focus();
        this.input.setSelectionRange(0, valueLength);
    }
    makeLabel() {
        let activeStatus = false;
        if (this.label) {
            if (this.inputFocus || this.newValue || this.inputFocus && !this.newValue) {
                activeStatus = true;
            }
            return (h("span", { class: {
                    ['nv-text-field-label']: true,
                    active: activeStatus,
                }, style: {
                    left: `${this.prefixWidth}px`,
                } }, this.label));
        }
    }
    makeBorderBottom() {
        return (h("div", { class: {
                'nv-line-ripple': true,
                active: this.inputFocus,
            } }));
    }
    makeHelperText() {
        return h("small", { class: 'nv-text-field-helper-text' }, this.helperText);
    }
    makeCounter() {
        if (this.counter && this.max) {
            return h("small", { class: 'nv-text-field-counter' }, this.valueLength, " / ", this.max);
        }
    }
    makePrefix() {
        if (this.prefixText) {
            return h("span", { class: 'nv-text-field-prefix' }, this.prefixText);
        }
    }
    makeSuffix() {
        if (this.suffixText) {
            return h("span", { class: 'nv-text-field-suffix' }, this.suffixText);
        }
    }
    handleChange(event) {
        this.newValue = event.target.value;
        this.value = event.target.value;
        const el = event.target;
        if (this.counter) {
            this.valueLength = event.target.value.length;
        }
        if (this.autoGrow && this.multiline) {
            this.height = el.scrollHeight;
        }
        this.onInput.emit(event);
    }
    handleFocus(value) {
        this.inputFocus = value;
    }
    makeInput() {
        if (this.multiline) {
            return (h("textarea", { onFocus: () => this.handleFocus(true), onBlur: () => this.handleFocus(false), onInput: event => this.handleChange(event), maxLength: this.max, minLength: this.min, readonly: this.readonly, cols: this.cols, rows: this.rows, style: {
                    height: `${this.height}px`,
                    resize: `${this.noResize ? 'none' : null}`,
                }, value: this.newValue, ref: el => this.input = el, name: this.name }));
        }
        return (h("input", { type: this.type, onFocus: () => this.handleFocus(true), onBlur: () => this.handleFocus(false), onInput: event => this.handleChange(event), maxLength: this.max, minLength: this.min, readonly: this.readonly, value: this.newValue, ref: el => this.input = el, name: this.name }));
    }
    makeView() {
        // tslint:disable: max-line-length
        if (this.inputType === 'password') {
            if (!this.passView) {
                return (h("nv-button", { onClick: this.showPassword, class: 'nv-txtfield-pass-view-btn', rounded: true, text: true }, h("svg", { viewBox: '0 0 30 30' }, h("g", { transform: 'translate(0 -289.06)' }, h("path", { transform: 'translate(0 289.06)', d: 'm25.607 4.3926-21.215 21.215 1.4141 1.4141 5.8438-5.8438a4\n                  4 0 0 0 3.3496 1.8223 4 4 0 0 0 4-4 4 4 0 0 0-1.8203-3.3516l4.2812-4.2812c2.1638\n                  1.8324 3.5391 4.5664 3.5391 7.6328h2c0-3.6056-1.6064-6.8373-4.1328-9.0391l4.1543-4.1543-1.4141-1.4141zm-10.607\n                  2.6074c-6.6156 0-12 5.3844-12 12h2c0-5.5347 4.4653-10 10-10 0.96443 0 1.8939 0.14328 2.7754 0.39648l1.5684-1.5684c-1.3491-0.52662-2.8107-0.82812-4.3438-0.82812z' })))));
            }
            return (h("nv-button", { onClick: this.showPassword, class: 'nv-txtfield-pass-view-btn', rounded: true, text: true }, h("svg", { viewBox: '0 0 30 30' }, h("g", { transform: 'translate(0 -289.06)' }, h("path", { d: 'm15 296.06c-6.6156 0-12 5.3844-12\n                12h2c0-5.5347 4.4653-10 10-10 5.5347 0\n                10 4.4653 10 10h2c0-6.6156-5.3844-12-12-12zm0 8a4 4 0 0 0-4 4 4\n                4 0 0 0 4 4 4 4 0 0 0 4-4 4 4 0 0 0-4-4z' })))));
        }
    }
    render() {
        const label = this.makeLabel();
        const borderBottom = this.makeBorderBottom();
        const helperText = this.makeHelperText();
        const counter = this.makeCounter();
        const prefix = this.makePrefix();
        const suffix = this.makeSuffix();
        const makeInput = this.makeInput();
        const view = this.makeView();
        return (h(Host, { class: {
                'nv-text-field': true,
                'nv-text-field-password': this.type === 'password',
                'nv-input-shaped': this.variant === 'shaped' ? true : false,
                'nv-input-shaped-filled': this.variant === 'shaped-filled' ? true : false,
                'nv-input-outlined': this.variant === 'outlined' ? true : false,
                'nv-input-shaped-outlined': this.variant === 'shaped-outlined' ? true : false,
            } }, h("div", { class: 'nv-text-field-container' }, h("div", { class: 'nv-text-field-elements' }, prefix, makeInput, suffix, view, label), borderBottom), h("div", { class: 'nv-text-field-helper-line' }, helperText, counter)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-text-field",
        "$members$": {
            "readonly": [4],
            "name": [1],
            "label": [1],
            "helperText": [1, "helper-text"],
            "variant": [1],
            "max": [2],
            "min": [2],
            "counter": [4],
            "value": [1],
            "prefixText": [1, "prefix-text"],
            "suffixText": [1, "suffix-text"],
            "type": [1],
            "multiline": [4],
            "width": [2],
            "height": [2],
            "autoGrow": [4, "auto-grow"],
            "cols": [2],
            "rows": [2],
            "noResize": [4, "no-resize"],
            "passView": [4, "pass-view"],
            "inputFocus": [32],
            "newValue": [32],
            "valueLength": [32],
            "prefixWidth": [32],
            "inputType": [32],
            "selectText": [64]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvToast {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // State
        //
        this.hovered = false;
        /**
         * If `true`, the toast will have displayed the progress bar.
         */
        this.showProgress = false;
        /**
         * The display duration in milliseconds of the alert.
         */
        this.duration = 4000;
        /**
         * If `true`, after the `duration`, the toast will have closed.
         */
        this.autoClose = true;
        /**
         * If `true`, the toast will have closed on click.
         */
        this.closeOnClick = true;
        this.closeClick = () => {
            if (this.closeOnClick) {
                this.close();
            }
        };
        this.onHover = () => {
            this.hovered = true;
        };
        this.onLeave = () => {
            this.hovered = false;
        };
    }
    //
    // Events
    //
    /**
     * Used for detect animation end and close the toast.
     * @internal
     */
    handleAnimEnd() {
        if (this.hovered) {
            return;
        }
        this.close();
    }
    /**
     * @internal
     * Used for detect the transition end for display toast.
     */
    handleTranEnd(evt) {
        if (evt.type === 'transitionend') {
            if (evt.propertyName !== 'right') {
                return;
            }
            if (this.el.classList.contains('nv-toast-flying')) {
                this.el.classList.remove('nv-toast-flying');
                this.el.classList.add('nv-toast-opened');
            }
            // go back to initial state after hiding
            if (this.el.classList.contains('nv-toast-hiding') ||
                this.el.classList.contains('nv-toast-closing')) {
                this.el.classList.remove('nv-toast-opened');
                this.el.classList.remove('nv-toast-opening');
                this.el.classList.remove('nv-toast-closing');
                this.el.classList.remove('nv-toast-flying');
                this.el.classList.remove('nv-toast-hiding');
                this.el.classList.add('nv-toast-closed');
                this.destroy();
            }
        }
    }
    //
    // Methods
    //
    async close() {
        this.el.classList.add('nv-toast-hiding');
    }
    async destroy() {
        this.el.remove();
    }
    async open() {
        this.el.classList.remove('nv-toast-closed');
        this.el.classList.remove('nv-toast-closing');
        // Wait a frame once display is no longer "none", to establish basis for animation
        runNextAnimationFrame(() => {
            // no operation during flying and hiding
            if (this.el.classList.contains('nv-toast-flying') ||
                this.el.classList.contains('nv-toast-hiding')) {
                return;
            }
            this.el.classList.add('nv-toast-flying');
        });
    }
    makeProgress() {
        if (this.autoClose) {
            return (h("div", { ref: el => this.progress = el, class: 'nv-toasts-progress', style: {
                    animationDuration: `${this.duration}ms`,
                } }));
        }
    }
    render() {
        const progress = this.makeProgress();
        return (h(Host, { onClick: this.closeClick, class: {
                'nv-toast': true,
                'nv-toast-closed': true,
                'nv-toast-show-progress': this.showProgress,
                'nv-toast-hovered': this.hovered,
            }, onMouseEnter: this.onHover, onMouseLeave: this.onLeave }, h("h4", null, this.textHeader), h("slot", null), progress));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-toast",
        "$members$": {
            "textHeader": [1, "text-header"],
            "showProgress": [4, "show-progress"],
            "duration": [2],
            "autoClose": [4, "auto-close"],
            "closeOnClick": [4, "close-on-click"],
            "hovered": [32],
            "close": [64],
            "destroy": [64],
            "open": [64]
        },
        "$listeners$": [[0, "animationend", "handleAnimEnd"], [0, "transitionend", "handleTranEnd"]],
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

// import Ripple                          from '../../utils/ripple';
class NvToasts {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        /**
         * The position of the toasts.
         */
        this.position = 'bottom-right';
    }
    render() {
        return (h(Host, { class: {
                'nv-toasts': true,
                'nv-toast-top-left': this.position === 'top-left' ? true : null,
                'nv-toast-top-right': this.position === 'top-right' ? true : null,
                'nv-toast-top-center': this.position === 'top-center' ? true : null,
                'nv-toast-bottom-left': this.position === 'bottom-left' ? true : null,
                'nv-toast-bottom-right': this.position === 'bottom-right' ? true : null,
                'nv-toast-bottom-center': this.position === 'bottom-center' ? true : null,
            } }, h("slot", null)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-toasts",
        "$members$": {
            "position": [1]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvTooltip {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        //
        // Computed
        //
        this.mouseEnter = (ev) => {
            const el = ev.target;
            const rootGetPos = el.getClientRects()[0];
            const tooltipGetPos = this.tooltipEl.getClientRects()[0];
            let posX = (rootGetPos.left + rootGetPos.width / 2) - tooltipGetPos.width / 2;
            let posY;
            if (this.position === 'top') {
                posY = rootGetPos.top - (tooltipGetPos.height + 6);
            }
            if (this.position === 'left') {
                posY = (rootGetPos.top + rootGetPos.height / 2) - tooltipGetPos.height / 2;
                posX = rootGetPos.left - (tooltipGetPos.width + 6);
            }
            if (this.position === 'right') {
                posY = (rootGetPos.top + rootGetPos.height / 2) - tooltipGetPos.height / 2;
                posX = rootGetPos.right + 6;
            }
            if (this.position === 'bottom') {
                posY = rootGetPos.bottom + 6;
            }
            this.tooltipEl.setAttribute('style', `left: ${posX}px; top: ${posY}px`);
            const body = document.querySelector('body');
            body.appendChild(this.tooltipEl);
            runNextAnimationFrame(() => {
                this.tooltipEl.classList.remove('nv-tooltip-hidden');
                this.tooltipEl.classList.add('nv-tooltip-display');
            });
        };
        this.mouseLeave = () => {
            runNextAnimationFrame(() => {
                this.tooltipEl.classList.add('nv-tooltip-hidden');
                this.tooltipEl.classList.remove('nv-tooltip-display');
            });
        };
        this.id = `tooltip-id-${new Date().getTime()}`;
    }
    render() {
        return (h(Host, { onMouseEnter: this.mouseEnter, onMouseLeave: this.mouseLeave, class: {
                'nv-tooltip-container': true,
            } }, h("div", null, h("slot", null)), h("div", { ref: el => this.tooltipEl = el, class: {
                'nv-tooltip': true,
                'nv-tooltip-hidden': true,
                'nv-tooltip-top': this.position === 'top' ? true : false,
                'nv-tooltip-bottom': this.position === 'bottom' ? true : false,
                'nv-tooltip-left': this.position === 'left' ? true : false,
                'nv-tooltip-right': this.position === 'right' ? true : false,
                [this.id]: true,
            } }, this.text)));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 4,
        "$tagName$": "nv-tooltip",
        "$members$": {
            "color": [1],
            "position": [1],
            "text": [1],
            "tooltipEl": [32]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

class NvUpload {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        this.progressLists = [];
        this.uploads = [];
        //
        // State
        //
        this.highlightEvent = false;
        this.filesToDo = 0;
        this.filesDone = 0;
        this.listFiles = [];
        this.listItens = [];
        this.update = false;
        /**
         * If `true`, the user can add a custom message element with a action click.
         */
        this.customMessage = false;
        /**
         * If `true`, the upload support multiple files upload.
         */
        this.multiple = false;
        /**
         * If `true`, the user cannot drag files for the upload.
         */
        this.nodrop = false;
        /**
         * If `false`, the list of the files will hidding.
         */
        this.list = true;
        /**
         * If `true`, the upload will be a compact style.
         */
        this.compact = false;
        /**
         * The type of the supported files for the upload.
         */
        this.accept = 'all';
        /**
         * If `true`, the user cannot interact with the upload.
         */
        this.disabled = false;
        this.highlight = () => {
            this.highlightEvent = true;
        };
        this.unhighlight = () => {
            this.highlightEvent = false;
        };
        this.handleDrop = (e) => {
            const dt = e.dataTransfer;
            const files = dt.files;
            this.handleFiles(files);
        };
        this.preventDefaults = (e) => {
            e.preventDefault();
            e.stopPropagation();
        };
        this.handleFiles = (files) => {
            this.update = false;
            // Create a initial progress.
            this.initProgress(files);
            // Upload files.
            this.listFiles.push(...files);
            ([...(this.listFiles)]).forEach((file, i) => {
                this.uploads.push(() => this.uploadFile(file, i));
            });
            this.input.value = '';
        };
        this.progressDone = (i, e, file) => {
            this.filesDone += 1;
            this.listFiles[i]['nv-send'] = true;
            this.nvOnSuccess.emit({
                file,
                XMLHttpRequest: e.srcElement,
            });
        };
        this.progressError = (e, file) => {
            this.nvOnError.emit({
                file,
                XMLHttpRequest: e.srcElement,
            });
        };
        this.updateProgress = (fileNumber, percent) => {
            const progress = this.progressLists[fileNumber];
            if (progress) {
                progress.value = Math.round(percent);
            }
        };
        this.uploadFile = (file, i) => {
            // Check if this file is not sent yet...
            if (!('nv-send' in file)) {
                const action = this.action;
                const xhr = new XMLHttpRequest();
                const formData = new FormData();
                xhr.open('POST', action, true);
                // Add progress tracker
                xhr.upload.addEventListener('progress', (e) => {
                    this.updateProgress(i, (e.loaded * 100.0 / e.total) || 100);
                });
                xhr.addEventListener('readystatechange', (e) => {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        this.progressDone(i, e, file);
                    }
                    else if (xhr.readyState === 4 && xhr.status !== 200) {
                        this.progressError(e, file);
                    }
                });
                formData.append(this.name, file);
                xhr.send(formData);
            }
            else {
                this.updateProgress(i, 100);
            }
        };
        this.openDialog = () => {
            this.input.click();
        };
        this.onChange = (e) => {
            const input = e.target;
            this.handleFiles(input.files);
        };
        this.nvOnSuccess = createEvent(this, "nvOnSuccess", 7);
        this.nvOnError = createEvent(this, "nvOnError", 7);
        this.nvOnDelete = createEvent(this, "nvOnDelete", 7);
    }
    //
    // Lifecyles
    //
    componentDidLoad() {
        if (!this.disabled) {
            const actionEl = this.el.querySelector('.nv-upload-action');
            if (actionEl) {
                actionEl.onclick = this.openDialog;
            }
            this.nvList.addEventListener('nvDidUpdate', () => console.log('teste'));
            if (!this.nodrop) {
                // Prepare and apply events for darg and drop.
                this.prepare();
            }
        }
    }
    componentDidUpdate() {
        if (!this.update) {
            this.update = true;
            [...(this.uploads)].forEach((func) => {
                func();
            });
        }
    }
    prepare() {
        // Apply the events
        ['dragenter', 'dragover', 'dragleave', 'drop'].forEach((eventName) => {
            this.el.addEventListener(eventName, this.preventDefaults, false);
        });
        // Apply highlight events
        ['dragenter', 'dragover'].forEach((eventName) => {
            this.el.addEventListener(eventName, this.highlight, false);
        });
        ['dragleave', 'drop'].forEach((eventName) => {
            this.el.addEventListener(eventName, this.unhighlight, false);
        });
        this.el.addEventListener('drop', this.handleDrop, false);
    }
    initProgress(files) {
        this.filesToDo = files.length;
    }
    makeIcon() {
        return (h("svg", { class: 'nv-upload-icon', viewBox: '0 0 30 30' }, h("path", { d: 'm4.6172 6c-0.36628 0.6326-0.67266\n         1.301-0.91992 2h22.605c-0.24726-0.699-0.55364-1.3674-0.91992-2h-20.766zm10.465\n        5.7598c-0.7698 0-1.5396 0.29155-2.1211 0.87305l-6.9609 6.9609c0.43664\n        0.50589 0.90744 0.97935 1.4219 1.4062l6.9531-6.9531c0.404-0.40397\n        1.0101-0.40397 1.4141 0l6.9531 6.9531c0.5142-0.42662 0.98544-0.9006\n        1.4219-1.4062l-6.9609-6.9609c-0.5815-0.5815-1.3513-0.87305-2.1211-0.87305z' })));
    }
    makeMessage() {
        if (!this.customMessage) {
            if (!this.compact) {
                return (h("div", { class: 'nv-text-center nv-my-3' }, h("h3", null, h("strong", { class: 'nv-upload-action' }, "Choose a file"), " or drag it here.")));
            }
            return (h("div", { class: 'nv-text-center nv-my-3' }, h("h4", null, h("strong", { class: 'nv-upload-action' }, "Choose a file"), " or drag it here.")));
        }
    }
    makeTickIcon() {
        return (h("svg", { class: 'nv-tick-icon-upload', viewBox: '0 0 30 30' }, h("g", { transform: 'translate(0 -283.15)' }, h("path", { transform: 'translate(0 283.15)', d: 'm27 6.0195-15.932 18.988-8.0684-6.7695v2.6094l6.7832\n          5.6914c0.83553 0.70109 2.1173 0.58944 2.8184-0.24609l14.398-17.16v-3.1133z' }))));
    }
    makeList() {
        const list = [];
        const nvProgress = [];
        let tickIcon;
        let removeBtn;
        this.listFiles.forEach((file, i) => {
            if (!('nv-send' in file)) {
                removeBtn = (h("nv-button", { onClick: () => this.removeItem(i), rounded: true, color: 'danger', size: 'small' }, "X"));
            }
            else {
                tickIcon = this.makeTickIcon();
            }
            list.push((h("nv-list-item", { textTitle: file.name }, h("div", { class: 'nv-upload-progress-container' }, h("div", { class: 'nv-upload-progress' }, h("nv-progress", { ref: el => nvProgress.push(el), id: `nv-upload-${file.name}`, value: 0 })), h("div", { class: 'nv-upload-actions' }, removeBtn, tickIcon)))));
        });
        this.progressLists = nvProgress || [];
        this.listItens = list || [];
        if (this.list) {
            return (h("div", { class: 'nv-list-upload' }, h("nv-list", { ref: el => this.nvList = el }, this.listItens)));
        }
    }
    removeItem(i) {
        this.nvOnDelete.emit(this.listFiles[i]);
        this.listFiles.splice(i, 1);
        this.progressLists.splice(i, 1);
        this.listItens = this.listItens.filter(item => item !== this.listItens[i]);
    }
    render() {
        const icon = this.makeIcon();
        const message = this.makeMessage();
        const list = this.makeList();
        return (h(Host, { class: {
                'nv-upload': true,
                'nv-upload-nodrop': this.nodrop,
                'nv-upload-highlight': this.highlightEvent,
                'nv-upload-compact': this.compact,
                'nv-upload-disabled': this.disabled,
            } }, h("div", { class: 'nv-upload-container' }, h("input", { type: 'file', onChange: this.onChange, ref: input => this.input = input, multiple: this.multiple, accept: this.accept }), icon, message), list));
    }
    get el() { return getElement(this); }
    static get cmpMeta() { return {
        "$flags$": 0,
        "$tagName$": "nv-upload",
        "$members$": {
            "color": [1],
            "customMessage": [4, "custom-message"],
            "multiple": [4],
            "nodrop": [4],
            "list": [4],
            "action": [1],
            "name": [1],
            "compact": [4],
            "accept": [1],
            "disabled": [4],
            "highlightEvent": [32],
            "filesToDo": [32],
            "filesDone": [32],
            "listFiles": [32],
            "listItens": [32],
            "update": [32]
        },
        "$listeners$": undefined,
        "$lazyBundleIds$": "-",
        "$attrsToReflect$": []
    }; }
}

const cmps = [
  NvAlert,
  NvAlert$1,
  NvAvatar,
  NvBadge,
  NvBreadcrumb,
  NvBreadcrumbs,
  NvButton,
  NvCard,
  NvCardBody,
  NvCardContent,
  NvCardFooter,
  NvCardHeader,
  NvCardIcon,
  NvCardMedia,
  NvCardText,
  NvCardTitle,
  NvCheckbox,
  NvChip,
  NvCollapse,
  NvCollapseItem,
  NvDialog,
  NvDialogAction,
  NvDialogAlert,
  NvDialogConfirm,
  NvDialogPrompt,
  NvDialogSelect,
  NvLabel,
  NvList,
  NvListGroup,
  NvListHeader,
  NvListItem,
  NvLoader,
  NvNavigationItem,
  NvNavigationItemHeader,
  NvNavigationItems,
  NvNavigationView,
  NvNavigationViewSeparator,
  NvProgress,
  NvRadio,
  NvRadioGroup,
  NvRating,
  NvScroll,
  NvSelect,
  NvSelectOption,
  NvSlider,
  NvSubHeader,
  NvSwitch,
  NvTab,
  NvTabBar,
  NvTabButton,
  NvTabs,
  NvTextField,
  NvToast,
  NvToasts,
  NvTooltip,
  NvUpload,
];
registerComponents(cmps);

exports.bootstrapHydrate = bootstrapHydrate;
